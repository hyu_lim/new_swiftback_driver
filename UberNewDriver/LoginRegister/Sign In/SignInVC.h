
#import "BaseVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SignInVC : BaseVC <UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UICollectionViewDataSource,UICollectionViewDelegate,FBSDKLoginButtonDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblNavigationTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDivider;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblCarModelInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblCarNumberInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailInfo;
@property (weak, nonatomic) IBOutlet UILabel *backgroundLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblVehicleInformationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOptionalTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPickerViewTitle;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtTaxiModel;
@property (weak, nonatomic) IBOutlet UITextField *txtTaxiNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtCarColor;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UITextField *txtOccupation;

@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;
@property (weak, nonatomic) IBOutlet UIButton *btnTerms;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectService;
@property (weak, nonatomic) IBOutlet UIButton *btnAgreeCondtion;
@property (weak, nonatomic) IBOutlet UIButton *btnNav_Register;
@property (weak, nonatomic) IBOutlet UIButton *btnFB;
@property (weak, nonatomic) IBOutlet UIButton *btnCountryCode;
@property (weak, nonatomic) IBOutlet UIButton *btnFacebook;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (weak, nonatomic) IBOutlet UIImageView *imgType;
@property (weak, nonatomic) IBOutlet UIImageView *imgInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgCarModelInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgCarNumberInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmailInfo;
@property (weak, nonatomic) IBOutlet UIImageView *fbIcon;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;

@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *pickTypeView;
@property (weak, nonatomic) IBOutlet UIPickerView *typePicker;
@property (weak, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UICollectionView *typeCollectionView;

- (IBAction)faceBookBtnPressed:(id)sender;
- (IBAction)selectCountryBtnPressed:(id)sender;
- (IBAction)googleBtnPressed:(id)sender;
- (IBAction)doneBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)saveBtnPressed:(id)sender;
- (IBAction)imgPickBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)typeBtnPressed:(id)sender;
- (IBAction)pickDoneBtnPressed:(id)sender;
- (IBAction)pickCancelBtnPressed:(id)sender;
- (IBAction)termsBtnPressed:(id)sender;
- (IBAction)checkBtnPressed:(id)sender;
- (IBAction)selectServiceBtnPressed:(id)sender;
- (IBAction)onClickCarModelInfo:(id)sender;
- (IBAction)onClickCarNumberInfo:(id)sender;
- (IBAction)onClickEmailInfo:(id)sender;
- (IBAction)onClickInfo:(id)sender;
- (IBAction)onClickFemale:(id)sender;
- (IBAction)onClickMale:(id)sender;
- (IBAction)onClickGender:(id)sender;

@end
