
#import "BaseVC.h"

@interface TermsVC : BaseVC <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;
@property (weak, nonatomic) IBOutlet UIWebView *webViewTerms;

- (IBAction)backBtnPressed:(id)sender;

@end
