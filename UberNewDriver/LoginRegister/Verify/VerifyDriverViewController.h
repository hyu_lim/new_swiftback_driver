
#import "BaseVC.h"



#import <UIKit/UIKit.h>
#import "VerifyPageContentViewController.h"

@interface VerifyDriverViewController : BaseVC <UITextFieldDelegate,UIAlertViewDelegate,UIPageViewControllerDataSource>
{
    
}

//- (IBAction)startWalkthrough:(id)sender;

@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *pageTitles2;
@property (strong, nonatomic) NSArray *pageImages2;
@property (strong, nonatomic) NSArray *pageTitles3;

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverVerificationText;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UIButton *btnTerm;
@property (strong, nonatomic) IBOutlet UIButton *btn_Continue;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (strong, nonatomic) IBOutlet UIWebView *webViewVerify;

- (IBAction)ContinueBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)openSwiftbackVerify:(id)sender;

@end