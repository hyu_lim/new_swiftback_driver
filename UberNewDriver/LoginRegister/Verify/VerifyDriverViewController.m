

#import "VerifyDriverViewController.h"
#import "AppDelegate.h"
#import "Constant.h"
#import "SWRevealViewController.h"

@interface VerifyDriverViewController ()

@end

@implementation VerifyDriverViewController

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    [self layoutSetup];
    [self setLocalizedStrings];
    
    // Create page view controller
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyPageViewController"];
    self.pageViewController.dataSource = self;
    
    VerifyPageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageViewController.view.frame = CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height - 210);
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)setLocalizedStrings {
    
    _pageTitles = @[NSLocalizedString(@"VERIFY_YOUR_PROFILE", nil), NSLocalizedString(@"DRIVER'S_LICENSE", nil), NSLocalizedString(@"VEHICLE_PHOTOS", nil), NSLocalizedString(@"VEHICLE'S_INSURANCE", nil), NSLocalizedString(@"HELP_BUILD_A_SAFE", nil)];
    
    _pageImages = @[@"icon_profile_verified.png", @"icon_profile_id.png", @"icon_carphoto.png", @"icon_insurance.png", @"icon_sharing.png"];
    
    _pageTitles2 = @[@" ", @" ", @" ", @" ", @" "];
    
    _pageImages2 = @[@" ", @" ", @" ", @" ", @" "];
    
    _pageTitles3 = @[NSLocalizedString(@"IN_FIVE_SIMPLE_STEPS", nil), NSLocalizedString(@"UPLOAD_A_SNAP_OF_YOUR_ID", nil), NSLocalizedString(@"UPLOAD_PHOTOS_OF_YOUR_VEHICLE", nil), NSLocalizedString(@"UPLOAD_PHOTOS_OF_YOUR_INSURANCE", nil), NSLocalizedString(@"AND_FUN_COMMUNITY_FOR_ALL", nil)];
    
    [self.lblNavTitle setText:NSLocalizedString(@"DRIVER_VERIFICATION", nil)];
    [self.lblDriverVerificationText setText:NSLocalizedString(@"DRIVER_VERIFICATION_TEXT", nil)];
    [self.btnVerify setTitle:NSLocalizedString(@"VERIFY", nil) forState:UIControlStateNormal];
    [self.btnDone setTitle:NSLocalizedString(@"DONE", nil) forState:UIControlStateNormal];
}

- (void) layoutSetup {
    
    [super setBackBarItem];
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //page controller properties
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor colorWithRed:1.00 green:1.00 blue:1.00 alpha:0.5];
    pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    pageControl.backgroundColor = [UIColor clearColor];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backBtnPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
- (IBAction)openSwiftbackVerify:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://swiftback.com/verify"]];
}*/

- (IBAction)ContinueBtnPressed:(id)sender {
    
}

//Page View Controller

//

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return 0;
}
//

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((VerifyPageContentViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((VerifyPageContentViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

/*- (void)move_pagination {
 
 
 UIScrollView *scrMain = (UIScrollView*) [self.view viewWithTag:1];
 
 UIPageControl *pgCtr = (UIPageControl*) [self.view viewWithTag:12];
 
 CGFloat contentOffset = scrMain.contentOffset.x;
 // calculate next page to display
 int nextPage = (int)(contentOffset/scrMain.frame.size.width) + 1 ;
 // if page is not 10, display it
 if( nextPage!=10 )  {
 [scrMain scrollRectToVisible:CGRectMake(nextPage*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
 pgCtr.currentPage=nextPage;
 // else start sliding form 1 :)
 } else {
 [scrMain scrollRectToVisible:CGRectMake(0, 0, scrMain.frame.size.width, scrMain.frame.size.height) animated:YES];
 pgCtr.currentPage=0;
 }
 }
 
 */


- (VerifyPageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    VerifyPageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyPageContentController"];
    pageContentViewController.imageFile = self.pageImages[index];
    pageContentViewController.titleText = self.pageTitles[index];
    pageContentViewController.imageFile2 = self.pageImages2[index];
    pageContentViewController.titleText2 = self.pageTitles2[index];
    pageContentViewController.titleText3 = self.pageTitles3[index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}


@end
