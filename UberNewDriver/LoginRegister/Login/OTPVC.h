
#import <UIKit/UIKit.h>

@interface OTPVC : UIViewController<UITextFieldDelegate>
{
    __weak IBOutlet UILabel *lblMobileNo;
    
    __weak IBOutlet UITextField *txtOTP;
    
    NSMutableDictionary *dictLogin;
    
    NSTimer *timerForOTPExpiry;
    
    int secondsLeft;
    
    int minutes, seconds;
    
    NSUserDefaults *defaults;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblVerifyNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblOTPText;
@property (weak, nonatomic) IBOutlet UIButton *btnResendOTP;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property (weak, nonatomic) IBOutlet UILabel *lblTimer;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


- (IBAction)onClickResend:(id)sender;
- (IBAction)onClickSubmit:(id)sender;
- (IBAction)onClickBack:(id)sender;

@end
