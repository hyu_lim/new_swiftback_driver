
#import "LoginVC.h"
#import "UIImageView+Download.h"
#import "AppDelegate.h"

@interface LoginVC ()
{
	AppDelegate *appDelegate;
	BOOL internet;
	NSMutableArray *arrForCountry;
	NSMutableDictionary *dictparam;
	
	NSString * strEmail;
	NSString * strPassword;
	NSString * strLogin;
	NSMutableString * strSocialId;
}

@end

@implementation LoginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

#pragma mark -
#pragma mark - View Life Cycle

@synthesize txtPassword;
@synthesize txtEmail;

- (void)viewDidLayoutSubviews
{
	[super viewDidLayoutSubviews];
	[self.scrLogin setContentSize:CGSizeMake(320, 600)];
}

- (BOOL)application:(UIApplication *)application
			openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
		 annotation:(id)annotation {
	
	return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation];
}

- (void)viewDidLoad
{
	[self setNeedsStatusBarAppearanceUpdate];
	[super viewDidLoad];
	[super setBackBarItem];
	[self setLocalizedStrings];
    [self layoutSetup];
}

- (void)layoutSetup {
    
    ////UI STYLE////
    //facebook button
    _btnFB.layer.cornerRadius = 5;
    _btnFB.layer.borderWidth = 1;
    _btnFB.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //register button
    _btnSignIn.layer.cornerRadius = 5;
    _btnSignIn.layer.borderWidth = 1;
    _btnSignIn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundRect.layer.shadowRadius = 5.0;
    _backgroundRect.layer.shadowOpacity = 0.4;
    
    //    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    //    loginButton.readPermissions =@[@"email",@"user_friends",@"public_profile"];
    //    loginButton.delegate=self;
    //    // Optional: Place the button in the center of your view.
    //    self.btnFB = loginButton;
    
    //NSLog(@"fonts: %@", [UIFont familyNames]);
    
    //txtEmail.text=@"nirav.kotecha99@gmail.com";
    //txtPassword.text=@"123456";
    dictparam=[[NSMutableDictionary alloc]init];
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strEmail=[pref objectForKey:PREF_EMAIL];
    strPassword=[pref objectForKey:PREF_PASSWORD];
    strLogin=[pref objectForKey:PREF_LOGIN_BY];
    strSocialId=[pref objectForKey:PREF_SOCIAL_ID];
    
    internet=[APPDELEGATE connected];
    
    if(strEmail!=nil || [strEmail length]>1)
    {
        [self getSignIn];
    }
    
    
    
    //[self customFont];
    
    //    [self.btnSignIn setTitle:NSLocalizedString(@"SIGN_IN", nil) forState:UIControlStateNormal];
    //
    //    [self.btnForgotPsw setTitle:NSLocalizedString(@"FORGOT_PASSWORD", nil) forState:UIControlStateNormal];
    
    //[self.btnSignUp setTitle:NSLocalizedString(@"SIGN_UP", nil) forState:UIControlStateNormal];
    
    //    self.txtEmail.placeholder = NSLocalizedString(@"EMAIL", nil);
    //    self.txtPassword.placeholder = NSLocalizedString(@"PASSWORD", nil);
    
    //[txtEmail setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    //[txtPassword setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    
    
    // background layer of "Sign in with Facebook" and "Sign in" button
    _lblButtonsBackground.layer.cornerRadius = 10;
    _lblButtonsBackground.clipsToBounds = YES;
    _lblButtonsBackground.layer.borderWidth = 0.7;
    _lblButtonsBackground.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
}

- (void)setLocalizedStrings
{
    self.lblNavigationTitle.text = NSLocalizedString(@"SIGN_IN", nil);
	self.txtEmail.placeholder = NSLocalizedString(@"EMAIL", nil);
	self.txtPassword.placeholder = NSLocalizedString(@"PASSWORD", nil);
	
	[self.btnForgotPsw setTitle:NSLocalizedString(@"FORGOT_PASSWORD", nil) forState:UIControlStateNormal];
	[self.btnSignIn setTitle:NSLocalizedString(@"SIGN_IN", nil) forState:UIControlStateNormal];
    [self.btnFB setTitle:NSLocalizedString(@"SIGN_IN_WITH_FACEBOOK", nil) forState:UIControlStateNormal];
	
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated
{
	self.navigationController.navigationBarHidden=YES;
	[self.btnSignUp setTitle:NSLocalizedString(@"Sign In", nil) forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated
{
	[self.btnSignUp setTitle:NSLocalizedString(@"Sign In", nil) forState:UIControlStateNormal];
}

/*
 -(void)customFont
 {
 self.txtEmail.font=[UberStyleGuide fontRegular];
 self.txtPassword.font=[UberStyleGuide fontRegular];
 
 //self.btnForgotPsw.titleLabel.font = [UberStyleGuide fontRegularBold];
 //self.btnSignIn.titleLabel.font = [UberStyleGuide fontRegularBold];
 //self.btnSignUp.titleLabel.font = [UberStyleGuide fontRegularBold];
 
 //self.btnSignIn=[APPDELEGATE setBoldFontDiscriptor:self.btnSignIn];
 //self.btnForgotPsw=[APPDELEGATE setBoldFontDiscriptor:self.btnForgotPsw];
 //self.btnSignUp=[APPDELEGATE setBoldFontDiscriptor:self.btnSignUp];
 
 }
 
 */

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
#pragma mark -
#pragma mark - Sign In

-(void)getSignIn
{
    dictparam=[[NSMutableDictionary alloc] init];
    
	if (strEmail==nil || [strEmail length]==0)
	{
		NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
		strEmail=[pref objectForKey:PREF_EMAIL];
		strPassword=[pref objectForKey:PREF_PASSWORD];
		strLogin=[pref objectForKey:PREF_LOGIN_BY];
		strSocialId=[pref objectForKey:PREF_SOCIAL_ID];
	}
    
	if([APPDELEGATE connected] )
	{
		if([strEmail length] > 0)
		{
			[APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
			
			if ([device_token length] == 0 || device_token == nil)
			{
				device_token = @"1212121212";
			}
            dictparam=[[NSMutableDictionary alloc]init];
			[dictparam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
			[dictparam setObject:@"ios" forKey:PARAM_DEVICE_TYPE];
			[dictparam setObject:strEmail forKey:PARAM_EMAIL];
			
			NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
			NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
			
			
			[dictparam setObject:majorVersion forKey:PARAM_VERSION];
			
			[dictparam setObject:strLogin forKey:PARAM_LOGIN_BY];
			if (![strLogin isEqualToString:@"manual"])
			{
				[dictparam setObject:strSocialId forKey:PARAM_SOCIAL_ID];
				
			}
			else
			{
				[dictparam setObject:strPassword forKey:PARAM_PASSWORD];
			}
			NSLog(@"Login Params %@",dictparam);
			
			// NSString *url = [NSString stringWithFormat:@"swift_back/public/%@",FILE_LOGIN];
			
			AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
			[afn getDataFromPath:FILE_LOGIN withParamData:dictparam withBlock:^(id response, NSError *error)
			 {
				 if (response)
				 {
					 NSLog(@"Login Response %@",response);
					 
					 if([[response valueForKey:@"success"] intValue]==1)
					 {
						 arrUser=response;
						 
						 [USERDEFAULT setObject:[response valueForKey:@"token"] forKey:PREF_USER_TOKEN];
						 [USERDEFAULT setObject:[response valueForKey:@"id"] forKey:PREF_USER_ID];
						 // [pref setObject:[NSString stringWithFormat:@"%@",device_token] forKey:PREF_DEVICE_TOKEN];
						 [USERDEFAULT setObject:[response valueForKey:PREF_USER_PHONE] forKey:PREF_USER_PHONE];
						 [USERDEFAULT setObject:[response objectForKey:PARAM_OTP_VERIFY] forKey:PARAM_OTP_VERIFY];
						 
						 
						 [USERDEFAULT setObject:txtEmail.text forKey:PREF_EMAIL];
						 [USERDEFAULT setObject:txtPassword.text forKey:PREF_PASSWORD];
						 
						 
						 if (![strLogin isEqualToString:@"manual"])
						 {
							 [USERDEFAULT setObject:strSocialId forKey:PARAM_SOCIAL_ID];
							 
						 }
						 
						 [USERDEFAULT setObject:strLogin forKey:PREF_LOGIN_BY];
						 [USERDEFAULT setBool:YES forKey:PREF_IS_LOGIN];
						 
						 [USERDEFAULT setObject:[response valueForKey:@"is_approved"] forKey:PREF_IS_APPROVED];
						 
						 [USERDEFAULT synchronize];
						 
						 
						 strEmail=[USERDEFAULT objectForKey:PREF_EMAIL];
						 strPassword=[USERDEFAULT objectForKey:PREF_PASSWORD];
						 
						 txtPassword.userInteractionEnabled=YES;
						 [APPDELEGATE hideLoadingView];
						 [APPDELEGATE showToastMessage:(NSLocalizedString(@"LOGIN_SUCCESS", nil))];
						 
						 // if ([[response objectForKey:PARAM_OTP_VERIFY] integerValue]== 1)
						 ///  {
						 [self performSegueWithIdentifier:@"seguetopickme" sender:self];
						 //   return;
						 
						 // }
						 //  else
						 ///  {
						 //   [self performSegueWithIdentifier:SEGUE_TO_OTP sender:self];
						 //  return;
						 //}
						 
						 
						 //            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"REGISTER_SUCCESS", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
						 //            [alert show];
					 }
					 else
					 {
						 txtPassword.userInteractionEnabled=YES;
                         
                         UIAlertController *emailAlert = [UIAlertController alertControllerWithTitle:@"" message:[response valueForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
                         UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
                         
                         [emailAlert addAction:okAction];
                         [self presentViewController:emailAlert animated:YES completion:nil];
					 }
				 }
				 
				 
				 [APPDELEGATE hideLoadingView];
				 NSLog(@"REGISTER RESPONSE --> %@",response);
			 }];
		}
        else {
            if(self.txtEmail.text.length == 0) {
                UIAlertController *emailAlert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"PLEASE_EMAIL", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
               
                [emailAlert addAction:okAction];
                [self presentViewController:emailAlert animated:YES completion:nil];
                                                 
            } else {
                UIAlertController *pwdAlert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"PLEASE_PASSWORD", nil) preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
                
                [pwdAlert addAction:okAction];
                [self presentViewController:pwdAlert animated:YES completion:nil];

            }
        }
	}
	else
	{
        
        UIAlertController *pwdAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [pwdAlert addAction:okAction];
        [self presentViewController:pwdAlert animated:YES completion:nil];
	}
	
}

#pragma mark -
#pragma mark - Button Action

- (IBAction)onClickSignIn:(id)sender
{
	[txtEmail resignFirstResponder];
	[txtPassword resignFirstResponder];
	
	strEmail=self.txtEmail.text;
	strPassword=self.txtPassword.text;
	strLogin=@"manual";
	
	/* NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
	 [pref setObject:txtEmail.text forKey:PREF_EMAIL];
	 [pref setObject:txtPassword.text forKey:PREF_PASSWORD];
	 [pref setObject:@"manual" forKey:PREF_LOGIN_BY];
	 [pref setBool:YES forKey:PREF_IS_LOGIN];
	 [pref synchronize];
	 */
	[self getSignIn];
	
}

/*- (IBAction)googleBtnPressed:(id)sender
 {
 [APPDELEGATE showLoadingWithTitle:@"PLEASE_WAIT_"];
 if([APPDELEGATE connected])
 {
 
 [[GooglePlusUtility sharedObject]loginWithBlock:^(id response, NSError *error)
 {
 [APPDELEGATE hideLoadingView];
 if (response) {
 NSLog(@"Response ->%@ ",response);
 txtPassword.userInteractionEnabled=NO;
 self.txtEmail.text=[response valueForKey:@"email"];
 
 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
 [pref setObject:txtEmail.text forKey:PREF_EMAIL];
 [pref setObject:@"google" forKey:PREF_LOGIN_BY];
 [pref setObject:[response valueForKey:@"userid"] forKey:PREF_SOCIAL_ID];
 [pref setBool:YES forKey:PREF_IS_LOGIN];
 [pref synchronize];
 
 [APPDELEGATE hideLoadingView];
 
 [self getSignIn];
 }
 }];
 
 
 }
 else
 {
 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
 [alert show];
 }
 
 
 }*/

- (IBAction)facebookBtnPressed:(id)sender
{
	if ([APPDELEGATE connected])
	{
		FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
		
		[login logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
						
			if (error) {
                NSLog(@"Process error");
            } else if (result.isCancelled) {
                NSLog(@"Cancelled");
            } else {
                NSLog(@"Logged in");
                [APPDELEGATE showLoadingWithTitle:@"Please wait"];

                // [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];

                if ([FBSDKAccessToken currentAccessToken]) {

                    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                                  initWithGraphPath:@"me"
                                                  parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
                                                  HTTPMethod:@"GET"];
                    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                                          id result,
                                                          NSError *error) {
                        NSLog(@"%@",result);
                        // Handle the result
                        [APPDELEGATE hideLoadingView];

                        self.txtEmail.text=[result valueForKey:@"email"];

                        txtPassword.userInteractionEnabled=NO;
                        txtPassword.text=@"";


                        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                        [pref setObject:[result valueForKey:@"email"] forKey:PREF_EMAIL];
                        [pref setObject:@"facebook" forKey:PREF_LOGIN_BY];
                            [pref setObject:[result valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
                        // [pref setBool:YES forKey:PREF_IS_LOGIN];
                        [pref synchronize];
                        strLogin = @"facebook";
                        
                        [self getSignIn];
                    }];
                }
            }

		}];
		

//		[login logInWithReadPermissions:@[@"public_profile",@"email"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
//			
//			if (error) {
//				NSLog(@"Process error");
//			} else if (result.isCancelled) {
//				NSLog(@"Cancelled");
//			} else {
//				NSLog(@"Logged in");
//				[APPDELEGATE showLoadingWithTitle:@"Please wait"];
//				
//				// [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"LOGIN", nil)];
//				
//				if ([FBSDKAccessToken currentAccessToken]) {
//					
//					FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
//												  initWithGraphPath:@"me"
//												  parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}
//												  HTTPMethod:@"GET"];
//					[request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
//														  id result,
//														  NSError *error) {
//						NSLog(@"%@",result);
//						// Handle the result
//						[APPDELEGATE hideLoadingView];
//						
//						self.txtEmail.text=[result valueForKey:@"email"];
//						
//						txtPassword.userInteractionEnabled=NO;
//						txtPassword.text=@"";
//						
//						
//						NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
//						[pref setObject:[result valueForKey:@"email"] forKey:PREF_EMAIL];
//						[pref setObject:@"facebook" forKey:PREF_LOGIN_BY];
//						[pref setObject:[result valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
//						// [pref setBool:YES forKey:PREF_IS_LOGIN];
//						[pref synchronize];
//						strLogin = @"facebook";
//						
//						[self getSignIn];
//					}];
//				}
//			}
//		}];
		
	}
	else
	{
		UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self
										   cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
		[alert show];
		
	}
	
	
	//    if([APPDELEGATE connected])
	//    {
	//        if (![[FacebookUtility sharedObject]isLogin])
	//        {
	//            [[FacebookUtility sharedObject]loginInFacebook:^(BOOL success, NSError *error)
	//             {
	//                 [APPDELEGATE hideLoadingView];
	//                 if (success)
	//                 {
	//
	//                     NSLog(@"Success");
	//                     appDelegate = [UIApplication sharedApplication].delegate;
	//                     [appDelegate userLoggedIn];
	//                     [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error) {
	//                         if (response) {
	//                             NSLog(@"%@",response);
	//                             self.txtEmail.text=[response valueForKey:@"email"];
	//
	//                             txtPassword.userInteractionEnabled=NO;
	//                             txtPassword.text=@"";
	//
	//
	//                             NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
	//                             [pref setObject:[response valueForKey:@"email"] forKey:PREF_EMAIL];
	//                             [pref setObject:@"facebook" forKey:PREF_LOGIN_BY];
	//                             [pref setObject:[response valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
	//                             [pref setBool:YES forKey:PREF_IS_LOGIN];
	//                             [pref synchronize];
	//
	//                             [self getSignIn];
	//
	//                         }
	//                     }];
	//                 }
	//             }];
	//        }
	//        else{
	//            NSLog(@"User Login Click");
	//            appDelegate = [UIApplication sharedApplication].delegate;
	//            [[FacebookUtility sharedObject]fetchMeWithFBCompletionBlock:^(id response, NSError *error) {
	//                [APPDELEGATE hideLoadingView];
	//
	//                if (response) {
	//                    NSLog(@"%@",response);
	//                    NSLog(@"%@",response);
	//                    self.txtEmail.text=[response valueForKey:@"email"];
	//                    txtPassword.userInteractionEnabled=NO;
	//                    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
	//                    [pref setObject:[response valueForKey:@"email"] forKey:PREF_EMAIL];
	//                    [pref setObject:@"facebook" forKey:PREF_LOGIN_BY];
	//                    [pref setObject:[response valueForKey:@"id"] forKey:PREF_SOCIAL_ID];
	//                    [pref setBool:YES forKey:PREF_IS_LOGIN];
	//                    [pref synchronize];
	//                    [self getSignIn];
	//
	//                }
	//            }];
	//
	//            [appDelegate userLoggedIn];
	//
	//        }
	//    }
	//    else
	//    {
	//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
	//        [alert show];
	//    }
}

- (IBAction)forgotBtnPressed:(id)sender
{
	
}

- (IBAction)backBtnPressed:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickBack:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark - TextField Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
	int y=0;
    
	if (textField==self.txtEmail)
	{
		y=70;
	}
	else if (textField==self.txtPassword){
		y=100;
	}
	[self.scrLogin setContentOffset:CGPointMake(0, y) animated:YES];
	
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
//	if (textField==self.txtEmail)
//	{
//		[self.txtPassword becomeFirstResponder];
//	}
//	else if (textField==self.txtPassword){
//		[textField resignFirstResponder];
//		[self.scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
//	}
    
    [textField resignFirstResponder];
    [self.scrLogin setContentOffset:CGPointMake(0, 0) animated:YES];
	return YES;
}

/*
 #pragma mark-
 #pragma mark- Text Field Delegate
 
 - (BOOL)textFieldShouldReturn:(UITextField *)textField     //Hide the keypad when we pressed return
 {
 if (textField==txtEmail)
 {
 [self.txtPassword becomeFirstResponder];
 }
 
 [textField resignFirstResponder];
 return YES;
 }
 
 - (void)textFieldDidBeginEditing:(UITextField *)textField
 
 {
 if(textField == self.txtPassword)
 {
 UITextPosition *beginning = [self.txtPassword beginningOfDocument];
 [self.txtPassword setSelectedTextRange:[self.txtPassword textRangeFromPosition:beginning
 toPosition:beginning]];
 [UIView animateWithDuration:0.3 animations:^{
 
 self.view.frame = CGRectMake(0, -35, 320, 480);
 
 } completion:^(BOOL finished) { }];
 }
 }
 - (void)textFieldDidEndEditing:(UITextField *)textField
 {
 UIDevice *thisDevice=[UIDevice currentDevice];
 if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
 {
 CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
 
 if (iOSDeviceScreenSize.height == 568)
 {
 if(textField == self.txtPassword)
 {
 [UIView animateWithDuration:0.3 animations:^{
 
 self.view.frame = CGRectMake(0, 0, 320, 568);
 
 } completion:^(BOOL finished) { }];
 }
 
 }
 else
 {
 
 if(textField == self.txtPassword)
 {
 [UIView animateWithDuration:0.3 animations:^{
 
 self.view.frame = CGRectMake(0, 0, 320, 480);
 
 } completion:^(BOOL finished) { }];
 }
 
 
 }
 }
 }
 */


@end
