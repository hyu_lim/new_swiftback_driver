

#import "AppDelegate.h"
#import "SignInVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SVProgressHUD.h"
#import <Stripe/Stripe.h>

@implementation AppDelegate
{
    MBProgressHUD *HUD;
    UIView *backgroundOverlayView;
    NSString *countryName;
}

@synthesize viewLoading;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Stripe setDefaultPublishableKey:@"pk_test_VMoMCCOURMHqknJ7WrmplAbM"];
    [FBSDKAppEvents activateApp];
    [FBSDKAppEvents logEvent:@"intitiated"];
    [self getDeviceLanguage];
    
    pushId=0;
    pushDict=[[NSDictionary alloc] init];
    
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];
    
    //==
    
    [GMSServices provideAPIKey:Google_Key];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

	//==
    
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
    else
    {
        //register to receive notifications
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    

    //[[Mint sharedInstance] initAndStartSession:@"1739739f"];
    
    [application setStatusBarHidden:NO];
    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if (launchOptions != nil)
    {
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {
            NSLog(@"Launched from push notification: %@", dictionary);
            
            NSMutableDictionary *aps=[dictionary valueForKey:@"aps"];
            
            NSDictionary *dict=[aps valueForKey:@"message"];
            
            pushId=[[dict valueForKey:@"unique_id"]intValue];
            if(pushId!=111 || pushId!=1 || pushId!=5)
            {
                pushId=0;
            }
            else
            {
                pushDict=dict;
            }
        }
    }
    
    //==
    
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    
    #ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
        //  [locationManager requestAlwaysAuthorization];
    }
    #endif
    [locationManager startUpdatingLocation];
    return YES;
    
}

- (void)userLoggedIn
{
    // Set the button title as "Log out"
    
    SignInVC *obj=[[SignInVC alloc]init];
    UIButton *loginButton = obj.btnFacebook;
    [loginButton setTitle:NSLocalizedString(@"LOG_OUT", nil) forState:UIControlStateNormal];
    
    // Welcome message
    // [self showMessage:@"You're now logged in" withTitle:@"Welcome!"];
    
}

- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation
{

        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation
                ];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    UIApplication *app = [UIApplication sharedApplication];
    UIBackgroundTaskIdentifier bgTask = 0;
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSDKAppEvents activateApp];
    application.applicationIconBadgeNumber = 0;
    
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Application's Documents directory
/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -
#pragma mark - Directory Path Methods

- (NSString *)applicationCacheDirectoryString
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cacheDirectory = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return cacheDirectory;
}

#pragma mark-
#pragma mark- Handle Push Method

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

//For interactive notification only
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    device_token=dt;

    if(dt==nil)
    {
        device_token=@"r11";
    }
    
    NSLog(@"My token is: %@", dt);
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setObject:device_token forKey:PREF_DEVICE_TOKEN];
    [pref synchronize];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"My token is error: %@", error);
    
    if (device_token==nil)
    {
        device_token=@"11111";
    }
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setObject:device_token forKey:PREF_DEVICE_TOKEN];
    [pref synchronize];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSMutableDictionary *aps=[userInfo valueForKey:@"aps"];
    NSMutableDictionary *msg=[aps valueForKey:@"message"];
    
    pushId=[[msg valueForKey:@"unique_id"]intValue];
    if(pushId!=111 || pushId!=1 || pushId!=5)
    {
        pushId=0;
    }
    else
    {
        pushDict=msg;
    }
    
}

-(void)handleRemoteNitification:(UIApplication *)application userInfo:(NSDictionary *)userInfo
{
   // NSMutableDictionary *aps=[userInfo valueForKey:@"aps"];
   // NSMutableDictionary *msg=[aps valueForKey:@"request_data"];
  //  UIAlertView *alert=[[UIAlertView alloc]initWithTitle:[NSString stringWithFormat:@"%@",msg] message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:@"cancel", nil];
    //[alert show];
}

#pragma mark-
#pragma mark- Indicator Delegate
-(void) showHUDLoadingView:(NSString *)strTitle
{
    HUD = [[MBProgressHUD alloc] initWithView:self.window];
    [self.window addSubview:HUD];
    //HUD.delegate = self;
    //HUD.labelText = [strTitle isEqualToString:@""] ? @"Loading...":strTitle;
    HUD.detailsLabelText=[strTitle isEqualToString:@""] ? @"Loading...":strTitle;
    [HUD show:YES];
}

-(void) hideHUDLoadingView
{
    [HUD removeFromSuperview];
    [HUD setHidden:YES];
    [HUD show:NO];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.4;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;    
    [HUD.layer addAnimation:transition forKey:nil];
}

-(void)showToastMessage:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.window
                                              animated:YES];
    
	// Configure for text only and offset down
	hud.mode = MBProgressHUDModeText;
	hud.detailsLabelText = message;
	hud.margin = 10.f;
	hud.yOffset = 150.f;
	hud.removeFromSuperViewOnHide = YES;
	[hud hide:YES afterDelay:2.5];
}

#pragma mark -
#pragma mark - Loading View


-(void)showLoadingWithTitle:(NSString *)title
{
    
    if (viewLoading==nil) {
        
        
        
//        viewLoading=[[UIView alloc]initWithFrame:self.window.bounds];
//        
//        viewLoading.backgroundColor = [UIColor colorWithRed:56/255.0f green:185/255.0f blue:163/255.0f alpha:0.9f];//[UIColor whiteColor];
//        //viewLoading.alpha=0.6f;
//        
//        UIImageView *img=[[UIImageView alloc]initWithFrame:CGRectMake((viewLoading.frame.size.width-88)/2, ((viewLoading.frame.size.height-88)/2)-88, 88, 88)];
//        
//        img.backgroundColor=[UIColor clearColor];
//        
//        img.animationImages=[NSArray arrayWithObjects:[UIImage imageNamed:@"loading_1.png"],[UIImage imageNamed:@"loading_2.png"],[UIImage imageNamed:@"loading_3.png"], nil];
//        img.animationDuration = 1.0f;
//        img.animationRepeatCount = 0;
//        [img startAnimating];
//        [viewLoading addSubview:img];
//        
//        UITextView *txt=[[UITextView alloc]initWithFrame:CGRectMake((viewLoading.frame.size.width-250)/2, ((viewLoading.frame.size.height-60)/2), 250, 60)];
//        txt.textAlignment=NSTextAlignmentCenter;
//        txt.backgroundColor=[UIColor clearColor];
//        txt.text=[title uppercaseString];
//        txt.font=[UIFont systemFontOfSize:16];
//        txt.userInteractionEnabled=FALSE;
//        txt.scrollEnabled=FALSE;
//        txt.textColor=[UIColor whiteColor];
//        txt.font=[UIFont fontWithName:@"OpenSansLight" size:13.0f];
//        [viewLoading addSubview:txt];
        
        //blackOverlayView behind of loading sign
        backgroundOverlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 700)];
        backgroundOverlayView.layer.backgroundColor = [UIColor blackColor].CGColor;
        backgroundOverlayView.layer.opacity = 0.5;
        
        
        viewLoading=[[UIView alloc]initWithFrame:self.window.bounds];
        //viewLoading = [[UIView alloc]initWithFrame:CGRectMake(120, 200, 80, 80)];
        //viewLoading.backgroundColor = [UIColor colorWithRed:65/255.0f green:183/255.0f blue:154/255.0f alpha:0.9f];
        //viewLoading.layer.cornerRadius = 5;
        viewLoading.hidden = YES;
        
        /*UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityView.center = CGPointMake(viewLoading.frame.size.width / 2.0, 35);
        [activityView startAnimating];
        activityView.tag = 100;
        [viewLoading addSubview:activityView];*/
        [SVProgressHUD show];
        
        //[SVProgressHUD setBackgroundLayerColor:[[UIColor redColor] colorWithAlphaComponent:0.4]];
        //[SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeCustom];
        
        
        UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
        lblLoading.text = NSLocalizedString(@"Loading", nil);
        lblLoading.textColor = [UIColor whiteColor];
        lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:12];
        lblLoading.textAlignment = NSTextAlignmentCenter;
        [viewLoading addSubview:lblLoading];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        transition.delegate = self;
        [self.viewLoading.layer addAnimation:transition forKey:nil];
        viewLoading.hidden = YES;
    }
    
    [self.window addSubview: backgroundOverlayView];
    [self.window addSubview: viewLoading];
    [self.window bringSubviewToFront:viewLoading];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    
    [backgroundOverlayView.layer addAnimation:transition forKey:nil];
    [self.viewLoading.layer addAnimation:transition forKey:nil];
}



-(void)hideLoadingView
{
    if (viewLoading) {
        [SVProgressHUD dismiss];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.5;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        transition.delegate = self;
        
        [backgroundOverlayView.layer addAnimation:transition forKey:nil];
        [viewLoading.layer addAnimation:transition forKey:nil];
        
        backgroundOverlayView.hidden = YES;
        viewLoading.hidden = YES;
        
        [backgroundOverlayView removeFromSuperview];
        [viewLoading removeFromSuperview];
        
        backgroundOverlayView = nil;
        viewLoading = nil;
    }
    
}

#pragma mark-
#pragma mark- Test Internet


- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return (networkStatus != NotReachable);
}

#pragma mark -
#pragma mark - sharedAppDelegate

+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

#pragma mark-
#pragma mark- Font Descriptor

-(id)setBoldFontDiscriptor:(id)objc
{
    if([objc isKindOfClass:[UIButton class]])
    {
        UIButton *button=objc;
        button.titleLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:13.0f];
        return button;
    }
    else if([objc isKindOfClass:[UITextField class]])
    {
        UITextField *textField=objc;
        textField.font = [UIFont fontWithName:@"OpenSans-Bold" size:13.0f];
        return textField;
        
        
    }
    else if([objc isKindOfClass:[UILabel class]])
    {
        UILabel *lable=objc;
        lable.font = [UIFont fontWithName:@"OpenSans-Bold" size:13.0f];
        return lable;
    }
    return objc;
}

//Country Specific Request setup
- (void)getDeviceLanguage {
    NSLocale *locale = [NSLocale currentLocale];
    NSString *user_country = [locale displayNameForKey:NSLocaleIdentifier value:[locale localeIdentifier]];
    NSString * dev_language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSLog(@"current device language : %@", dev_language);
    
//    NSLog(@"user's country : %@", user_country);
//    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
//    [pref setObject:dev_language forKey:PREF_DEVICE_LANGUAGE];
//    [pref synchronize];

}

#pragma mark - CLLocationManager delegate methods
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError : %@", error);
    //    UIAlertView *errorAlert = [[UIAlertView alloc]
    //                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    NSLog(@"didUpdateToLocation : %@", [locations lastObject]);
    CLLocation *currentLocation = [locations lastObject];
    
    NSLog(@"Resolving the address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error){
        //        NSLog(@"Found placemarks : %@, error : %@", placemarks, error);
        //        NSString *address;
        if(error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            //            address = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
            //                       placemark.subThoroughfare,
            //                       placemark.thoroughfare,
            //                       placemark.postalCode,
            //                       placemark.locality,
            //                       placemark.administrativeArea,
            //                       placemark.country];
            
            
            
            NSLog(@"COUNTRY ^^^^^^ %@",placemark.country);
            NSString *country = placemark.country;
            
            NSUserDefaults *pref = [[ NSUserDefaults alloc] init];
            [pref setObject:country forKey:@"CURRENT_COUNTRY"];
            [pref synchronize];
            
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    }];
    
}


@end
