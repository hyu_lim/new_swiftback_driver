
#import "AboutVC.h"
#import "PickMeUpMapVC.h"
#import "ArrivedMapVC.h"
#import "FeedBackVC.h"


@interface AboutVC ()
{
    NSMutableDictionary *dictAbout;
}

@end

@implementation AboutVC{
    UIView *backgroundOverlayView;
    UIView* loadingView;
}

#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    [super setBackBarItem];
    [self layoutSetup];
    [self customSetup];
    [self setLocalizedStrings];
    
    
    self.btnMenu.titleLabel.font=[UberStyleGuide fontRegular];
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
    [self.viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    NSString *fullURL = @"http://swiftback.com/aboutdriver/";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webViewAbout loadRequest:requestObj];
}

- (void)setLocalizedStrings {
    [self.lblNavTitle setText:NSLocalizedString(@"ABOUT", nil)];
    [self.btnTutorial setTitle:NSLocalizedString(@"WALKTHROUGH", nil) forState:UIControlStateNormal];
}

- (void)customSetup {
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

- (void)layoutSetup {
    
    //blackOverlayView behind of loading sign
    backgroundOverlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 700)];
    backgroundOverlayView.layer.backgroundColor = [UIColor blackColor].CGColor;
    backgroundOverlayView.layer.opacity = 0.5;
    
    //activity indicator for web view
    loadingView = [[UIView alloc]initWithFrame:CGRectMake(120, 200, 80, 80)];
    loadingView.backgroundColor = [UIColor colorWithRed:56/255.0f green:185/255.0f blue:163/255.0f alpha:0.9f];
    loadingView.layer.cornerRadius = 5;
    
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
    [activityView startAnimating];
    activityView.tag = 100;
    [loadingView addSubview:activityView];
    
    UILabel* lblLoading = [[UILabel alloc]initWithFrame:CGRectMake(0, 48, 80, 30)];
    lblLoading.text = NSLocalizedString(@"LOADING", nil);
    lblLoading.textColor = [UIColor whiteColor];
    lblLoading.font = [UIFont fontWithName:lblLoading.font.fontName size:12];
    lblLoading.textAlignment = NSTextAlignmentCenter;
    [loadingView addSubview:lblLoading];
    
    [self.view addSubview:backgroundOverlayView];
    [self.view addSubview:loadingView];
    [self.view bringSubviewToFront:loadingView];
    _webViewAbout.delegate = self;
    
    //Support button
    _btnTutorial.layer.cornerRadius = 5;
    _btnTutorial.layer.borderWidth = 1;
    _btnTutorial.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //rectangle background Rect
    _backgroudRect.layer.cornerRadius = 10;
    _backgroudRect.layer.borderWidth = 1;
    _backgroudRect.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroudRect.layer.shadowRadius = 5.0;
    _backgroudRect.layer.shadowOpacity = 0.4;
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //blurView
    _blurView.layer.cornerRadius = 10;
    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    
    _webViewAbout.scrollView.bounces = NO;

}

- (void)webViewDidFinishLoad:(UIWebView *)webViewAbout {
    [backgroundOverlayView setHidden:YES];
    [loadingView setHidden:YES];
}

- (void)webViewDidStartLoad:(UIWebView *)webViewAbout {
    [backgroundOverlayView setHidden:NO];    
    [loadingView setHidden:NO];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scrollView setContentSize:CGSizeMake(320, 700)];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - ViewLife Cycle

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Button Method

- (IBAction)backBtnPressed:(id)sender
{
    NSArray *currentControllers = self.navigationController.viewControllers;
    NSMutableArray *newControllers = [NSMutableArray
                                      arrayWithArray:currentControllers];
    UIViewController *obj=nil;
    
    for (int i=0; i<newControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if ([vc isKindOfClass:[FeedBackVC class]])
        {
            obj = (FeedBackVC *)vc;
        }
        else if ([vc isKindOfClass:[ArrivedMapVC class]])
        {
            obj = (ArrivedMapVC *)vc;
        }
        else if ([vc isKindOfClass:[PickMeUpMapVC class]])
        {
            obj = (PickMeUpMapVC *)vc;
        }
        
    }
    [self.navigationController popToViewController:obj animated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickTutorial:(id)sender {
}

@end
