
#import "TutorialVC.h"

@interface TutorialVC ()
{
    int i;
}

@end

@implementation TutorialVC

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    i=1;
    
    self.img.image=[UIImage imageNamed:@"img1.png"];
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(i>1)
    {
        i--;
        self.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"img%d.png",i]];
    }
    else
    {
        i=1;
        self.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"img%d.png",i]];
    }
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    if(i<4)
    {
        i++;
        self.img.image=[UIImage imageNamed:[NSString stringWithFormat:@"img%d.png",i]];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
