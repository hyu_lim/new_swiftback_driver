
#import "BaseVC.h"

@interface SettingVC : UITableViewController


@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailableText;
@property (weak, nonatomic) IBOutlet UILabel *lblSoundText;
@property (weak, nonatomic) IBOutlet UILabel *lblNotiOnDemandText;
@property (weak, nonatomic) IBOutlet UILabel *lblNotiForSuggestion;
@property (weak, nonatomic) IBOutlet UILabel *lblNotiAdvanceText;
@property (weak, nonatomic) IBOutlet UILabel *lblNotiChatText;
@property (weak, nonatomic) IBOutlet UILabel *lblYes;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailable;
@property (weak, nonatomic) IBOutlet UILabel *lblSound;
@property (weak, nonatomic) IBOutlet UILabel *lblSoundtext;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UISwitch *swAvailable;
@property (weak, nonatomic) IBOutlet UISwitch *swSound;
@property (weak, nonatomic) IBOutlet UISwitch *swRideNow;
@property (weak, nonatomic) IBOutlet UISwitch *swAdvanceRide;
@property (weak, nonatomic) IBOutlet UISwitch *swChat;
@property (weak, nonatomic) IBOutlet UISwitch *swSuggestion;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)setState:(id)sender;
- (IBAction)setRideNowState:(id)sender;
- (IBAction)setRideLaterState:(id)sender;
- (IBAction)setChatState:(id)sender;
- (IBAction)setSound:(id)sender;
- (IBAction)setSuggestionState:(id)sender;

@end
