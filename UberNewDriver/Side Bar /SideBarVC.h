
#import "BaseVC.h"
#import "SWRevealViewController.h"
#import "PickMeUpMapVC.h"

//@protocol timerProtocol <NSObject>
//
//-(void)invalidateTimer;
//
//@end

@interface SideBarVC : BaseVC <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>
{
    BOOL internet;
}

//@property (strong,nonatomic) id <timerProtocol> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UIImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (nonatomic,strong) PickMeUpMapVC *ViewObj;
@property (weak, nonatomic) IBOutlet UIButton *btnToggle;

@end
