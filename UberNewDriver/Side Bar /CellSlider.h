

#import <UIKit/UIKit.h>

@interface CellSlider : UITableViewCell
{
    id cellData;
    id cellParent;
}

@property (nonatomic,weak) IBOutlet UILabel *lblName;
@property (nonatomic,weak) IBOutlet UILabel *lblChatCounter;
@property (nonatomic,weak) IBOutlet UIImageView *imgIcon;

-(void)setCellData:(id)data withParent:(id)parent;

@end
