
#import "SettingVC.h"
#import "PickMeUpMapVC.h"
#import "ArrivedMapVC.h"
#import "FeedBackVC.h"

@interface SettingVC ()
{
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
}

@end

@implementation SettingVC
@synthesize swAvailable,swSound;

- (void)viewDidLoad
{
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
//    [super setBackBarItem];
    [self customFont];
    [self customSetup];
    [self.viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self layoutSetup];
    [self setLocalizedStrings];
    [self checkState];
}

- (void)setLocalizedStrings {

    [self.lblNavTitle setText:NSLocalizedString(@"SETTINGS", nil)];
    [self.lblAvailableText setText:NSLocalizedString(@"AVAILABLE_TO_DRIVE", nil)];
    [self.lblSoundText setText:NSLocalizedString(@"SOUND", nil)];
    [self.lblNotiOnDemandText setText:NSLocalizedString(@"NOTI_FOR_ON_DEMAND_REQUEST", nil)];
    [self.lblNotiAdvanceText setText:NSLocalizedString(@"NOTI_FOR_ADVANCED_REQUEST", nil)];
    [self.lblNotiChatText setText:NSLocalizedString(@"NOTI_FOR_CHAT", nil)];
    [self.lblNotiForSuggestion setText:NSLocalizedString(@"NOTI_FOR_SUGGESTION", nil)];
}

- (void)layoutSetup {
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref synchronize];
    strUserId=[pref objectForKey:PREF_USER_ID];
    strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    self.lblAvailable.text = NSLocalizedString(@"AVAILABLE_TO_DRIVE?", nil);
    //self.lblSound.text=NSLocalizedString(@"ON", nil);
    self.lblSoundtext.text = NSLocalizedString(@"SOUND", nil);
    self.lblYes.text = NSLocalizedString(@"YES", nil);
    // Do any additional setup after loading the view.
    
    
    if ([[pref valueForKey:@"SOUND"] isEqualToString:@"off"])
    {
        self.lblSound.text=NSLocalizedString(@"OFF", nil);
        [swSound setOn:NO animated:NO];
    }
    else
    {
        self.lblSound.text=NSLocalizedString(@"ON", nil);
        [swSound setOn:YES animated:NO];
        [pref setObject:@"on" forKey:@"SOUND"];
    }
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark-
#pragma mark- customFont

-(void)customFont
{
    self.lblAvailable.font=[UberStyleGuide fontSemiBold];
    self.lblYes.font=[UberStyleGuide fontSemiBold];
    self.lblSound.font=[UberStyleGuide fontSemiBold];
    //self..font=[UberStyleGuide fontRegular];
}

-(void)checkState
{
    if([APPDELEGATE connected])
    {
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_CHECKSTATUS,PARAM_ID,strUserId,PARAM_TOKEN,strUserToken];
        NSLog(@"PageURL : %@", pageUrl);
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 NSLog(@"Setting Data= %@",response);
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     if([[response valueForKey:@"is_active"] intValue]==1)
                     {
                         swAvailable.on=YES;
                         self.lblYes.text = NSLocalizedString(@"YES", nil);
                     }
                     else
                     {
                         swAvailable.on=NO;
                         self.lblYes.text = NSLocalizedString(@"NO", nil);
                     }
					 if ([[response objectForKey:@"ride_now"] intValue] == 1) {
						 
						 [self.swRideNow setOn:YES];
						 
					 }
					 else{
						 [self.swRideNow setOn:NO];

					 }
					 if ([[response objectForKey:@"ride_later"] intValue] == 1) {
						 [self.swAdvanceRide setOn:YES];
					 }
					 else{
						 [self.swAdvanceRide setOn:NO];
					 }
					 
					 if ([[response objectForKey:@"chat_notification"] intValue] == 1) {
						 [self.swChat setOn:YES];
					 }
					 else{
						 [self.swChat setOn:NO];
					 }
                     
                     if ([[response objectForKey:@"suggestion_push"] intValue] == 1) {
                         [self.swSuggestion setOn:YES];
                     }
                     else{
                         [self.swSuggestion setOn:NO];
                     }
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)setStatus
{
    if([APPDELEGATE connected])
    {
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_TOGGLE withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             
             NSLog(@"Availability = %@",response);
             if (response)
             {
                 if([[response valueForKey:@"success"]intValue]==1)
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"AVAILABILITY_UODATE", nil)];
                 }
             }
             
         }];
    }
}

-(void)toggleNotfications:(NSString*)rideType {
	
	if([APPDELEGATE connected])
	{
		NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
		
		[dictparam setObject:strUserId forKey:PARAM_ID];
		[dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        
        if([rideType isEqualToString:@"ride_now"]) {
            [dictparam setObject:@"1" forKey:rideType];
//            if([self.swRideNow isOn]) {
//                [dictparam setObject:@"1" forKey:rideType];
//            } else {
//                [dictparam setObject:@"0" forKey:rideType];
//            }
        }else if([rideType isEqualToString:@"ride_later"]) {
            [dictparam setObject:@"1" forKey:rideType];
//            if([self.swAdvanceRide isOn]) {
//                [dictparam setObject:@"1" forKey:rideType];
//            } else {
//                [dictparam setObject:@"0" forKey:rideType];
//            }
        }else if([rideType isEqualToString:@"chat_notification"]) {
            [dictparam setObject:@"1" forKey:rideType];
//            if([self.swChat isOn]) {
//                [dictparam setObject:@"1" forKey:rideType];
//            } else {
//                [dictparam setObject:@"0" forKey:rideType];
//            }
        }else if([rideType isEqualToString:@"suggestion_push"]) {
            if([self.swSuggestion isOn]) {
                [dictparam setObject:@"1" forKey:rideType];
            } else {
                [dictparam setObject:@"0" forKey:rideType];
            }
        }
        
        NSLog(@"DictParam : %@", dictparam);
        
//		[dictparam setObject:@"1" forKey:rideType];
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
		[afn getDataFromPath:FILE_TOGGLE_RIDE_NOTIFICATION withParamData:dictparam withBlock:^(id response, NSError *error)
		 {
			 
			 NSLog(@" Ride now state = %@",response);
			 if (response)
			 {
				 if([[response valueForKey:@"success"]intValue]==1)
				 {
					 [APPDELEGATE showToastMessage:NSLocalizedString(@"Notifications updated", nil)];
				 }
			 }
			 
		 }];
	}

}

- (IBAction)backBtnPressed:(id)sender
{
    NSArray *currentControllers = self.navigationController.viewControllers;
    NSMutableArray *newControllers = [NSMutableArray
                                      arrayWithArray:currentControllers];
    UIViewController *obj=nil;
    
    for (int i=0; i<newControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if ([vc isKindOfClass:[FeedBackVC class]])
        {
            obj = (FeedBackVC *)vc;
        }
        else if ([vc isKindOfClass:[ArrivedMapVC class]])
        {
            obj = (ArrivedMapVC *)vc;
        }
        else if ([vc isKindOfClass:[PickMeUpMapVC class]])
        {
            obj = (PickMeUpMapVC *)vc;
        }
    }
    [self.navigationController popToViewController:obj animated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)setState:(id)sender
{
//    if ([swAvailable isOn]==NO)
//    {
//        self.lblYes.text=NSLocalizedString(@"NO", nil);
//        
//    }
//    else
//    {
//        self.lblYes.text=NSLocalizedString(@"YES", nil);
//    }
    [self setStatus];
}

- (IBAction)setRideNowState:(id)sender {
    
    [self toggleNotfications:@"ride_now"];
}

- (IBAction)setRideLaterState:(id)sender {

    [self toggleNotfications:@"ride_later"];
}

- (IBAction)setChatState:(id)sender {

	[self toggleNotfications:@"chat_notification"];
}

- (IBAction)setSound:(id)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    if ([swSound isOn]==NO)
    {
        self.lblSound.text=NSLocalizedString(@"OFF", nil);
        [pref setObject:@"off" forKey:@"SOUND"];
        [pref synchronize];
    }
    else
    {
        self.lblSound.text=NSLocalizedString(@"ON", nil);
        [pref setObject:@"on" forKey:@"SOUND"];
        [pref synchronize];
    }
}

- (IBAction)setSuggestionState:(id)sender {

    [self toggleNotfications:@"suggestion_push"];
}

@end
