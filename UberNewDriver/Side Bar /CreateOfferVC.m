//
//  CreateOfferVC.m
//  SwiftBack Driver
//
//  Created by Elluminati on 18/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "CreateOfferVC.h"
#import "SWRevealViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "Social/Social.h"
#import <pop/POP.h>
//#import "FBSDKShareLinkContent.h"



@interface CreateOfferVC ()
@property (nonatomic, strong) SLComposeViewController *facebookComposerSheet;
@end

@implementation CreateOfferVC{
    NSString *weeklyValue,*facebookShare;
    NSString *twitterShare;
    NSString *whatsappShare;
    NSString *currencySign;
    NSString *currentCountry;
}

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    [self getUserLocation];
    [self customSetup];
    [self layoutSetup];
    [self setLocalizedStrings];
    
    [FBSDKAppEvents logEvent:@"rideOfferLoaded"];
    
    NSLog(@"ARRUSER : %@", arrUser);
    
    //View Controller Clipping
    weeklyValue = @"0";
    
    twitterShare=@"0";
    whatsappShare=@"0";
    facebookShare=@"0";
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    [self.scrollView setScrollEnabled:YES];
    [self.scrollView setContentSize:CGSizeMake(270, 480)];
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        viewForGreenOverlay.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = viewForGreenOverlay.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [viewForGreenOverlay addSubview:blurEffectView];
        
    } else {
        viewForGreenOverlay.backgroundColor = [UIColor whiteColor];
    }
    
    
    
    
    
}

- (void)setLocalizedStrings {
    [estimateNextBtn setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateNormal];
    [self.lblDistanceTitle setText:NSLocalizedString(@"DISTANCE", nil)];
    [self.lblOfferPriceTitle setText:NSLocalizedString(@"OFFER_PRICE", nil)];
    [self.lblCollectText setText:NSLocalizedString(@"COLLECT_PAYEMENT_TEXT", nil)];
    
    [dateNextBtn setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateNormal];
    [self.lblEnterDateText setText:NSLocalizedString(@"ENTER_DATE_TEXT", nil)];
    [self.lblTravellingWeeklyText setText:NSLocalizedString(@"TRAVELLING_WEEKLY_TEXT", nil)];
    [self.lblNoOfSeatTitle setText:NSLocalizedString(@"NO_OF_SEATS", nil)];
    [self.lblNoteTitle setText:NSLocalizedString(@"NOTES", nil)];
    [self.lblEnterDateText2 setText:NSLocalizedString(@"ENTER_DATE_TEXT", nil)];
    [noteNextBtn setTitle:NSLocalizedString(@"NEXT", nil) forState:UIControlStateNormal];
    
    [self.btnEdit setTitle:NSLocalizedString(@"EDIT", nil) forState:UIControlStateNormal];
    [self.lblRideOfferTitle setText:NSLocalizedString(@"RIDE_OFFER", nil)];
    [self.lblDepartureDateTitle setText:NSLocalizedString(@"DEPARTURE_DATE", nil)];
    [self.lblDepartureTimeTitle setText:NSLocalizedString(@"DEPARTURE_TIME", nil)];
    [self.lblDistanceTitle2 setText:NSLocalizedString(@"DISTANCE", nil)];
    [self.lblSeatOfferedText setText:NSLocalizedString(@"SEATS_OFFERED", nil)];
    [self.lblOfferPriceTitle2 setText:NSLocalizedString(@"OFFER_PRICE", nil)];
    [self.lblShareText setText:NSLocalizedString(@"SHARE_ON_FACEBOOK", nil)];
    [self.lblNotesTitle setText:NSLocalizedString(@"NOTES", nil)];
    [confirmOfferBtn setTitle:NSLocalizedString(@"CONFIRM", nil) forState:UIControlStateNormal];
    [self.lblPaymentDescriptionText setText:NSLocalizedString(@"PAYEMENT_DESCRIPTION_TEXT", nil)];
    [self.lblNavTitle setText:NSLocalizedString(@"OFFER_A_RIDE", nil)];
}

- (void)layoutSetup {
    
    [datepicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    
    SEL selector = NSSelectorFromString(@"setHighlightsToday:");
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    [invocation setArgument:&no atIndex:2];
    [invocation invokeWithTarget:datepicker];
    
    [datepicker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:15*60]];
    [datepicker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*7]];
    
    //    [datepicker addTarget:self
    //                          action:@selector(setMinimumDateForFutureRequest)
    //                forControlEvents:UIControlEventValueChanged];
    

    CLLocationCoordinate2D current;
    current.latitude=[struser_lati doubleValue];
    current.longitude=[struser_longi doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:current.latitude
                                                            longitude:current.longitude
                                                                 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:viewForGoogleMap.bounds camera:camera];
    mapView_.myLocationEnabled = NO;
    [viewForGoogleMap addSubview:mapView_];
    mapView_.delegate=self;
    
    driverMarker = [[GMSMarker alloc] init];
    sourceMarker = [[GMSMarker alloc] init];
    destinationMarker = [[GMSMarker alloc] init];

    //blurViewForAddress
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.blurViewForAddress.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.blurViewForAddress.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.blurViewForAddress addSubview:blurEffectView];
    }
    else {
        self.blurViewForAddress.backgroundColor = [UIColor blackColor];
    }
    
    //viewForEstimate
    viewForEstimate.layer.cornerRadius = 10;
    viewForEstimate.clipsToBounds = YES;
    viewForEstimate.layer.shadowRadius = 4.0;
    viewForEstimate.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForEstimate.layer.shadowOpacity = 0.7;

    //viewForOfferInfo
    viewForOfferInfo.layer.cornerRadius = 10;
    viewForOfferInfo.clipsToBounds = YES;
    
    //estimateNextBtn
    estimateNextBtn.layer.cornerRadius = 19;
    estimateNextBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    estimateNextBtn.layer.borderWidth = 1;
    
    //viewForDate
    viewForDate.layer.cornerRadius = 10;
    viewForDate.clipsToBounds = YES;
    viewForDate.layer.shadowRadius = 4.0;
    viewForDate.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForDate.layer.shadowOpacity = 0.7;
    
    //viewForDatePicker
    viewForDatePicker.layer.cornerRadius = 10;
    viewForDatePicker.clipsToBounds = YES;
    
    //ViewForWeekly
    viewForWeekly.layer.cornerRadius = 10;
    viewForWeekly.clipsToBounds = YES;
    
    //viewForAddress
    viewForAddress.layer.borderColor = [UIColor colorWithRed:85.f/255.f green:85.f/255.f blue:85.f/255.f alpha:0.1].CGColor;
    viewForAddress.layer.borderWidth = 0.5;
    
    //datePicker
    [datepicker setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    //dateNextBtn
    dateNextBtn.layer.cornerRadius = 19;
    dateNextBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    dateNextBtn.layer.borderWidth = 1;
    dateNextBtn.clipsToBounds = YES;

    //viewForNotes
    viewForNotes.layer.cornerRadius = 10;
    viewForNotes.clipsToBounds = YES;
    viewForNotes.layer.shadowRadius = 4.0;
    viewForNotes.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForNotes.layer.shadowOpacity = 0.7;
    
    //viewForNoOfPassenger
    viewForNoOfPassenger.layer.cornerRadius = 10;
    viewForNoOfPassenger.clipsToBounds = YES;
    
    //noteNextBtn
    noteNextBtn.layer.cornerRadius = 19;
    noteNextBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    noteNextBtn.layer.borderWidth = 1;
    noteNextBtn.clipsToBounds = YES;
    
    //txtNotes
    txtNotes.layer.cornerRadius = 15;
    txtNotes.clipsToBounds = YES;
    txtNotes.layer.borderColor = [UIColor whiteColor].CGColor;
    txtNotes.layer.borderWidth = 1;
    
    //viewForConfirm
    viewForConfirm.layer.cornerRadius = 15;
    viewForConfirm.clipsToBounds = YES;
    //viewForConfirm.layer.borderWidth = 1;
    //viewForConfirm.layer.borderColor = [UIColor lightGrayColor].CGColor;
    viewForConfirm.layer.shadowRadius = 4.0;
    viewForConfirm.layer.shadowColor = [UIColor grayColor].CGColor;
    viewForConfirm.layer.shadowOpacity = 0.7;
    
    
    
    
    //viewForConfirm
    //tableForPlaces.layer.cornerRadius = 15;
    tableForPlaces.clipsToBounds = YES;
    
    
    //confirmOfferBtn
    confirmOfferBtn.layer.cornerRadius = 19;
    confirmOfferBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    confirmOfferBtn.layer.borderWidth = 1;
    confirmOfferBtn.clipsToBounds = YES;
    
    //resize switch for weekly setup
    swForWeekly.transform = CGAffineTransformMakeScale(0.6, 0.6);
    
    //resize switch for facebook sharing
    self.FacebookShareSwitch.transform = CGAffineTransformMakeScale(0.7, 0.7);
    
    txtSource.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"FROM_DESTINATION", nil) attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:0.5]}];
    txtDestination.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"TO_DESTINATION", nil) attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:255.f/255.f green:255.f/255.f blue:255.f/255.f alpha:0.5]}];
}

//menuBtn
-(void)customSetup{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

-(void)reloadDatePicker
{
    [datepicker setMinimumDate:[[NSDate date] dateByAddingTimeInterval:15*60]];
    [datepicker setMaximumDate:[[NSDate date] dateByAddingTimeInterval:60*60*24*7]];
    [datepicker reloadInputViews];
}

#pragma mark-
#pragma mark- Get Location

-(void)getUserLocation
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil)
    {
        struser_lati=[NSString stringWithFormat:@"%.8f",currentLocation.coordinate.latitude];//[NSString stringWithFormat:@"%.8f",+37.40618700];
        struser_longi=[NSString stringWithFormat:@"%.8f",currentLocation.coordinate.longitude];//[NSString stringWithFormat:@"%.8f",-122.18845228];
    }
    
//    CLLocationCoordinate2D current;
//    current.latitude=[struser_lati doubleValue];
//    current.longitude=[struser_longi doubleValue];
//    [mapView_ animateToLocation:current];
    
    
    driverMarker.position = currentLocation.coordinate;
    driverMarker.icon = [UIImage imageNamed:@"pin_driver"];
    driverMarker.map = mapView_;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor darkGrayColor];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    if([placeMarkArr count]>0)
    {
        NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"]; // AUTOCOMPLETE API
        
        // cell.lblTitle.text=currentPlaceMark.name;
        cell.textLabel.text = formatedAddress;
        
        cell.textLabel.font = [UberStyleGuide fontRegular];
    }
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
    tableForPlaces.hidden=YES;
    
    if([isSource isEqualToString:@"1"])
    {
        isList = YES;
        placeID = [aPlacemark objectForKey:@"place_id"];
        
        txtSource.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        
         [self textFieldShouldReturn:txtSource];
        
        [self searchLocationByPlaceID];
    }
    else if([isSource isEqualToString:@"2"])
    {
        placeID = [aPlacemark objectForKey:@"place_id"];
        isList = YES;
        
        txtDestination.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:txtDestination];
        
        [self searchLocationByPlaceID];

    }
    
    // [self textFieldShouldReturn:nil];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeMarkArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

// Address Events
#pragma mark
#pragma mark - Search Address in Google API

-(IBAction)onClickSearch:(UITextField*)sender{
    
    [placeMarkArr removeAllObjects];
    tableForPlaces.hidden=YES;
    
    NSString *address;
    
    if (sender == txtSource) {
        address = txtSource.text;
        isSource = @"1";
    }
    else{
        address = txtDestination.text;
        isSource = @"2";
    }
    
    
    if(address == nil)
        tableForPlaces.hidden=YES;
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    //[dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:address forKey:@"input"]; // AUTOCOMPLETE API
    [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
    [dictParam setObject:GOOGLE_KEY forKey:@"key"];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             //NSArray *arrAddress=[response valueForKey:@"results"];
             NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
             
             NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
             
             if ([arrAddress count] > 0)
             {
                 
                 CATransition *transition = [CATransition animation];
                 
                 transition.duration = 0.2;
                 
                 transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                 
                 transition.type = kCATransitionFade;
                 
                 transition.delegate = self;
                 
                 [tableForPlaces.layer addAnimation:transition forKey:nil];
                 

                 
                 tableForPlaces.hidden=NO;
                 
                 placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                 //[placeMarkArr addObject:Placemark]; o
                 [tableForPlaces reloadData];
                 
                 if(arrAddress.count==0)
                 {
                     CATransition *transition = [CATransition animation];
                     
                     transition.duration = 0.2;
                     
                     transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                     
                     transition.type = kCATransitionFade;
                     
                     transition.delegate = self;
                     
                     [tableForPlaces.layer addAnimation:transition forKey:nil];
                     tableForPlaces.hidden=YES;
                 }
             }
             
         }
         
     }];
    
}

#pragma mark
#pragma mark - Search Location by Place

-(void)searchLocationByPlaceID{
    
    [APPDELEGATE showLoadingWithTitle:@"Loading"];
    AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [helper getLocationFromPlaceIdwithParamData:placeID withBlock:^(id response, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        if (response)
        {
            if ([[response objectForKey:@"status"] isEqualToString:@"OK"]) {
                NSDictionary *dictLocation=[[[response valueForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"];
                
                if ([isSource isEqualToString:@"1"]) {
                    
                    sourceLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                    sourceLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                    
                    sourcecoordinate.latitude=[sourceLatitude doubleValue];
                    sourcecoordinate.longitude=[sourceLongitude doubleValue];
                    
                        sourceMarker.position = sourcecoordinate;
                        sourceMarker.icon=[UIImage imageNamed:@"pin_client_org"];
                        sourceMarker.map = mapView_;
                    
                    [mapView_ animateToLocation:sourcecoordinate];
                    if(txtDestination.text.length>0)
                    {
                        [self getDistanceAndCost];
                    }
                    
                }
                else{
                    destinationLatitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lat"]];
                    destinationLongitude=[NSString stringWithFormat:@"%@",[dictLocation valueForKey:@"lng"]];
                    
                    destinationcoordinate.latitude=[destinationLatitude doubleValue];
                    destinationcoordinate.longitude=[destinationLongitude doubleValue];
                    
                    destinationMarker.position = destinationcoordinate;
                    destinationMarker.icon=[UIImage imageNamed:@"pin_destination"];
                    destinationMarker.map = mapView_;
                    
                    if(txtSource.text.length<1)
                    {
                        [txtSource becomeFirstResponder];
                    }
                    else
                    {
                        [self getDistanceAndCost];
                    }
                }
            }
        }
    }];
}

#pragma mark
#pragma mark -  Get Distance & Cost

-(void)getDistanceAndCost
{
    if ([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Please wait..."];
        NSMutableDictionary *dictparam = [[NSMutableDictionary alloc]init];
        [dictparam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictparam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictparam setObject:sourceLatitude forKey:PARAM_SOURCE_LATITUDE];
        [dictparam setObject:sourceLongitude forKey:PARAM_SOURCE_LONGITUDE];
        [dictparam setObject:destinationLatitude forKey:PARAM_DESTIANTION_LATITUDE];
        [dictparam setObject:destinationLongitude forKey:PARAM_DESTIANTION_LONGITUDE];
        
   
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_GET_USER_INFO withParamData:dictparam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response)
            {
                if ([[response valueForKey:@"success"] boolValue])
                {
                    NSDictionary *details = [response valueForKey:@"details"];
                    
                    if ([[details valueForKey:@"distance"] integerValue] > 0) {
                        [lblEstimateDistance setText:[NSString stringWithFormat:@"%.2f Km",[[details valueForKey:@"distance"] doubleValue]]];
                        
                        if([response objectForKey:@"currency_sign"]) {
                            currencySign = [response objectForKey:@"currency_sign"];
                            NSLog(@"Sign : %@", currencySign);
                        } else {
                            currencySign = @"$";
                        }
                        
                        if([response objectForKey:@"current_country"]) {
                            currentCountry = [response objectForKey:@"current_country"];
                            NSLog(@"CC : %@", currentCountry);
                        } else {
                            currentCountry = @"Singapore";
                        }
                        
                        [lblOfferPrice setText:[NSString stringWithFormat:@"%@%@",currencySign,[details valueForKey:@"total"]]];
                        distance = [NSString stringWithFormat:@"%@",[details valueForKey:@"distance"]];
                        [viewForEstimate setHidden:NO];
                        cost = [NSString stringWithFormat:@"%@",[details valueForKey:@"total"]];
                         [self DrawPath:sourcecoordinate to:destinationcoordinate];
                        
                    }
                    
                    else{
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"NOT_VALID_LOCATION", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                    }
                }
                else{
                    if([response valueForKey:@"error"]){
                        
                        if ([[response valueForKey:@"error"] isEqualToString:@"User Not Found"] || [[response valueForKey:@"error"] isEqualToString:@"Not a valid token"]) {
                            [USERDEFAULT setBool:NO forKey:PREF_IS_LOGIN];
                            [USERDEFAULT removeObjectForKey:PREF_LOGIN_OBJECT];
                            [USERDEFAULT synchronize];
                            [self.revealViewController.navigationController popToRootViewControllerAnimated:YES];
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Login expired" message:@"Please relogin." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alert show];                        }
                        else{
                            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                            [alert show];
                        }
                        
                    }
                    
                }
            }
            else
            {
                
                [viewForEstimate setHidden:YES];
                [txtDestination setText:@""];
                destinationLatitude = @"";
                destinationLongitude = @"";
                destinationMarker = [[GMSMarker alloc]init];
                [txtDestination becomeFirstResponder];
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:@"Please try again." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
                
                
                
                
            }
            
        }];
    }
}

#pragma mark-
#pragma mark- Show Route With Google

-(void)DrawPath:(CLLocationCoordinate2D)f to:(CLLocationCoordinate2D)t
{
    [APPDELEGATE showLoadingWithTitle:@"loading"];
    [mapView_ clear];
    

    GMSMarker *markerOwner = [[GMSMarker alloc] init];
    markerOwner.position = f;
    markerOwner.icon = [UIImage imageNamed:@"pin_client_org"];
    markerOwner.map = mapView_;
    
    GMSMarker *markerDriver = [[GMSMarker alloc] init];
    markerDriver.position = t ;
    
    markerDriver.icon = [UIImage imageNamed:@"pin_destination"];
    markerDriver.map = mapView_;
    
    routes = [self calculateRoute:f to:t];
    
    [self centerMap:routes];
    [APPDELEGATE hideLoadingView];
}

-(NSArray*)calculateRoute:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY];
    //chk
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] ||  [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
    }
    else
    {
        GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
        GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 4.0f;
        singleLine.strokeColor = [UIColor colorWithRed:27.0/255.0 green:151.0/255.0 blue:200.0/255.0 alpha:1.0];
        singleLine.map = mapView_;
        
        routes = json[@"routes"];
        
        
    }
    NSString *points = @"";
    if ([routes count] > 0)
    {
        points=[[[routes objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
        
    }
    
    return [self decodePolyLine:[points mutableCopy]];
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded {
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init] ;
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            if(index<len)
            {
                b = [encoded characterAtIndex:index++] - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            }
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude=[[NSNumber alloc] initWithFloat:lat *1e-5];
        NSNumber *longitude=[[NSNumber alloc] initWithFloat:lng *1e-5];
        
        printf("[%f,", [latitude doubleValue]);
        printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]] ;
        [array addObject:loc];
    }
    
    return array;
}

-(void)centerMap:(NSArray*)locations
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (CLLocation *loc in locations)
    {
        location.latitude = loc.coordinate.latitude;
        location.longitude = loc.coordinate.longitude;
        // Creates a marker in the center of the map.
        bounds = [bounds includingCoordinate:location];
    }
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:20.0f]];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

#pragma mark
#pragma mark - Button events

// Estimate View events

- (IBAction)onClickEstimateNext:(id)sender{
    
    if([[arrUser valueForKey:@"is_approved"]boolValue]) {
        
        [viewForEstimate setHidden:YES];
        [viewForDate setHidden:NO];
        [self reloadDatePicker];
        
    } else {
        UIAlertController *notVerifiedAlert = [UIAlertController alertControllerWithTitle:@"Verification" message:@"You have to be verified to offer a ride." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [notVerifiedAlert addAction:okAction];
        [self presentViewController:notVerifiedAlert animated:YES completion:nil];
    }
}

- (IBAction)onClickMinus:(id)sender{
    
    if ([cost doubleValue] > 0) {
        
        cost = [NSString stringWithFormat:@"%.1f",[cost doubleValue] - 0.5];
        [lblOfferPrice setText:[NSString stringWithFormat:@"$%@",cost]];
        
    }
}

- (IBAction)onClickPlus:(id)sender{
	
	if ([cost doubleValue] < 40) {
        
		cost = [NSString stringWithFormat:@"%.1f",[cost doubleValue] + 0.5];
		[lblOfferPrice setText:[NSString stringWithFormat:@"$%@",cost]];
	}
}

// Date View Events
- (IBAction)onClickDateNext:(id)sender{
    
    [viewForDate setHidden:YES];
    [viewForNotes setHidden:NO];
}

- (IBAction)goBackToEstimate:(id)sender{
    
    [viewForEstimate setHidden:NO];
    [viewForDate setHidden:YES];
}

// Note View Events
- (IBAction)onClickPassengerStepper:(id)sender{
    
    [lblPassengerCount setText:[NSString stringWithFormat:@"%.f",passengerStepper.value]];
}

- (IBAction)onClickNotesNext:(id)sender
{
    [self.FacebookShareSwitch setOn:NO];
    facebookShare=@"0";
    [viewForNotes setHidden:NO];
    [_confirmViewContainer setHidden:NO];
    [_closeAlert setHidden:NO];
    [viewForGreenOverlay setHidden:NO];
    
    
    //animation
    
    CATransition *transition2 = [CATransition animation];
    
    transition2.duration = 0.5;
    
    transition2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition2.type = kCATransitionFade;
    
    transition2.delegate = self;
    
    [viewForGreenOverlay.layer addAnimation:transition2 forKey:nil];
    
    POPSpringAnimation *anim2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerBounds];
    
    anim2.springBounciness = 15;
    anim2.springSpeed = 10;
    
    anim2.fromValue = [NSValue valueWithCGRect:CGRectMake(0, -500, 270, 370)];
    anim2.toValue = [NSValue valueWithCGRect:CGRectMake(0, 0, 270, 370)];
    [_confirmViewContainer.layer setCornerRadius:10];
    [_confirmViewContainer.layer pop_addAnimation:anim2 forKey:@"bounce2"];
    

    
    /*
    
    CATransition *transition = [CATransition animation];
    
    transition.duration = 0.5;
    
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition.type = kCATransitionFade;
    
    transition.delegate = self;
    
    [viewForNotes.layer addAnimation:transition forKey:nil];
    [viewForConfirm.layer addAnimation:transition forKey:nil];
    [viewForGreenOverlay.layer addAnimation:transition forKey:nil];*/
    
    [lblFrom setText:txtSource.text];
    [lblTo setText:txtDestination.text];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"SGT"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//2013-07-15:10:00:00
    strFutureDate = [formatter stringFromDate:datepicker.date];
    
    NSString *strDate = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"dd MMM"];
    NSString *strTime  = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"HH:mm a"];

    [lblDate setText:strDate];
    [lblTime setText:strTime];
    [lblDistance setText:lblEstimateDistance.text];
    [lblPrice setText:lblOfferPrice.text];
    [lblNotes setText:txtNotes.text];
//    [lblSeats setText:lblPassengerCount.text];
    if([lblPassengerCount.text isEqualToString:@"1"]) {
        [lblSeats setText:[NSString stringWithFormat:@"%@ Seat",lblPassengerCount.text]];
    } else {
        [lblSeats setText:[NSString stringWithFormat:@"%@ Seats",lblPassengerCount.text]];
    }
}

- (BOOL)textFieldDidBeginEditing:(UITextField *)textField {
    if(textField == txtNotes){
        [UIView animateWithDuration:0.5 animations:^{
            [viewForNotes setFrame:CGRectMake(viewForNotes.frame.origin.x, 100, viewForNotes.frame.size.width, viewForNotes.frame.size.height)];
        }];
    }
    return YES;
}

- (BOOL)textFieldDidEndEditing:(UITextField *)textField {
    if(textField == txtNotes) {
        [UIView animateWithDuration:0.5 animations:^{
            [viewForNotes setFrame:CGRectMake(viewForNotes.frame.origin.x, 322, viewForNotes.frame.size.width, viewForNotes.frame.size.height)];
        }];
    }
    return YES;
}

- (IBAction)goBackToDateView:(id)sender{
    
    [viewForDate setHidden:NO];
    [viewForNotes setHidden:YES];
    [self reloadDatePicker];
}

// Confirm Offer Events
- (IBAction)onClickEditOffer:(id)sender{
    
    [_confirmViewContainer setHidden:YES];
    [viewForGreenOverlay setHidden:YES];
    [viewForNotes setHidden:NO];
    [viewForDate setHidden: NO];
    
    CATransition *transition2 = [CATransition animation];
    
    transition2.duration = 0.5;
    
    transition2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition2.type = kCATransitionFade;
    
    transition2.delegate = self;
    
    [viewForGreenOverlay.layer addAnimation:transition2 forKey:nil];
    
    POPSpringAnimation *anim2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerBounds];
    
    anim2.springBounciness = 15;
    anim2.springSpeed = 10;
    
    anim2.fromValue = [NSValue valueWithCGRect:CGRectMake(0, 0, 270, 370)];
    anim2.toValue = [NSValue valueWithCGRect:CGRectMake(0, -500, 270, 370)];
    [_confirmViewContainer.layer setCornerRadius:10];
    [_confirmViewContainer.layer pop_addAnimation:anim2 forKey:@"bounce2"];
}

- (IBAction)onClickConfirmOffer:(id)sender{
    
    
    [viewForNotes setHidden:YES];
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    NSLog(@"CURRENT COUNTRY IN CONFIRM OFFER : %@",[pref objectForKey:PREF_COUNTRY]);
    
    if ([APPDELEGATE connected]) {
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:sourceLatitude forKey:PARAM_LATITUDE];
        [dictParam setObject:sourceLongitude  forKey:PARAM_LONGITUDE];
        [dictParam setObject:destinationLatitude forKey:PARAM_DESTIANTION_LATITUDE];
        [dictParam setObject:destinationLongitude  forKey:PARAM_DESTIANTION_LONGITUDE];
        [dictParam setObject:txtSource.text forKey:PARAM_SOURCE_ADDRESS];
        [dictParam setObject:txtDestination.text forKey:PARAM_DESTIANTION_ADDRESS];
        [dictParam setObject:strFutureDate forKey:DATE_TIME];
        [dictParam setObject:weeklyValue forKey:PARAM_WEEKLY];
        [dictParam setObject:distance forKey:PARAM_DISTANCE];
        [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:[pref objectForKey:PREF_COUNTRY] forKey:PARAM_CURRENT_COUNTRY];
        [dictParam setObject:cost forKey:PARAM_TOTAL];
        
        [dictParam setObject:[NSString stringWithFormat:@"%d",(int)passengerStepper.value] forKey:PARAM_NO_OF_PASSENGER];
        [dictParam setObject:txtNotes.text forKey:PARAM_NOTE];
        
        NSLog(@"DICTPARAM : %@", dictParam);
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_OFFER_RIDE withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response) {
                if ([[response objectForKey:@"success"] boolValue])
                {
                    if([facebookShare isEqualToString:@"1"])
                    {
                        [self FacebookShare];
                        
                    }
                    
                    if([twitterShare isEqualToString:@"1"])
                    {
                        [self twitterShare];
                    }
                    
                    else
                    {
                        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Offer created successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                        [alert show];
                        
                        if ([self.delegate respondsToSelector:@selector(rideOfferCreated)]) {
                            [self.delegate rideOfferCreated];
                        }
                        
                        [self.navigationController popViewControllerAnimated:YES];
                    }
                }
                else{
                    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Error occured" message:@"Please try again." delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                    [alert show];
                }
            }
        }];
    } else {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)onClickWeekly:(id)sender {
    
    if([swForWeekly isOn]) {
        weeklyValue = @"1";
    } else {
        weeklyValue = @"0";
    }
}


//backBtn
- (IBAction)onClickCancelOffer:(id)sender {
    UIAlertController *offerAlert = [UIAlertController alertControllerWithTitle:@"" message:@"Are you sure to cancel ride offer?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [_confirmViewContainer setHidden:YES];
        [viewForGreenOverlay setHidden:YES];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [offerAlert addAction:okAction];
    [offerAlert addAction:cancelAction];
    
    [self presentViewController:offerAlert animated:YES completion:nil];
    
    CATransition *transition2 = [CATransition animation];
    
    transition2.duration = 0.5;
    
    transition2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition2.type = kCATransitionFade;
    
    transition2.delegate = self;
    
    [viewForGreenOverlay.layer addAnimation:transition2 forKey:nil];
    
    POPSpringAnimation *anim2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerBounds];
    
    anim2.springBounciness = 15;
    anim2.springSpeed = 10;
    
    anim2.fromValue = [NSValue valueWithCGRect:CGRectMake(0, 0, 270, 370)];
    anim2.toValue = [NSValue valueWithCGRect:CGRectMake(0, -500, 270, 370)];
    [_confirmViewContainer.layer setCornerRadius:10];
    [_confirmViewContainer.layer pop_addAnimation:anim2 forKey:@"bounce2"];
}

- (IBAction)onClickBack:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 
#pragma mark - Faceboook Share 

- (IBAction)onClickFacebookShare:(id)sender
{
    if([self.FacebookShareSwitch isOn])
    {
        facebookShare=@"1";
    }
    else
    {
        facebookShare=@"0";
    }
}

-(void)FacebookShare
{
    
    
    
    [viewForScreenshot setHidden:NO];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictInfo=[pref objectForKey:PREF_LOGIN_OBJECT];
    
    NSString *strDate = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"dd MMM"];
    
    NSString *strTime  = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"HH:mma"];
    
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc]init];
    
    
    content.contentURL = [NSURL URLWithString:@"http://swiftback.com"];
    content.contentDescription = [NSString stringWithFormat:@"SwiftBack App :Share a ride, Meet new people, Save some cost! "];
    content.contentTitle = [NSString stringWithFormat:@"🚙 %@ needs a ride to \"%@\" from \"%@\" on %@ %@. Offer a ride at swiftback.com ✌",[dictInfo valueForKey:@"first_name"],txtDestination.text,txtSource.text,strDate,strTime];
    content.imageURL = [NSURL URLWithString:@"http://swiftback.com/wp-content/uploads/2016/07/swiftback_carpool-01-01.png"];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]]){
        dialog.mode = FBSDKShareDialogModeNative;
    }
    else {
        dialog.mode = FBSDKShareDialogModeBrowser; //or FBSDKShareDialogModeAutomatic
    }
    dialog.shareContent = content;
    dialog.delegate = self;
    //dialog.fromViewController = self;
    [dialog show];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Offer created successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    /*
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        

        
        
        
        NSString *strDate = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"dd MMM"];
        NSString *strTime  = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"HH:mma"];
       
        NSString *strShare=[NSString stringWithFormat:@"I’m heading from \"%@\"  to \"%@\" on %@ %@. Get a lift with SwiftBack : www.swiftback.com",txtSource.text,txtDestination.text,strDate,strTime];
        
        
        
        
    
        SLComposeViewController *facebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        
        [facebook addImage:[UIImage imageNamed:@"socialsharing-facebook-image-01.jpg"]];
        [facebook setInitialText:@"Check out this article."];
        [facebook addURL:[NSURL URLWithString:@"www.swiftback.com"]];
        [facebook setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 NSLog(@"The user cancelled.");
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 NSLog(@"The user posted to Facebook");
                 [APPDELEGATE showToastMessage:@"Posted on facebook successfully."];
             }
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Offer created successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
             [alert show];
             
             if ([self.delegate respondsToSelector:@selector(rideOfferCreated)]) {
                 [self.delegate rideOfferCreated];
             }
             
             [self.navigationController popViewControllerAnimated:YES];
         }];
        [self presentViewController:facebook animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"FACEBOOK", nil) message:NSLocalizedString(@"FACEBOOK_ALERT_MSG",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }*/
    /*
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc]init];
    [content setContentDescription:@"I’m heading from \"%@\"  to \"%@\" on %@ %@. Get a lift with SwiftBack : www.swiftback.com"];
    [content setContentTitle:@"SwiftBack Carpooling"];
    [content setImageURL:[NSURL URLWithString:@"http://swiftback.com/wp-content/uploads/2014/08/ASAS.png"]];
    
    [shareDialog setMode:FBSDKShareDialogModeAutomatic];
    [shareDialog setShareContent:content];
    [shareDialog setFromViewController:self];
    [shareDialog show];
    
    FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"fbauth2://"]]){
        dialog.mode = FBSDKShareDialogModeNative;
    }
    else {
        dialog.mode = FBSDKShareDialogModeBrowser; //or FBSDKShareDialogModeAutomatic
    }
    dialog.shareContent = content;
    dialog.delegate = self;
    dialog.fromViewController = self;
    [dialog show];
    */
    
    /*FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
     content.contentURL = [NSString stringWithFormat:@"I’m heading from \"%@\"  to \"%@\" on %@ %@. Get a lift with SwiftBack : www.swiftback.com",txtSource.text,txtDestination.text,strDate,strTime];
     content.imageURL = @"socialsharing-facebook-image-01.jpg";
     content.contentTitle = @"SwiftBack - Carpooling App";
     content.contentDescription = @"SwiftBack - Carpooling App";
     //if (![content canShow]) {
     // // fallback presentation when there is no FB app
     //  dialog.mode = FBSDKShareDialogModeFeedBrowser;
     //}
     
     FBSDKShareDialog *dialog = [[FBSDKShareDialog alloc] init];
     //dialog.fromViewController = viewController;
     dialog.shareContent = content;
     dialog.mode = FBSDKShareDialogModeNative; // if you don't set this before canShow call, canShow would always return YES
     if (![dialog canShow]) {
     // fallback presentation when there is no FB app
     dialog.mode = FBSDKShareDialogModeFeedBrowser;
     }
     [dialog show];*/
}

- (UIImage*)takeScreenshot
{
    /*
    [self.iconShare setHidden:NO];
    [_textShare setHidden:YES];
    [backgroundRect setHidden:YES];
    [_hideTitle setHidden:NO];
    [_hideHeader setHidden:NO];
    [_hideLogo setHidden:NO];
    [backgroundRectShare setHidden:NO];
    [_userReferralCode setHidden:NO];
    [_useReferralCode setHidden:YES];
     */
    
    
    //first we will make an UIImage from your view
    UIGraphicsBeginImageContext(self.view.bounds.size);
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *sourceImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //now we will position the image, X/Y away from top left corner to get the portion we want
    UIGraphicsBeginImageContext(viewForScreenshot.frame.size);
    [sourceImage drawAtPoint:CGPointMake(0, -90)];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}

-(void)twitterShare
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        
        SLComposeViewController *tweetsheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
        
        NSString *strDate = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"dd MMM"];
        NSString *strTime  = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"HH:mma"];
        
        NSString *strShare=[NSString stringWithFormat:@"I’m heading to \"%@\" from %@ on %@ %@.Get a ride with SwiftBack: www.swiftback.com",txtSource.text,txtDestination.text,strDate,strTime];
        
        [tweetsheet setInitialText:strShare];
        
        SLComposeViewController *tweet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [tweet setInitialText:strShare];
        
        [tweetsheet setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled)
             {
                 NSLog(@"The user cancelled.");
             }
             else if (result == SLComposeViewControllerResultDone)
             {
                 NSLog(@"The user posted to Facebook");
                 [APPDELEGATE showToastMessage:@"Posted on facebook successfully."];
             }
             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Offer created successfully." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
             [alert show];
             
             if ([self.delegate respondsToSelector:@selector(rideOfferCreated)]) {
                 [self.delegate rideOfferCreated];
             }
             
             [self.navigationController popViewControllerAnimated:YES];
         }];
        [self presentViewController:tweetsheet animated:YES completion:nil];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"TWITTER", nil) message:NSLocalizedString(@"TWITTER_ALERT_MSG",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
        [alert show];
    }
    
    
}

-(void)whatsappShare
{
    //SLComposeViewController *whatsapp = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    NSString *strDate = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"dd MMM"];
    NSString *strTime  = [[UtilityClass sharedObject] DateToString:datepicker.date withFormate:@"HH:mma"];
    
    NSString *message=[NSString stringWithFormat:@"I’m heading to \"%@\" from %@ %@ %@.Get a ride with SwiftBack: www.swiftback.com",txtSource.text,txtDestination.text,strDate,strTime];
    
    //[whatsapp setInitialText:strShare];
    
    
    //NSString *message = NSLocalizedString(@"WHATSAPP_SHARING_TEXT", nil);
    NSString *url = [NSString stringWithFormat:@"whatsapp://send?text=%@",message];
    NSURL *whatsappURL = [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    if([[UIApplication sharedApplication] canOpenURL:whatsappURL]) {
        [[UIApplication sharedApplication] openURL:whatsappURL];
        
    } else {
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"WHATSAPP_NOT_INSTALLED_MSG_TITLE", nil) message:NSLocalizedString(@"WHATSAPP_NOT_INSTALLED_MSG", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
    
}

- (IBAction)facebookClicked:(id)sender {
    
    if(toggleIsOn){
        facebookShare=@"0";
    }
    else {
        facebookShare=@"1";
    }
    toggleIsOn = !toggleIsOn;
    [self.btnFacebookshare setImage:[UIImage imageNamed:toggleIsOn ? @"facebook-2.png" :@"facebook-1.png"] forState:UIControlStateNormal];

    
    /*
    
    
    UIButton *btn=(UIButton *)sender;
    
    if(btn.tag == 0)
    {
        
        btn.tag=0;
        [btn setBackgroundImage:[UIImage imageNamed:@"facebook-logo.png"] forState:UIControlStateNormal];
        
        
        facebookShare=@"0";
        
        self.btnFacebookShare.enabled=TRUE;
        
    }
    else
    {
        btn.tag=1;
        [btn setBackgroundImage:[UIImage imageNamed:@"facebook-logo-2.png"] forState:UIControlStateNormal];
        
        facebookShare=@"1";
        
        self.btnFacebookShare.enabled=TRUE;
        
    }
     */
    
    
    
    
}



- (IBAction)twitterClicked:(id)sender {
    
    
    /*
    UIButton *btn=(UIButton *)sender;
    if(btn.tag == 0)
    {
        btn.tag=0;
        [btn setBackgroundImage:[UIImage imageNamed:@"twitter-icon.png"] forState:UIControlStateNormal];
        
        twitterShare=@"0";
        
        self.btnTwitterShare.enabled=TRUE;
        
    }
    else
    {
        btn.tag=1;
        [btn setBackgroundImage:[UIImage imageNamed:@"twitter-logo-2.png"] forState:UIControlStateNormal];
        
        twitterShare=@"1";
        
        self.btnTwitterShare.enabled=TRUE;
        
    }
     */
    
    if(toggleTwitterIsOn){
        twitterShare=@"0";
    }
    else {
        twitterShare=@"1";
    }
    toggleTwitterIsOn = !toggleTwitterIsOn;
    [self.btnTwitterShare setImage:[UIImage imageNamed:toggleTwitterIsOn ? @"twitter-2.png" :@"twitter-1.png"] forState:UIControlStateNormal];
}


- (IBAction)whatsappClicked:(id)sender {
    
    
    if(toggleWhatsappIsOn){
        whatsappShare=@"0";
    }
    else {
        whatsappShare=@"1";
    }
    toggleWhatsappIsOn = !toggleWhatsappIsOn;
    [self.btnWhatsappShare setImage:[UIImage imageNamed:toggleWhatsappIsOn ? @"whatsapp-2.png" :@"whatsapp-1.png"] forState:UIControlStateNormal];
}
 
 

- (IBAction)closeAlert:(id)sender {
    
    
    [viewForNotes setHidden:NO];
    [_closeAlert setHidden:YES];
    //[_confirmViewContainer setHidden:YES];
    [viewForGreenOverlay setHidden:YES];
    
    
    //animation
    /*
     CATransition *transition = [CATransition animation];
     
     transition.duration = 1;
     
     transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
     
     transition.type = kCATransitionFade;
     
     transition.delegate = self;
     
     [_alertView.layer addAnimation:transition forKey:nil];
     */
    
    //animation
    
    CATransition *transition2 = [CATransition animation];
    
    transition2.duration = 0.5;
    
    transition2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    transition2.type = kCATransitionFade;
    
    transition2.delegate = self;
    
    [viewForGreenOverlay.layer addAnimation:transition2 forKey:nil];
    [viewForNotes.layer addAnimation:transition2 forKey:nil];
    
    POPSpringAnimation *anim2 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerBounds];
    
    anim2.springBounciness = 15;
    anim2.springSpeed = 10;
    
    anim2.fromValue = [NSValue valueWithCGRect:CGRectMake(0, 0, 270, 370)];
    anim2.toValue = [NSValue valueWithCGRect:CGRectMake(0, -500, 270, 370)];
    [_confirmViewContainer.layer setCornerRadius:10];
    [_confirmViewContainer.layer pop_addAnimation:anim2 forKey:@"bounce2"];
}



@end
