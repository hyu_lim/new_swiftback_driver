//
//  CreateOfferVC.h
//  SwiftBack Driver
//
//  Created by Elluminati on 18/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "Social/Social.h"

@protocol CreateOffer <NSObject>

-(void)rideOfferCreated;

@end

@interface CreateOfferVC : UIViewController<UITextFieldDelegate,GMSMapViewDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate>{
    
    CLLocationManager *locationManager;
    CLLocationCoordinate2D sourcecoordinate,destinationcoordinate;
    
    GMSMarker *driverMarker;
    GMSMarker *sourceMarker,*destinationMarker;
    GMSMapView *mapView_;
    
    NSString *isSource,*placeID;
    NSString *sourceLatitude,*sourceLongitude;
    NSString *destinationLatitude,*destinationLongitude;
    NSString *distance,*cost;
    NSString *strFutureDate;
    NSMutableArray *placeMarkArr;
    NSArray *routes;
    BOOL isList;
    
    __weak IBOutlet UIView *viewForScreenshot;
    
    IBOutlet UIButton *btnFacebookShare;
    BOOL toggleIsOn;
    
    IBOutlet UIButton *btnTwitterShare;
    BOOL toggleTwitterIsOn;
    
    IBOutlet UIButton *btnWhatsappShare;
    BOOL toggleWhatsappIsOn;
    
    
    __weak IBOutlet UIButton *menuBtn;
    
   //Address Outlets
    __weak IBOutlet UITextField *txtSource;
    __weak IBOutlet UITextField *txtDestination;
    __weak IBOutlet UITableView *tableForPlaces;
    __weak IBOutlet UIView *viewForGoogleMap;
    __weak IBOutlet UIView *viewForAddress;
    
    // Estimate Outlets
    __weak IBOutlet UILabel *lblEstimateDistance;
    __weak IBOutlet UILabel *lblOfferPrice;
    __weak IBOutlet UIButton *minusBtn;
    __weak IBOutlet UIButton *plusBtn;
    __weak IBOutlet UIButton *estimateNextBtn;
    __weak IBOutlet UIView *viewForEstimate;
    __weak IBOutlet UIView *viewForOfferInfo;
    
    // Date Outlets
    __weak IBOutlet UIButton *dateNextBtn;
    __weak IBOutlet UIDatePicker *datepicker;
    __weak IBOutlet UISwitch *swForWeekly;
    __weak IBOutlet UIView *viewForDate;
    __weak IBOutlet UIView *viewForDatePicker;
    __weak IBOutlet UIView *viewForWeekly;
    
    // Note Outlets
    __weak IBOutlet UILabel *lblPassengerCount;
    __weak IBOutlet UITextField *txtNotes;
	__weak IBOutlet UIButton *noteNextBtn;
    __weak IBOutlet UIStepper *passengerStepper;
    __weak IBOutlet UIView *viewForNotes;
    __weak IBOutlet UIView *viewForNoOfPassenger;
    
    
    //Greenoverlay view
    __weak IBOutlet UIView *viewForGreenOverlay;
    
    // Confirm offer Outlets
    __weak IBOutlet UILabel *lblFrom;
    __weak IBOutlet UILabel *lblTo;
    __weak IBOutlet UILabel *lblDate;
    __weak IBOutlet UILabel *lblTime;
    __weak IBOutlet UILabel *lblDistance;
    __weak IBOutlet UILabel *lblNotes;
    __weak IBOutlet UILabel *lblSeats;
    __weak IBOutlet UILabel *lblPrice;
    __weak IBOutlet UIButton *confirmOfferBtn;
    __weak IBOutlet UIView *viewForConfirm;    
}

@property (strong,nonatomic) id <CreateOffer> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblCollectText;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferPriceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterDateText;
@property (weak, nonatomic) IBOutlet UILabel *lblTravellingWeeklyText;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfSeatTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNoteTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterDateText2;
@property (weak, nonatomic) IBOutlet UILabel *lblRideOfferTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceTitle2;
@property (weak, nonatomic) IBOutlet UILabel *lblShareText;
@property (weak, nonatomic) IBOutlet UILabel *lblSeatOfferedText;
@property (weak, nonatomic) IBOutlet UILabel *lblOfferPriceTitle2;
@property (weak, nonatomic) IBOutlet UILabel *lblNotesTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentDescriptionText;
@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIView *blurViewForAddress;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UISwitch *FacebookShareSwitch;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *confirmViewContainer;

- (IBAction)onClickCancelOffer:(id)sender;
- (IBAction)onClickBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeAlert;

// Address Events
-(IBAction)onClickSearch:(UITextField*)sender;

// Estimate View events
- (IBAction)onClickEstimateNext:(id)sender;
- (IBAction)onClickMinus:(id)sender;
- (IBAction)onClickPlus:(id)sender;

// Date View Events
- (IBAction)onClickDateNext:(id)sender;
- (IBAction)goBackToEstimate:(id)sender;

// Note View Events
- (IBAction)onClickPassengerStepper:(id)sender;
- (IBAction)onClickNotesNext:(id)sender;

- (IBAction)goBackToDateView:(id)sender;

// Confirm Offer Events
- (IBAction)onClickEditOffer:(id)sender;
- (IBAction)onClickConfirmOffer:(id)sender;
- (IBAction)onClickWeekly:(id)sender;

// Facebook Share Events
- (IBAction)onClickFacebookShare:(id)sender;



@property (nonatomic, retain) IBOutlet UIButton *btnFacebookshare;


//@property (weak, nonatomic) IBOutlet UIButton *btnFacebookShare;
@property (nonatomic, retain) IBOutlet UIButton *btnTwitterShare;

@property (nonatomic, retain) IBOutlet UIButton *btnWhatsappShare;






@end
