//
//  RideOffers.m
//  SwiftBack Driver
//
//  Created by Elluminati on 17/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "RideOffers.h"
#import "SWRevealViewController.h"
#import "RideNextCell.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"
#import "RatingBar.h"
#import "ConfirmOfferCell.h"
#import "ChatVC.h"

@interface RideOffers () {
    
    UITableViewController *tableViewController;
    NSString *currencySign;
    NSString *currentCountry;
}

@end

@implementation RideOffers

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customSetup];
    [self layoutSetup];
    [self setLocalizedStrings];
    
	myOffers = [[NSMutableArray alloc]init];
	confirmOffers = [[NSMutableArray alloc]init];
	OtherOffers = [[NSMutableArray alloc]init];
    
//    //Pull to refresh table view configuration
//    tableViewController = [[UITableViewController alloc] init];
//    tableViewController.tableView = tableForOffers;
//    
//    self.refreshControl = [[UIRefreshControl alloc] init];
//    self.refreshControl.backgroundColor = [UIColor clearColor];
//    self.refreshControl.tintColor = [UIColor darkGrayColor];
//    [self.refreshControl addTarget:self action:@selector(getOffers) forControlEvents:UIControlEventValueChanged];
//    tableViewController.refreshControl = self.refreshControl;
}

-(void)viewWillAppear:(BOOL)animated{
	[self getOffers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)customSetup{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        [_menuOn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        
        //Swipe to reveal menu
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
    }
    
    
}

- (void)setLocalizedStrings {
    [self.lblNavTitle setText:NSLocalizedString(@"RIDE_OFFERS", nil)];
}

- (void)layoutSetup {
    
    //NYSegmentedControl
    
    self.customSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"PENDING", nil), NSLocalizedString(@"CONFIRMED", nil), NSLocalizedString(@"OTHER_OFFERS", nil)]];
    self.customSegmentedControl.frame = CGRectMake(10, 60, self.view.frame.size.width - 20, 35);
    [self.customSegmentedControl addTarget:self action:@selector(segmentSelected) forControlEvents:UIControlEventValueChanged];
    
    self.customSegmentedControl.titleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleFont = [UIFont systemFontOfSize:12.0f];
    
    self.customSegmentedControl.backgroundColor = [UIColor colorWithRed:37.f/255.f green:131.f/255.f blue:111.f/255.f alpha:1.0f];
    
    self.customSegmentedControl.borderWidth = 0.0f;
    
    self.customSegmentedControl.segmentIndicatorBackgroundColor = [UIColor colorWithRed:59.f/255.f green:177.f/255.f blue:156.f/255.f alpha:1.0f];
    self.customSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    self.customSegmentedControl.segmentIndicatorInset = 2.0f;
    self.customSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    
    self.customSegmentedControl.cornerRadius = CGRectGetHeight(self.customSegmentedControl.frame) / 2.0f;
    
    [self.view addSubview:self.customSegmentedControl];
    
    //View Controller Clipping
    mainView.layer.cornerRadius = 5;
    mainView.clipsToBounds = YES;
}

#pragma mark
#pragma mark - Get Offers List

-(void)getOffers{

    if ([APPDELEGATE connected]) {
		
		[APPDELEGATE showLoadingWithTitle:@"Loading"];
		
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
		
        [dictParam setObject:[NSString stringWithFormat:@"%ld",(long)self.customSegmentedControl.selectedSegmentIndex] forKey:@"my_offers"];
        
        
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_OFFERS withParamData:dictParam withBlock:^(id response, NSError *error) {
			
			[APPDELEGATE hideLoadingView];
            if (response) {
                if ([[response objectForKey:@"success"]boolValue]) {
                    
                    if([response objectForKey:@"currency_sign"]) {
                        currencySign = [response objectForKey:@"currency_sign"];
                        NSLog(@"Sign : %@", currencySign);
                    } else {
                        currencySign = @"";
                    }
                    
                    if([currencySign isEqualToString:@""]) {
                        currencySign = @"$";
                    }
                    
                    if([response objectForKey:@"current_country"]) {
                        currentCountry = [response objectForKey:@"current_country"];
                        NSLog(@"CC : %@", currentCountry);
                    } else {
                        currentCountry = @"";
                    }
                    
                    if([currentCountry isEqualToString:@""]) {
                        currentCountry = @"Singapore";
                    }
                    
                    if (self.customSegmentedControl.selectedSegmentIndex == 0) {
						
                        myOffers = [[response objectForKey:@"offers"] mutableCopy];
						
						if ([myOffers count] > 0) {
							[tableForOffers setHidden:NO];
							[no_items setHidden:YES];
							[tableForOffers reloadData];
						}
						else{
							[tableForOffers setHidden:YES];
							[no_items setHidden:NO];
						}
	
                    }
                    else if (self.customSegmentedControl.selectedSegmentIndex == 1)
                    {
						
						confirmOffers = [[response objectForKey:@"offers"] mutableCopy];
						
						if ([confirmOffers count] > 0) {
							[tableForOffers setHidden:NO];
							[no_items setHidden:YES];
							[tableForOffers reloadData];
							[dottedLineImgView setHidden:NO];
						}
						else{
							[tableForOffers setHidden:YES];
							[no_items setHidden:NO];
							[dottedLineImgView setHidden:YES];
						}

                    }
                    else{
						OtherOffers = [[response objectForKey:@"offers"] mutableCopy];
						if ([OtherOffers count] > 0) {
							[tableForOffers setHidden:NO];
							[no_items setHidden:YES];
							[tableForOffers reloadData];
						}
						else{
							[tableForOffers setHidden:YES];
							[no_items setHidden:NO];
						}
                    }
                    
					
                }
				else{
					[tableForOffers setHidden:YES];
					[no_items setHidden:NO];
					[dottedLineImgView setHidden:YES];
				}
            } else {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
            }
            [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }];
    }
}

- (void)reloadData {
    //Reload table data
    [tableForOffers reloadData];
    
    //End the refreshing
    if(self.refreshControl) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update : %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrDictionary = [NSDictionary dictionaryWithObject:[UIColor darkGrayColor] forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        [self.refreshControl endRefreshing];
    }
}

#pragma mark
#pragma mark - TableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        return [myOffers count];
        
    }
    else if (self.customSegmentedControl.selectedSegmentIndex == 1){
        
        return [confirmOffers count];
    }
    else
    {
        return [OtherOffers count];
        
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSDictionary *dictRequest;
    
    RideNextCell *cellZero = [tableView dequeueReusableCellWithIdentifier:@"Cell"];


    if (self.customSegmentedControl.selectedSegmentIndex == 0) {
        dictRequest = [myOffers objectAtIndex:indexPath.row];
		
		// Add utility buttons
		NSMutableArray *rightUtilityButtons = [NSMutableArray new];
		
		
		
		[rightUtilityButtons sw_addUtilityButtonWithColor:
		 [UIColor colorWithRed:0.898 green:0.451 blue:0.451 alpha:1]
													title:@"X"];
		
		cellZero.rightUtilityButtons = rightUtilityButtons;
		cellZero.delegate = self;
        
        
        cellZero.layer.cornerRadius = 10;
        cellZero.layer.masksToBounds = YES;
        
        
        
        //cellZero.layer.frame = CGRectOffset(cellZero.layer.frame, 10, 10);
        
        //cellZero.layer.bounds = CGRectInset(cellZero.layer.frame, 5.0f, 1.0f);
        

    }
    else if (self.customSegmentedControl.selectedSegmentIndex == 1)
    {
        dictRequest = [confirmOffers objectAtIndex:indexPath.row];
        
        
		NSDictionary *dictOwner = [dictRequest objectForKey:@"owner"];
		
		[dottedLineImgView setHidden: NO];

		ConfirmOfferCell *confirmCell = [tableView dequeueReusableCellWithIdentifier:@"ConfirmCell"];

        // code for cancel button

        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor clearColor]title:@"X"];
        confirmCell.rightUtilityButtons = rightUtilityButtons;
        confirmCell.delegate = self; // here finish
        
        confirmCell.layer.cornerRadius = 20;
        confirmCell.layer.masksToBounds = YES;
        
        
        
		UIView *cell1BackgroundView = [confirmCell viewWithTag:900];
		cell1BackgroundView.backgroundColor = [UIColor whiteColor];
		
		cell1BackgroundView.layer.cornerRadius = 10;
		cell1BackgroundView.layer.shadowRadius = 5.0;
		cell1BackgroundView.layer.shadowOpacity = 0.4;
		cell1BackgroundView.layer.borderColor = [UIColor lightGrayColor].CGColor;
		cell1BackgroundView.layer.borderWidth = 0.5;
		
		cell1BackgroundView.clipsToBounds = YES;
		
		confirmCell.backgroundColor = [UIColor clearColor];
		confirmCell.layer.backgroundColor = [UIColor clearColor].CGColor;
		
		//Adjust horizontal and vertical lines size
		UILabel *horizontalLine = [confirmCell viewWithTag:903];
		UILabel *verticalLine1 = [confirmCell viewWithTag:904];
		UILabel *verticalLine2 = [confirmCell viewWithTag:905];
		
		horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
		
		verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
		
		verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);
		
		//cell2 properties
		
		
		//New View in Cell2
		UIView *cell2BackgroundView = [confirmCell viewWithTag:901];
		cell2BackgroundView.backgroundColor = [UIColor whiteColor];
		
		cell2BackgroundView.layer.cornerRadius = 10;
		cell2BackgroundView.clipsToBounds = YES;
		
		confirmCell.backgroundColor = [UIColor clearColor];
		confirmCell.layer.backgroundColor = [UIColor clearColor].CGColor;
		
		//viewForDriverInfo
		UIView *viewForDriverInfo = [confirmCell viewWithTag:1000];
		viewForDriverInfo.layer.cornerRadius = 10;
		
		UIImageView *imgViewForDriverInfo = [confirmCell viewWithTag:1001];
		imgViewForDriverInfo.layer.cornerRadius = 10;
		

		//[confirmCell.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
		[confirmCell.passengerImgView applyRoundedCornersFullWithColor:[UberStyleGuide colorDefault]];
		[confirmCell.passengerImgView downloadFromURL:[dictOwner objectForKey:PARAM_PICTURE] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
		[confirmCell.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:PARAM_SOURCE_ADDRESS]]];
		[confirmCell.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:PARAM_DESTIANTION_ADDRESS]]];
        
		[confirmCell.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:PARAM_TOTAL]]];
		[confirmCell.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:PARAM_DISTANCE] doubleValue]]];
		[confirmCell.lblDriverName setText:[dictOwner objectForKey:PARAM_NAME]];
		[confirmCell.ratingView setUserInteractionEnabled:NO];
		
		RBRatings ratings = [[dictOwner objectForKey:@"rating"] floatValue];
		[confirmCell.ratingView setRatings:ratings];
		
		NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
		[formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
		[formatter setDateFormat:@"yyyy-MM-dd"];//2013-07-15:10:00:00
		
		NSDate *date = [formatter dateFromString:[dictRequest objectForKey:@"request_date"]];
				
		[formatter setDateFormat:@"dd MMM"];

		
		[confirmCell.lblDepartureDate setText:[formatter stringFromDate:date]];

		
		[confirmCell.lblDepartureTime setText:[dictRequest objectForKey:@"request_time"]];
		
		//[confirmCell.lblDepartureTime setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"hh:MM a"]];
		[confirmCell.markBtn setTag:indexPath.row];
		[confirmCell.markBtn addTarget:self action:@selector(completeOffer:) forControlEvents:UIControlEventTouchUpInside];
		[confirmCell.chatBtn setTag:indexPath.row];
		[confirmCell.chatBtn addTarget:self action:@selector(goToChat:) forControlEvents:UIControlEventTouchUpInside];
		
		return confirmCell;
		
    }
    else{
        dictRequest = [OtherOffers objectAtIndex:indexPath.row];
    }

	[dottedLineImgView setHidden: YES];
    UIView *viewForCellOne = [cellZero viewWithTag:904];
    //viewForCellOne.layer.cornerRadius = 10;
    viewForCellOne.clipsToBounds = YES;

    //Adjust horizontal and vertical lines size
    UILabel *horizontalLine = [cellZero viewWithTag:908];
    UILabel *verticalLine1 = [cellZero viewWithTag:909];
    UILabel *verticalLine2 = [cellZero viewWithTag:910];
    
    horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
    
    verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
    
    verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);
    
    [cellZero.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
    [cellZero.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
    [cellZero.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];
    
    [cellZero.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:@"total"]]];
    [cellZero.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
    
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
	[formatter setDateFormat:@"yyyy-MM-dd"];//2013-07-15:10:00:00
	
	NSDate *date = [formatter dateFromString:[dictRequest objectForKey:@"request_date"]];
	
	[formatter setDateFormat:@"dd MMM"];
	
	
	[cellZero.lblDepartureDate setText:[formatter stringFromDate:date]];
	[cellZero.lblDepartureTime setText:[dictRequest objectForKey:@"request_time"]];


    return cellZero;
        
    
}

#pragma mark
#pragma mark - Tableview Cell Delete (custom class SwipeTableViewCell)

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [tableForOffers indexPathForCell:cell];
    NSLog(@"%li",(long)indexPath.row);
	switch (index) {
		case 0:
		{
			// Delete button is pressed
			NSIndexPath *cellIndexPath = [tableForOffers indexPathForCell:cell];
            NSString *offerID;
			if(self.customSegmentedControl.selectedSegmentIndex == 0)
            {
                 offerID= [[myOffers objectAtIndex:cellIndexPath.row] objectForKey:PARAM_OFFER_ID];
                [self deleteOffer:offerID indexPath:cellIndexPath];
            }
            else if(self.customSegmentedControl.selectedSegmentIndex == 1)
            {
                offerID= [[confirmOffers objectAtIndex:cellIndexPath.row] objectForKey:PARAM_OFFER_ID];
                [self deleteOffer:offerID indexPath:cellIndexPath];
            }
            else
            {
                 offerID= [[OtherOffers objectAtIndex:cellIndexPath.row] objectForKey:PARAM_OFFER_ID];
            }
            
			[self canBecomeFirstResponder];
			break;
		}
		default:
			break;
	}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
	if (self.customSegmentedControl.selectedSegmentIndex == 1)
	{
		return 250.0f;
	}
	else
	{
		return 100.0f;
	}
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *v = [UIView new];
    [v setBackgroundColor: [UIColor clearColor]];
    return v;
}

#pragma mark
#pragma mark - Tableview Button events

-(void)completeOffer:(UIButton*)sender
{
	
	NSDictionary *dictOffer = [confirmOffers objectAtIndex:sender.tag];
	
	UIAlertController *cancelAlert = [UIAlertController alertControllerWithTitle:@"Offer Completion" message:@"Are you sure to want to complete the trip?" preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
		
		if([APPDELEGATE  connected])
		{
			[APPDELEGATE showLoadingWithTitle:@"Loading..."];
			NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
			
			[dictParam setObject:[dictOffer objectForKey:PARAM_OFFER_ID] forKey:PARAM_OFFER_ID];
			[dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
			[dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
			
			AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
			[afn getDataFromPath:FILE_COMPETE_OFFER withParamData:dictParam withBlock:^(id response, NSError *error)
			 {
				 
				 NSLog(@"Mark Trip as completed = %@",response);
				 [APPDELEGATE hideLoadingView];
				 if (response)
				 {
					 if([[response valueForKey:@"success"] intValue]==1)
					 {
						 CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tableForOffers];
						 NSIndexPath *indexPath = [tableForOffers indexPathForRowAtPoint:buttonPosition];
						 
						 [confirmOffers removeObjectAtIndex:indexPath.row];

						 [tableForOffers beginUpdates];
						 [tableForOffers deleteRowsAtIndexPaths:@[indexPath ] withRowAnimation:UITableViewRowAnimationAutomatic];

							[tableForOffers endUpdates];
						 
						 if ([confirmOffers  count] == 0) {
							 [tableForOffers setHidden:YES];
							 [dottedLineImgView setHidden:YES];
							 [no_items setHidden:NO];
						 }
						 
						 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Trip is completed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
						 [alert show];
					 }
					 else{
						 UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
						 [alert show];
					 }
				 }
				 
			 }];
		}
		else
		{
			UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
			[alert show];
		}
	}];
	
	UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
	
	[cancelAlert  addAction:okAction];
	[cancelAlert addAction:cancelAction];
	
	[self presentViewController:cancelAlert animated:YES completion:nil];
}

- (void)deleteOffer:(NSString*)offerID indexPath:(NSIndexPath*)indexPath
{
	if([APPDELEGATE connected])
	{
		[APPDELEGATE  showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
		
		NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
		
		[dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
		[dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
		[dictParam setValue:offerID forKey:PARAM_OFFER_ID];
		
		AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
		[afn getDataFromPath:FILE_CANCEL_RIDE withParamData:dictParam withBlock:^(id response, NSError *error)
		 {
			 [APPDELEGATE hideLoadingView];
			 
			 if (response)
			 {
				 if([[response valueForKey:@"success"]boolValue])
				 {
					 [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCELLED", nil)];
					 [tableForOffers beginUpdates];
					 [tableForOffers deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                     if(self.customSegmentedControl.selectedSegmentIndex == 0)
                     {
                         [myOffers removeObjectAtIndex:indexPath.row];
                         [tableForOffers endUpdates];
                         
                         if ([myOffers count] == 0)
                         {
                             [tableForOffers setHidden:YES];
                             [no_items setHidden:NO];
                         }
                     }
                     else if(self.customSegmentedControl.selectedSegmentIndex == 1)
                     {
                         [confirmOffers removeObjectAtIndex:indexPath.row];
                         [tableForOffers endUpdates];
                         
                         if ([confirmOffers count] == 0)
                         {
                             [tableForOffers setHidden:YES];
                             [no_items setHidden:NO];
                             [dottedLineImgView setHidden:YES];
                         }
                     }
				 }
				 else
				 {}
			 }
			 
			 
		 }];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
		[alert show];
	}
}

-(IBAction)goToChat:(UIButton*)sender{
	
	CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tableForOffers];
	NSIndexPath *indexPath = [tableForOffers indexPathForRowAtPoint:buttonPosition];
	NSDictionary *dictOwner = [[confirmOffers objectAtIndex:indexPath.row] objectForKey:@"owner"];
	
	[self performSegueWithIdentifier:SEGUE_TO_DIRECT_CHAT sender:dictOwner];
}

#pragma mark
#pragma mark - Segment event

- (void)segmentSelected {
    
    [self getOffers];
    
    if([self.customSegmentedControl selectedSegmentIndex] == 1) {
        [dottedLineImgView setHidden: NO];
    } else {
        [dottedLineImgView setHidden: YES];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	
	if ([[segue identifier] isEqualToString:SEGUE_TO_CREATE_OFFER]) {
		CreateOfferVC *offer = [segue destinationViewController];
		[offer setDelegate:self];

	}
	if ([[segue identifier] isEqualToString:SEGUE_TO_DIRECT_CHAT]) {
		ChatVC *vc = [segue destinationViewController];
		vc.dictOwner = sender;
	}
}

#pragma mark -
#pragma mark - Create Offer Delegate Method

-(void)rideOfferCreated{
	
	[self.customSegmentedControl setSelectedSegmentIndex:0];
}

- (IBAction)onClickShowOfferVC:(id)sender {
	
	[self performSegueWithIdentifier:SEGUE_TO_CREATE_OFFER sender:nil];
}
- (IBAction)btnMenu:(id)sender {
    
    //[_menuOn setHidden: NO];
}
- (IBAction)menuOn:(id)sender {
    
    //[_menuOn setHidden: YES];
}






@end
