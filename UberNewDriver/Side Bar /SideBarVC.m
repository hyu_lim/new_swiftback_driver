
#import "SideBarVC.h"
#import "SWRevealViewController.h"
#import "PickMeUpMapVC.h"
#import "CellSlider.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"

@interface SideBarVC ()
{
    NSMutableArray *arrImages,*arrListName,*arrIdentifire;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
}

@end

@implementation SideBarVC

@synthesize ViewObj;


#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    internet=[APPDELEGATE connected];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref synchronize];
    strUserId=[pref objectForKey:PREF_USER_ID];
    strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    [self.imgProfilePic applyRoundedCornersFullWithColorSlim:[UIColor clearColor]];
    [self.imgProfilePic downloadFromURL:[arrUser valueForKey:PREF_USER_PICTURE] withPlaceholder:nil];

    self.lblName.text=[NSString stringWithFormat:@"%@ %@",[arrUser valueForKey:@"first_name"],[arrUser valueForKey:@"last_name"]];
    
    arrListName=[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"RIDES", nil),NSLocalizedString(@"RIDE_REQUESTS",nil),NSLocalizedString(@"RIDE_OFFERS", nil),NSLocalizedString(@"LEADERBOARD", nil),NSLocalizedString(@"MESSAGE", nil),NSLocalizedString(@"PROFILE",nil),NSLocalizedString(@"HISTORY",nil),NSLocalizedString(@"SUPPORT", nil),NSLocalizedString(@"ABOUT", nil),NSLocalizedString(@"SHARE",nil),NSLocalizedString(@"SETTINGS",nil),nil];
    
    arrIdentifire=[[NSMutableArray alloc]initWithObjects:@"RIDES", @"ADVANCE",@"OFFERS",@"LEADERBOARD",@"MESSAGE", @" PROFILE", @" HISTORY",  @"SUPPORT", @"ABOUT", @" SHARE",@" SETTINGS",nil];
    
    arrImages=[[NSMutableArray alloc]initWithObjects:@"icon_slider_ride",@"icon_slider_calendar",@"icon_ride_offer",@"icon_slider_trophy",@"icon_chat_white",@"icon_slider_profile",@"icon_slider_history",@"icon_mail_white",@"nav_about",@"nav_share",@"icon_slider_settings",nil];
    
    NSMutableArray *arrTemp=[[NSMutableArray alloc]init];
    NSMutableArray *arrImg=[[NSMutableArray alloc]init];
    
    for (int i=0; i<arrPage.count; i++)
    {
        NSMutableDictionary *temp1=[arrPage objectAtIndex:i];
        [arrTemp addObject:[temp1 valueForKey:@"title"]];
        [arrImg addObject:@"nav_support"];
    }
    
    [arrListName addObjectsFromArray:arrTemp];
    [arrIdentifire addObjectsFromArray:arrTemp];
    [arrImages addObjectsFromArray:arrImg];
    [arrListName addObject:NSLocalizedString(@"LOG_OUT", nil)];
    [arrImages addObject:@"ub__nav_logout"];
    
    
    self.navigationItem.leftBarButtonItem=nil;
    
    self.tableView.backgroundView=nil;
    self.tableView.backgroundColor=[UIColor clearColor];
    
    //Profile Picture setup
    [self.imgProfilePic applyRoundedCornersFullWithColorSlim:[UIColor whiteColor]];
    [self.imgProfilePic downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
    
    //Name setup
    self.lblName.text = [NSString stringWithFormat:@"%@ %@!", [arrUser valueForKey:@"first_name"], [arrUser valueForKey:@"last_name"]];
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrListName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CellSlider *cell=(CellSlider *)[tableView dequeueReusableCellWithIdentifier:@"CellSlider"];
    if (cell==nil) {
        cell=[[CellSlider alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellSlider"];
    }
    
    if(indexPath.row==4)
    {
        NSInteger count=[[USERDEFAULT valueForKey:PREF_CHAT_COUNTER] integerValue];
        if(count==0)
        {
            cell.lblChatCounter.hidden=YES;
        }
        else
        {
            cell.lblChatCounter.hidden=NO;
            cell.lblChatCounter.layer.masksToBounds = YES;
            cell.lblChatCounter.layer.cornerRadius = (cell.lblChatCounter.frame.size.width / 2);
            [cell.lblChatCounter setText:[NSString stringWithFormat:@"%@",[USERDEFAULT valueForKey:PREF_CHAT_COUNTER]]];
            [cell.lblChatCounter setBackgroundColor:[UIColor colorWithRed:255.0/255.0f green:82.0/255.0f blue:82.0/255.0f alpha:1]];
        }
    }
    else
    {
        cell.lblChatCounter.hidden=YES;
    }

    
    
    cell.lblName.text=[arrListName objectAtIndex:indexPath.row];
    cell.imgIcon.image=[UIImage imageNamed:[arrImages objectAtIndex:indexPath.row]];
    
    //[cell setCellData:[arrSlider objectAtIndex:indexPath.row] withParent:self];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if ([[arrListName objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"LOG_OUT", nil)])
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"LOG_OUT", nil)  message:NSLocalizedString(@"ARE_YOU_SURE_YOU_WANT_TO_LOGOUT", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"NO", nil) otherButtonTitles:NSLocalizedString(@"YES", nil), nil];
        alert.tag=100;
        [alert show];
        return;
    }
    if ([[arrListName objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"RIDES", nil)])
    {
        [self.revealViewController rightRevealToggle:nil];
        
        UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
        
        self.ViewObj=(PickMeUpMapVC *)[nav.childViewControllers objectAtIndex:0];
        [self.ViewObj goToRide];
        return;
    }
    
//    if ([[arrListName objectAtIndex:indexPath.row]isEqualToString:NSLocalizedString(@"Share", nil)])
//    {
//        NSLog(@"shareButton pressed");
//        
//        NSString *texttoshare = @"Hello"; //this is your text string to share
//        //UIImage *imagetoshare = @""; //this is your image to share
//        NSArray *activityItems = @[texttoshare];
//        
//        
//        UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
//        activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint];
//        [self presentViewController:activityVC animated:TRUE completion:nil];
//
//        return;
//    }
    
   // NSLog(@"%d",indexPath.row);
//    if ((indexPath.row >6)&&(indexPath.row<(arrListName.count-2)))
//    {
//        [self.revealViewController rightRevealToggle:self];
//        
//        UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
//        
//        ViewObj=(PickMeUpMapVC *)[nav.childViewControllers objectAtIndex:0];
//        
//        
//        if ([[arrListName objectAtIndex:indexPath.row]isEqualToString:@" About"])
//        {
//            
//        }
//        else
//        {
//            
//           // NSDictionary *dictTemp=[arrPage objectAtIndex:indexPath.row-4];
//            
//           // [ViewObj performSegueWithIdentifier:@"contact us" sender:dictTemp];
//        }
//      
//        return;
//    }
    [self.revealViewController rightRevealToggle:self];
    
    UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;
    
    ViewObj=(PickMeUpMapVC *)[nav.childViewControllers objectAtIndex:0];
    
    if(ViewObj!=nil)
        
    {
        [ViewObj goToSetting:[[arrIdentifire objectAtIndex:indexPath.row] lowercaseString]] ;

    }
}
- (IBAction)btnToggle:(id)sender {
    
    [self.btnToggle addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside ];
    
    
}

#pragma mark - Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 100)
    {
        if (buttonIndex == 1)
        {
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            [pref synchronize];
            [pref removeObjectForKey:PARAM_REQUEST_ID];
            [pref removeObjectForKey:PARAM_SOCIAL_ID];
            [pref removeObjectForKey:PREF_EMAIL];
            [pref removeObjectForKey:PREF_LOGIN_BY];
            [pref removeObjectForKey:PREF_PASSWORD];
            [pref removeObjectForKey:PREF_USER_ID];
            [pref removeObjectForKey:PREF_USER_TOKEN];
            [pref setBool:NO forKey:PREF_IS_LOGIN];
            
            [pref setObject:@"" forKey:PREF_USER_ID];
            [pref setObject:@"" forKey:PREF_USER_TOKEN];
            [pref synchronize];
          UINavigationController *nav=(UINavigationController *)self.revealViewController.frontViewController;

          ViewObj=(PickMeUpMapVC *)[nav.childViewControllers objectAtIndex:0];
          
        //  if(ViewObj!=nil)
          //  [ViewObj inv];
            [self.navigationController   popToRootViewControllerAnimated:YES];
            [APPDELEGATE showToastMessage:NSLocalizedString(@"LOGGED_OUT", nil)];
            
            /*
            
            if(internet)
            {
                
                    NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
                
                    [dictparam setObject:strUserId forKey:PARAM_ID];
                    [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
                
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_LOGOUT withParamData:dictparam withBlock:^(id response, NSError *error)
                     {
                         
                         NSLog(@"Log Out= %@",response);
                         [APPDELEGATE hideLoadingView];
                         if (response)
                         {
                             if([[response valueForKey:@"success"] intValue]==1)
                             {
                                 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                 [pref synchronize];
                                 [pref removeObjectForKey:PARAM_REQUEST_ID];
                                 [pref removeObjectForKey:PARAM_SOCIAL_ID];
                                 [pref removeObjectForKey:PREF_EMAIL];
                                 [pref removeObjectForKey:PREF_LOGIN_BY];
                                 [pref removeObjectForKey:PREF_PASSWORD];
                                 [pref removeObjectForKey:PREF_USER_ID];
                                 [pref removeObjectForKey:PREF_USER_TOKEN];
                                 [pref setBool:NO forKey:PREF_IS_LOGIN];
                                 
                                 [pref setObject:@"" forKey:PREF_USER_ID];
                                 [pref setObject:@"" forKey:PREF_USER_TOKEN];
                                 [pref synchronize];
                                 
                                 
                                 
//                                 if ([self.delegate respondsToSelector:@selector(invalidateTimer)])
//                                 {
//                                     [self.delegate invalidateTimer];
//                                 }
                                 
                                 
                                 [self.navigationController   popToRootViewControllerAnimated:YES];
                                 [APPDELEGATE showToastMessage:NSLocalizedString(@"LOGED_OUT", nil)];
                                 
                             }
                         }
                         
                     }];

            }
            else
            {
                UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                [alert show];
            }
            
            */
        }
    }
}

#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
