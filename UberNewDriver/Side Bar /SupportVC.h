

#import "BaseVC.h"
#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface SupportVC : BaseVC<MFMailComposeViewControllerDelegate>
{
    MFMailComposeViewController *mailComposer;
}


@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblWeGotYouText;
@property (weak, nonatomic) IBOutlet UILabel *lblCoveredText;
@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;

- (IBAction)btnEmailClick:(id)sender;
- (IBAction)btnCallClick:(id)sender;

@end
