
#import "HistoryCell.h"
#import "UIView+Utils.h"

@implementation HistoryCell

- (void)awakeFromNib {
    [self.ratingView initRateBar];
    [self.imgOwner applyRoundedCornersFullWithColor:[UIColor whiteColor]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
