//
//  RideOffers.h
//  SwiftBack Driver
//
//  Created by Elluminati on 17/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
#import <NYSegmentedControl/NYSegmentedControl.h>
#import "CreateOfferVC.h"


@interface RideOffers : UIViewController <SWTableViewCellDelegate,CreateOffer>
{
    
    NSMutableArray *myOffers;
    NSMutableArray *confirmOffers;
    NSMutableArray *OtherOffers;

    __weak IBOutlet UIButton *menuBtn;
    __weak IBOutlet UIImageView *no_items;
	__weak IBOutlet UIImageView *dottedLineImgView;
    __weak IBOutlet UITableView *tableForOffers;
    __weak IBOutlet UIView *mainView;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (nonatomic, strong) NYSegmentedControl *customSegmentedControl;
@property (strong, nonatomic) UIRefreshControl *refreshControl;

- (IBAction)onClickShowOfferVC:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menuOn;

@end
