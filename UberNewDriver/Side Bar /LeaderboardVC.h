//
//  LeaderboardVC.h
//  SwiftBack Driver
//
//  Created by Elluminati on 12/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeaderboardVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *arrSwiftPoints;
    __weak IBOutlet UIButton *menuBtn;
    __weak IBOutlet UITableView *tableForBoard;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentPositionTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblInfoText;
@property (weak, nonatomic) IBOutlet UILabel *lblPointCalculatedTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblPointCalculatedText;
@property (weak, nonatomic) IBOutlet UILabel *lblWalkerName;
@property (weak, nonatomic) IBOutlet UILabel *lblWalkerPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblWalkerRank;
@property (weak, nonatomic) IBOutlet UILabel *lblCircle1;
@property (weak, nonatomic) IBOutlet UILabel *lblCircle2;
@property (weak, nonatomic) IBOutlet UIButton *btnLeaderboardReward;
@property (weak, nonatomic) IBOutlet UIView *blurViewForCurrentPosition;
@property (weak, nonatomic) IBOutlet UIView *viewForCurrentPosition;
@property (weak, nonatomic) IBOutlet UIView *blurViewForExplanation;
@property (weak, nonatomic) IBOutlet UIView *viewForExplanation;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIView *viewForInfo;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImgForExplanation;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImgView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;

- (IBAction)onClickRewardBtn:(id)sender;
- (IBAction)onClickInfoBtn:(id)sender;

- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer;
- (void)hideOverlayView:(UITapGestureRecognizer *)tapGestureRecognizer;

@end
