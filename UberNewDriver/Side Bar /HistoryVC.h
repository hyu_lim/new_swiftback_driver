
#import "BaseVC.h"
#import "RatingBar.h"

@interface HistoryVC : BaseVC <UITableViewDataSource,UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UILabel *lblBasePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDistCost;
@property (weak, nonatomic) IBOutlet UILabel *lblPerDist;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeCost;
@property (weak, nonatomic) IBOutlet UILabel *lblPerTime;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblReferrel;
@property (weak, nonatomic) IBOutlet UILabel *lblPromo;
@property (weak, nonatomic) IBOutlet UILabel *lblBase_Price;
@property (weak, nonatomic) IBOutlet UILabel *lblDist_Cost;
@property (weak, nonatomic) IBOutlet UILabel *lblTime_Cost;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalDue;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Referrel;
@property (weak, nonatomic) IBOutlet UILabel *lbl_Promo;
@property (weak, nonatomic) IBOutlet UILabel *lblInvoice;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (weak, nonatomic) IBOutlet UIView *paymentView;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UITableView *tableHistory;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoItems;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (weak, nonatomic) IBOutlet UIButton *menuOn;

- (IBAction)closeBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;

@end
