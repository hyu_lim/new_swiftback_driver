
#import "BaseVC.h"


@interface ContactVC : BaseVC <UIWebViewDelegate>

@property (strong , nonatomic) NSDictionary *dictContact;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIWebView *webViewContact;

- (IBAction)backBtnPressed:(id)sender;

@end
