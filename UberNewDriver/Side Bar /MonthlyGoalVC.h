//
//  MonthlyGoalVC.h
//  SwiftBack Driver
//
//  Created by Zin Mar Htet on 22/1/16.
//  Copyright (c) 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthlyGoalVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *backgroundRect;

@end
