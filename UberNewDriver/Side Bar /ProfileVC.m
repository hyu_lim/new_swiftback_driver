
#import "ProfileVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "UtilityClass.h"
#import "PickMeUpMapVC.h"
#import "ArrivedMapVC.h"
#import "FeedBackVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Social/Social.h>

@interface ProfileVC ()
{
    BOOL internet;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strPassword;
    CGFloat savingAmount;
    CGFloat goalAmount;
    NSString *currencySignText;
    CGFloat keyboardHeight;
    BOOL isApproved;
    NSString *currencySign;
    NSString *currentCountry;
    
    //For fade in navigation bar
    // At this offset the Header stops its transformations
    CGFloat offset_HeaderStop;
    // At this offset the Black label reaches the Header
    CGFloat offset_B_LabelHeader;
    // The distance between the bottom of the Header and the top of the White Label
    CGFloat distance_W_LabelHeader;
    
}

@end

@implementation ProfileVC {
    NSInteger numberOfViews;
    NSString *commentString;
}

@synthesize txtAddress,title,txtBio,txtEmail,txtLastName,txtName,txtNumber,txtZip,txtPassword,btnProPic,txtNewPassword,bgNewPwd,bgPwd,txtTaxiModel,txtTaxiNumber,txtReNewPassword,bgReNewPwd,txtCarColor,userName,txtOccupation,txtGender;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    //For fade in navigation bar
    offset_HeaderStop = 40.0;
    offset_B_LabelHeader = 10.0;
    distance_W_LabelHeader = 35.0;
    
    arrRatingData = [[NSMutableArray alloc] init];
    arrReviewList = [[NSMutableArray alloc] init];
    dictReview = [[NSDictionary alloc] init];
    
    if([arrUser valueForKey:@"rating_data"] != nil) {
        [arrRatingData addObject:[arrUser valueForKey:@"rating_data"]];
        NSLog(@"Rating Data  ===>  %@", arrRatingData);
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidLoad];
    [super setBackBarItem];
    [self layoutSetup];
    [self setDataForUserInfo];
    [self prepareHorizontalScrollView];
    [self getSavings];
    [self setLocalizedStrings];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        //View Controller Clipping
        _mainView.layer.cornerRadius = 5;
        _mainView.clipsToBounds = YES;
    }
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpAndDownWithTapGestureRecognizer:)];
    
    [self.viewForEarnings addGestureRecognizer:singleTapGestureRecognizer];
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *singleTapGestureRecognizer2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpAndDownForReviewWithTapGestureRecognizer:)];
    
    [self.viewForReview addGestureRecognizer:singleTapGestureRecognizer2];

    
    //keyboard notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    //UITapGestureRecognizer to upload profile image from alert view
    UITapGestureRecognizer *profileNoticeTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imgPicBtnPressed:)];
    [self.viewForProfilePicAlert addGestureRecognizer:profileNoticeTapGestureRecognizer];
    
    //For fade in navigation bar
    self.headerBlurImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.viewForNavigation.frame.size.width, self.viewForNavigation.frame.size.height)];
    //    [self.headerBlurImageView downloadFromURL:[dictInfo valueForKey:@"picture"] withPlaceholder:nil];
    [self.headerBlurImageView setImage:[UIImage imageNamed:@"bg_green_for_navbar"]];
    
    self.headerBlurImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.headerBlurImageView.alpha = 0.0;
    [self.viewForNavigation addSubview: self.headerBlurImageView];
    
    //    //blurView
    //    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
    //        self.headerBlurImageView.backgroundColor = [UIColor clearColor];
    //        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    //        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    //
    //        blurEffectView.frame = self.headerBlurImageView.bounds;
    //        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    //        [self.headerBlurImageView addSubview:blurEffectView];
    //
    //    } else {
    //        self.headerBlurImageView.backgroundColor = [UIColor blackColor];
    //    }
    
    self.viewForNavigation.clipsToBounds = YES;
    [self.viewForNavigation bringSubviewToFront:self.btnMenu];
    [self.viewForNavigation bringSubviewToFront:self.lblNavTitle];
    [self.viewForNavigation bringSubviewToFront:self.btnEdit];
    [self.viewForNavigation bringSubviewToFront:self.btnUpdate];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.lblCarModelInfo.hidden=YES;
    self.imgCarModelInfo.hidden=YES;
    self.lblCarNumberInfo.hidden=YES;
    self.imgCarNumberInfo.hidden=YES;
    self.lblEmailInfo.hidden=YES;
    self.imgEmailInfo.hidden=YES;
    self.btnInfo1.tag=0;
    self.btnInfo2.tag=0;
    self.btnInfo3.tag=0;
    self.viewResetPwd.hidden=YES;
}

- (void)viewDidAppear:(BOOL)animated
{
    //[self.btnMenu setTitle:NSLocalizedString(@"PROFILE", nil) forState:UIControlStateNormal];
}

- (void)layoutSetup {
    
    [self.profileImage applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self.ScrollProfile setScrollEnabled:YES];
//    [self.ScrollProfile setContentSize:CGSizeMake(320, 440)];
    
    [self textDisable];
    //[self localizeString];
    
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
    [self.viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref synchronize];
    strUserId=[pref objectForKey:PREF_USER_ID];
    strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    strPassword=[pref objectForKey:PREF_PASSWORD];
    
    [txtName setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtLastName setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtNumber setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    
    [txtTaxiNumber setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtTaxiModel setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    [txtCarColor setTintColor:[UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1]];
    
    [self.btnUpdate setHidden:YES];
    // Do any additional setup after loading the view.
    
    //rectangle background
    _backgroundRect.layer.cornerRadius = 10;
    _backgroundRect.layer.borderWidth = 1;
    _backgroundRect.layer.borderColor = [UIColor whiteColor].CGColor;
    //    _backgroundRect.layer.shadowRadius = 5.0;
    //    _backgroundRect.layer.shadowOpacity = 0.4;
    
    
    //rectangle background
    //    _backgroundVehicleInfo.layer.cornerRadius = 10;
    _backgroundVehicleInfo.layer.borderWidth = 0.7;
    _backgroundVehicleInfo.layer.borderColor = [UIColor lightGrayColor].CGColor;
    //    _backgroundVehicleInfo.layer.shadowRadius = 5.0;
    //    _backgroundVehicleInfo.layer.shadowOpacity = 0.4;
    
    //rectangle background Reset
    _backgroundReset.layer.cornerRadius = 10;
    _backgroundReset.layer.borderWidth = 1;
    _backgroundReset.layer.borderColor = [UIColor whiteColor].CGColor;
    _backgroundReset.layer.shadowRadius = 5.0;
    _backgroundReset.layer.shadowOpacity = 0.4;
    
    //rectangle background Reset
    _viewForEarnings.layer.cornerRadius = 10;
    _viewForEarnings.layer.borderWidth = 1;
    _viewForEarnings.layer.borderColor = [UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1].CGColor;
    _viewForEarnings.layer.shadowRadius = 3.0;
    _viewForEarnings.layer.shadowOpacity = 0.3;
    
    //rectangle background Reset
    _viewForReview.layer.cornerRadius = 10;
    _viewForReview.layer.borderWidth = 1;
    _viewForReview.layer.borderColor = [UIColor colorWithRed:59.0/255.0 green:167/255.0 blue:140.0/255.0 alpha:1].CGColor;
    _viewForReview.layer.shadowRadius = 3.0;
    _viewForReview.layer.shadowOpacity = 0.3;
    
    //rectangle background Monthly Goal
    _viewForMonthlyGoal.layer.cornerRadius = 10;
    _viewForMonthlyGoal.layer.borderWidth = 1;
    _viewForMonthlyGoal.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewForMonthlyGoal.layer.shadowRadius = 5.0;
    _viewForMonthlyGoal.layer.shadowOpacity = 0.4;
    
    //rectangle background Monthly Goal
    _viewResetPwd.layer.cornerRadius = 10;
    _viewResetPwd.layer.borderWidth = 1;
    _viewResetPwd.layer.borderColor = [UIColor whiteColor].CGColor;
    _viewResetPwd.layer.shadowRadius = 5.0;
    _viewResetPwd.layer.shadowOpacity = 0.4;
    
    // documentBtn
    _documentBtn.layer.cornerRadius = 5;
    _documentBtn.layer.borderWidth = 0.1;
    _documentBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //viewForProfilePicAlert
    _viewForProfilePicAlert.layer.cornerRadius = 10;
    _viewForProfilePicAlert.layer.borderColor = [UIColor colorWithRed:216.f/255.f green:216.f/255.f blue:216.f/255.f alpha:0.7].CGColor;
    _viewForProfilePicAlert.layer.borderWidth = 1;
    _viewForProfilePicAlert.clipsToBounds = YES;
    
    
    //blurView
//    _blurView.layer.cornerRadius = 10;
//    _blurView.clipsToBounds = YES;
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        self.blurView.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.blurView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.blurView addSubview:blurEffectView];
        
    } else {
        self.blurView.backgroundColor = [UIColor blackColor];
    }
    //View Controller Clipping
//    _mainView.layer.cornerRadius = 5;
//    _mainView.clipsToBounds = YES;
    
}

//Set data
- (void)setDataForUserInfo {
    
    NSLog(@"ARRUSER : %@", arrUser);
    txtName.text=[arrUser valueForKey:@"first_name"];
    userName.text=[arrUser valueForKey:@"first_name"];
    txtLastName.text=[arrUser valueForKey:@"last_name"];
    txtEmail.text=[arrUser valueForKey:@"email"];
    txtNumber.text=[arrUser valueForKey:@"phone"];
    txtAddress.text=[arrUser valueForKey:@"address"];
    txtZip.text=[arrUser valueForKey:@"zipcode"];
    txtBio.text=[arrUser valueForKey:@"bio"];
    txtTaxiModel.text=[arrUser valueForKey:@"car_type"];
    txtTaxiNumber.text=[arrUser valueForKey:@"car_number"];
    txtCarColor.text=[arrUser valueForKey:@"car_color"];
    isApproved = [[arrUser valueForKey:@"is_approved"] boolValue];
    txtBio.text = [arrUser valueForKey:PARAM_BIO];
    txtOccupation.text = [arrUser valueForKey:PARAM_OCCUPATION];
    txtGender.text = [arrUser valueForKey:@"gender"];
    //    NSLog(@"Is Approved : %d",isApproved);
    
    // 1 is approved, 0 is not approved
    if ([[arrUser valueForKey:@"is_approved"] boolValue ]) {
        _documentBtn.hidden = YES;
        _btnResetPassword.frame = CGRectMake(_btnResetPassword.frame.origin.x, _btnResetPassword.frame.origin.y - 45, _btnResetPassword.frame.size.width, _btnResetPassword.frame.size.height);
        NSLog(@"Approved : %d",isApproved);
    } else {
        _documentBtn.hidden = NO;
        NSLog(@"Not Approved : %d",isApproved);
    }
    
    txtPassword.text=@"";
    txtNewPassword.text=@"";
    
    if([arrRatingData count] <= 0) {
        numberOfViews = 0;
        [self.ratingView initRateBar];
        [self.ratingView setUserInteractionEnabled:NO];
        //RBRatings rate=([[arrUser valueForKey:@"rating"]floatValue]*2);
        RBRatings rating = 0;
        [self.ratingView setRatings:rating];

    } else {
        NSLog(@"ArrRatingData ===> %@", arrRatingData[0]);
        numberOfViews = [arrRatingData[0][@"rating_list"] count];
        [arrReviewList addObject:[arrRatingData[0] objectForKey:@"rating_list"]];
        NSLog(@"RATING LIST COUNT ===> %li", (long)numberOfViews);
        
        [self.ratingView initRateBar];
        [self.ratingView setUserInteractionEnabled:NO];
        //RBRatings rate=([[arrUser valueForKey:@"rating"]floatValue]*2);
        RBRatings rating = (float)([[arrRatingData[0] valueForKey:@"rating"]floatValue]*2);
        [self.ratingView setRatings:rating];
    }
    
    [self.profileImage downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
    [self.profileBackground downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
}

//+++++++++++++++++++++++++++++++++++++//
- (void) prepareHorizontalScrollView {
    
    CGRect scrollViewFrame = CGRectMake(0, 25, self.viewForReview.frame.size.width, 100);
    self.horizontalScrollView.frame = scrollViewFrame;
    self.horizontalScrollView.pagingEnabled = YES;
    self.horizontalScrollView.bounces = NO;
    
    
    //Create left button and add to the subview
    CGRect leftBtnFrame = CGRectMake(10, 70, 20, 20);
    self.btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnLeft addTarget:self action:@selector(onClickLefBtn) forControlEvents:UIControlEventTouchUpInside];
    self.btnLeft.frame = leftBtnFrame;
    [self.btnLeft setImage:[UIImage imageNamed:@"icon_left_arrow_black"] forState:UIControlStateNormal];
    [self.viewForReview addSubview:self.btnLeft];
    
    
    
    //Create right button and add to the subview
    CGRect rightBtnFrame = CGRectMake((self.viewForReview.frame.size.width - 40), 70, 20, 20);
    self.btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnRight addTarget:self action:@selector(onClickRightBtn) forControlEvents:UIControlEventTouchUpInside];
    self.btnRight.frame = rightBtnFrame;
    [self.btnRight setImage:[UIImage imageNamed:@"icon_right_arrow_black"] forState:UIControlStateNormal];
    [self.viewForReview addSubview:self.btnRight];
    
    NSLog(@"ArrReviewList ===> %@", arrReviewList);
    
    
    //Init RatingBar here (P.S. cannnot initialize it inside of loop, if we do, stars won't be filled)
    [self.clientRatingView initRateBar];
    
    if(numberOfViews == 0) {
        CGRect placeholderFrame = CGRectMake(30, 15, self.viewForReview.frame.size.width - 60, self.viewForReview.frame.size.height - 60);
        self.placeholderImgView = [[UIImageView alloc] initWithFrame:placeholderFrame];
        [self.placeholderImgView setImage:[UIImage imageNamed:@"icon_no_review.png"]];
        [self.viewForReview addSubview:self.placeholderImgView];
        [self.viewForReview bringSubviewToFront:self.viewForReview];
    } else {
        for(int i=0; i<numberOfViews; i++) {
            
            dictReview = arrReviewList[0][i];
            //        NSLog(@"FirstName : %@",[dictReview objectForKey:@"first_name"]);
            //        NSLog(@"LastName : %@",[dictReview objectForKey:@"last_name"]);
            //        NSLog(@"ID : %@",[dictReview objectForKey:@"id"]);
            //        NSLog(@"Rating : %@",[dictReview objectForKey:@"rating"]);
            //        NSLog(@"Picture : %@",[dictReview objectForKey:@"picture"]);
            //        NSLog(@"Comment : %@",[dictReview objectForKey:@"comment"]);
            
            //Set the origin of the sub view
            CGFloat myOrigin = i * self.viewForReview.frame.size.width;
            
            //Create the sub view and allocate memory
            self.baseView = [[UIView alloc] initWithFrame:CGRectMake(myOrigin, 0, self.horizontalScrollView.frame.size.width, self.horizontalScrollView.frame.size.height)];
            //Set the background to clear color
            self.baseView.backgroundColor = [UIColor clearColor];
            
            
            //Create a imageView and add to the subview
            CGRect imgViewFrame = CGRectMake(50.f, 20.f, 70.f, 70.f);
            self.clientImgView = [[UIImageView alloc] initWithFrame:imgViewFrame];
            [self.clientImgView downloadFromURL:[dictReview objectForKey:@"picture"]withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
            [self.clientImgView applyRoundedCornersFull];
            [self.baseView addSubview:self.clientImgView];
            
            
            //Create a label and add to the subview
            CGRect nameFrame = CGRectMake(130.0f, 15.0f, 200.0f, 21.0f);
            self.lblClientName = [[UILabel alloc]initWithFrame:nameFrame];
            self.lblClientName.text = [NSString stringWithFormat:@"%@", [[dictReview objectForKey:@"first_name"] capitalizedString]];
            [self.lblClientName setFont:[UIFont fontWithName:@"OpenSans-Light" size:13.f]];
            self.lblClientName.textColor = [UIColor darkGrayColor];
            [self.baseView addSubview:self.lblClientName];
            
            
            //Create RatingBar
            self.clientRatingView = [[RatingBar alloc] initWithSize:CGSizeMake(70.f, 13.f) AndPosition:CGPointMake(130.f, 37.f)];
            self.clientRatingView.backgroundColor = [UIColor clearColor];
            //        [self.clientRatingView initRateBar];
            [self.clientRatingView setUserInteractionEnabled:NO];
            RBRatings rating = (float)([[dictReview objectForKey:@"rating"] floatValue] *2);
            //        NSLog(@"Rating ==> %f", (float)([[dictReview objectForKey:@"rating"] floatValue] *2));
            [self.clientRatingView setRatings:rating];
            [self.baseView addSubview:self.clientRatingView];
            
            
            //Create a label and add to the subview
            CGRect commentFrame = CGRectMake(130.0f, 53.0f, 150.0f, 40.0f);
            self.lblClientComment = [[UILabel alloc]initWithFrame:commentFrame];
            
            commentString = [dictReview objectForKey:@"comment"];
            if([commentString isEqualToString:@""] || [commentString isKindOfClass:[NSNull class]] || !commentString){
                commentString = NSLocalizedString(@"NO_REVIEW", nil);
            }
            self.lblClientComment.text = [NSString stringWithFormat:@"%@", commentString];
            [self.lblClientComment setFont:[UIFont fontWithName:@"OpenSans-Light" size:11.f]];
            [self.lblClientComment setNumberOfLines:0];
            [self.lblClientComment sizeToFit];
            [self.lblClientComment setLineBreakMode:NSLineBreakByWordWrapping];
            self.lblClientComment.textColor = [UIColor darkGrayColor];
            [self.baseView addSubview:self.lblClientComment];
            
            
            //Set the scroll view
            self.horizontalScrollView.delegate = self;
            [self.horizontalScrollView addSubview:self.baseView];
            self.horizontalScrollView.contentSize = CGSizeMake(self.viewForReview.frame.size.width * numberOfViews, self.horizontalScrollView.frame.size.height);
            [self.viewForReview addSubview:self.horizontalScrollView];
        }
    }
    
    if((numberOfViews == 0) || (numberOfViews == 1)) {
        [self.btnLeft setHidden:YES];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:YES];
    }
    
    [self.viewForReview bringSubviewToFront:self.btnLeft];
    [self.viewForReview bringSubviewToFront:self.btnRight];
}

- (void)onClickLefBtn {
    CGFloat pageWidth = self.horizontalScrollView.frame.size.width;
    CGFloat offset = self.horizontalScrollView.contentOffset.x - pageWidth;
    [self.horizontalScrollView setContentOffset:CGPointMake(offset, 0)];
    
    if(offset == 0.0) {
        [self.btnLeft setHidden:YES];
        [self.btnRight setHidden:NO];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.horizontalScrollView.layer addAnimation:transition forKey:nil];
}

- (void)onClickRightBtn {
    CGFloat pageWidth = self.horizontalScrollView.frame.size.width;
    CGFloat offset = self.horizontalScrollView.contentOffset.x + pageWidth;
    CGFloat mainViewFrameSize = self.view.frame.size.width * (numberOfViews-1);
    [self.horizontalScrollView setContentOffset:CGPointMake(offset, 0)];
    NSLog(@"OFFSET ===> %f", offset);
    NSLog(@"MAIN VIEW FRAME SIZE ===> %f", mainViewFrameSize);
    
    if(offset == mainViewFrameSize) {
        [self.btnRight setHidden:YES];
        [self.btnLeft setHidden:NO];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.horizontalScrollView.layer addAnimation:transition forKey:nil];
}

// scrolling ends
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth/2)/pageWidth) + 1;
    
    if(page == 0) {
        [self.btnRight setHidden:NO];
        [self.btnLeft setHidden:YES];
    } else if(page == numberOfViews-1) {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
}

// dragging ends
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth/2)/pageWidth) + 1;
    
    if(page == 0) {
        [self.btnRight setHidden:NO];
        [self.btnLeft setHidden:YES];
    } else if(page == numberOfViews-1) {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
}

//+++++++++++++++++++++++++++++++++++++//


//For fade in navigation bar
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offset = scrollView.contentOffset.y;
    self.headerBlurImageView.alpha = MIN(1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader);
}


//Progress View methods

- (void)showCurrentSavingWithPV{
    NSLog(@"showCurrentSavingWithPV");
    NSLog(@"Goal Amount : %f", savingAmount);
    NSLog(@"Goal Amount : %f", goalAmount);
    CGFloat progressValue = (savingAmount / goalAmount);
    NSLog(@"Progress Value : %f", progressValue);

    _lblProgBarStart.text = [NSString stringWithFormat:@"%@0",currencySignText];
    _lblMonthlyGoal.text = [NSString stringWithFormat:@"%@%d",currencySignText, (int)goalAmount];
    _pvSaving.progress = progressValue;
    
}

//To set white status bar
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    self.txtName.font=[UberStyleGuide fontRegular];
    self.txtLastName.font=[UberStyleGuide fontRegular];
    self.txtEmail.font=[UberStyleGuide fontRegular];
    self.txtAddress.font=[UberStyleGuide fontRegular];
    self.txtBio.font=[UberStyleGuide fontRegular];
	self.txtOccupation.font=[UberStyleGuide fontRegular];
    self.txtGender.font=[UberStyleGuide fontRegular];
	self.txtZip.font=[UberStyleGuide fontRegular];
    self.txtTaxiNumber.font=[UberStyleGuide fontRegular];
    
    self.btnMenu.titleLabel.font=[UberStyleGuide fontRegular:9.0f];
    self.btnEdit=[APPDELEGATE setBoldFontDiscriptor:self.btnEdit];
    self.btnUpdate=[APPDELEGATE setBoldFontDiscriptor:self.btnUpdate];
    
}

#pragma mark - Localization
-(void)setLocalizedStrings
{
    [self.lblNavTitle setText:NSLocalizedString(@"PROFILE", nil)];
    [self.lblWelcomeBackText setText:NSLocalizedString(@"WELCOME_BACK_", nil)];
    [self.txtName setPlaceholder:NSLocalizedString(@"FIRST_NAME", nil)];
    [self.txtLastName setPlaceholder:NSLocalizedString(@"LAST_NAME", nil)];
    [self.txtEmail setPlaceholder:NSLocalizedString(@"EMAIL", nil)];
    [self.txtNumber setPlaceholder:NSLocalizedString(@"NUMBER", nil)];
    [self.txtGender setPlaceholder:NSLocalizedString(@"GENDER", nil)];
    [self.txtOccupation setPlaceholder:NSLocalizedString(@"OCCUPATION", nil)];
    [self.txtBio setPlaceholder:NSLocalizedString(@"BIO", nil)];
    [self.lblVehicleInformationText setText:NSLocalizedString(@"VEHICLE_INFORMATION_TEXT", nil)];
    [self.txtTaxiNumber setPlaceholder:NSLocalizedString(@"TAXI_NUMBER", nil)];
    [self.txtTaxiModel setPlaceholder:NSLocalizedString(@"TAXI_MODEL", nil)];
    [self.txtCarColor setPlaceholder:NSLocalizedString(@"CAR_COLOR", nil)];
    [self.btnResetPassword setTitle:NSLocalizedString(@"RESET_PASSWORD", nil) forState:UIControlStateNormal];
    
    [self.lblReviewsTitle setText:NSLocalizedString(@"REVIEWS", nil)];
    
    [self.lblMonthlyGoalTitle setText:NSLocalizedString(@"MONTHLY_GOAL", nil)];
    [self.lblSavingText setText:NSLocalizedString(@"SAVING_THIS_MONTH", nil)];
    [self.lblGrossSavingsText setText:NSLocalizedString(@"GROSS_SAVINGS", nil)];
    [self.btnSetMonthlyGoal setTitle:NSLocalizedString(@"SET_MONTHLY_GOAL", nil) forState:UIControlStateNormal];
    
    [self.lblSetMonthlyGoalTitle setText:NSLocalizedString(@"SET_MONTHLY_GOAL_TEXT", nil)];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"SUBMIT", nil) forState:UIControlStateNormal];
    
    [self.lblResetPasswordTitle setText:NSLocalizedString(@"RESET_PASSWORD", nil)];
    [self.txtPassword setPlaceholder:NSLocalizedString(@"PASSWORD", nil)];
    [self.txtNewPassword setPlaceholder:NSLocalizedString(@"NEW_PASSWORD", nil)];
    [self.btnCancel2 setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnReset setTitle:NSLocalizedString(@"RESET", nil) forState:UIControlStateNormal];
}

-(void)updatePRofile
{
    NSLog(@"\n\n IN Update Profile");
    
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"UPDATING_PROFILE", nil)];
    internet=[APPDELEGATE connected];
    
    if(internet)
    {
        NSMutableDictionary *dictparam;
        dictparam= [[NSMutableDictionary alloc]init];
        
        [dictparam setObject:txtName.text forKey:PARAM_FIRST_NAME];
        [dictparam setObject:userName.text forKey:PARAM_FIRST_NAME];
        [dictparam setObject:txtLastName.text forKey:PARAM_LAST_NAME];
        [dictparam setObject:txtEmail.text forKey:PARAM_EMAIL];
        [dictparam setObject:txtNumber.text forKey:PARAM_PHONE];
        [dictparam setObject:txtTaxiNumber.text forKey:PARAM_CAR_NUMBER];
        [dictparam setObject:txtCarColor.text forKey:PARAM_CAR_COLOR];
        [dictparam setObject:txtTaxiModel.text forKey:PARAM_TAXI_MODEL];
		[dictparam setObject:txtBio.text forKey:PARAM_BIO];
		[dictparam setObject:txtOccupation.text forKey:PARAM_OCCUPATION];

		
        //[dictparam setObject:strPassword forKey:PARAM_PASSWORD];
        
        //            [dictparam setObject:txtPassword.text forKey:PARAM_OLDPASSWORD];
        //            [dictparam setObject:txtNewPassword.text forKey:PARAM_NEWPASSWORD];
        //[dictparam setObject:strPassword forKey:PARAM_PASSWORD];
        //[dictparam setObject:txtAddress.text forKey:PARAM_ADDRESS];
        //[dictparam setObject:txtZip.text forKey:PARAM_ZIPCODE];
        //[dictparam setObject:device_token forKey:PARAM_DEVICE_TOKEN];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        [dictparam setObject:@"" forKey:PARAM_PICTURE];
        
        UIImage *imgUpload = [[UtilityClass sharedObject]scaleAndRotateImage:self.profileImage.image];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_UPDATE_PROFILE withParamDataImage:dictparam andImage:imgUpload withBlock:^(id response, NSError *error)
         {
             
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     arrUser=response;
                     
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"PROFILE_UPDATED", nil)];
                     txtName.text=[arrUser valueForKey:@"first_name"];
                     userName.text=[arrUser valueForKey:@"first_name"];
                     txtLastName.text=[arrUser valueForKey:@"last_name"];
                     txtEmail.text=[arrUser valueForKey:@"email"];
                     txtNumber.text=[arrUser valueForKey:@"phone"];
                     txtAddress.text=[arrUser valueForKey:@"address"];
                     txtZip.text=[arrUser valueForKey:@"zipcode"];
                     txtBio.text=[arrUser valueForKey:@"bio"];
                     txtTaxiNumber.text=[arrUser valueForKey:@"car_number"];
                     [self.profileImage downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
                     [self.profileBackground downloadFromURL:[arrUser valueForKey:@"picture"] withPlaceholder:nil];
                     
                 }
                 else
                 {
                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PROFILE_UPDATE_FAIL", nil) message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                     [alert show];
                 }
                 [self.btnEdit setHidden:NO];
                 [self.btnUpdate setHidden:YES];
                 [self textDisable];
             }
             
             [APPDELEGATE hideLoadingView];
             
             NSLog(@"REGISTER RESPONSE --> %@",response);
         }];
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}

#pragma mark-
#pragma mark- Button Method

- (IBAction)LogOutBtnPressed:(id)sender
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    
    [pref synchronize];
    [pref removeObjectForKey:PARAM_REQUEST_ID];
    [pref removeObjectForKey:PARAM_SOCIAL_ID];
    [pref removeObjectForKey:PREF_EMAIL];
    [pref removeObjectForKey:PREF_LOGIN_BY];
    [pref removeObjectForKey:PREF_PASSWORD];
    [pref setBool:NO forKey:PREF_IS_LOGIN];
    
    [self.navigationController.navigationController    popToRootViewControllerAnimated:YES];
}

- (IBAction)editBtnPressed:(id)sender
{
    [self textEnable];
    [APPDELEGATE showToastMessage:NSLocalizedString(@"YOU_CAN_EDIT_YOUR_PROFILE", nil)];
    [self.btnEdit setHidden:YES];
    [self.btnUpdate setHidden:NO];
    [txtName becomeFirstResponder];
}

- (IBAction)updateBtnPressed:(id)sender
{
    internet=[APPDELEGATE connected];
    if (self.txtNewPassword.text.length>=1 || self.txtReNewPassword.text.length>=1)
    {
        if ([txtNewPassword.text isEqualToString:txtReNewPassword.text])
        {
            [self updatePRofile];
            
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"PROFILE_UPDATE_FAIL", nil) message:NSLocalizedString(@"NOT_MATCH_RETYPE",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else
    {
        [self updatePRofile];
    }
}

- (IBAction)backBtnPressed:(id)sender
{
    NSArray *currentControllers = self.navigationController.viewControllers;
    NSMutableArray *newControllers = [NSMutableArray
                                      arrayWithArray:currentControllers];
    UIViewController *obj=nil;
    
    for (int i=0; i<newControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if ([vc isKindOfClass:[FeedBackVC class]])
        {
            obj = (FeedBackVC *)vc;
        }
        else if ([vc isKindOfClass:[ArrivedMapVC class]])
        {
            obj = (ArrivedMapVC *)vc;
        }
        else if ([vc isKindOfClass:[PickMeUpMapVC class]])
        {
            obj = (PickMeUpMapVC *)vc;
        }
        
    }
    [self.navigationController popToViewController:obj animated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)imgPicBtnPressed:(id)sender
{

    [self.view endEditing:YES];
    self.ScrollProfile.contentOffset = CGPointMake(0, 0);
    UIActionSheet *action=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"TAKE_PHOTO", nil), NSLocalizedString(@"SELECT_IMAGE", nil), nil];
    action.tag=10001;
    [action showInView:self.view];
}

- (IBAction)onClickEmailInfo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0)
    {
        btn.tag=1;
        self.lblEmailInfo.hidden=NO;
        self.imgEmailInfo.hidden=NO;
    }
    else
    {
        btn.tag=0;
        self.lblEmailInfo.hidden=YES;
        self.imgEmailInfo.hidden=YES;
    }
}

- (IBAction)onClickTaxiModelInfo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0)
    {
        btn.tag=1;
        self.lblCarModelInfo.hidden=NO;
        self.imgCarModelInfo.hidden=NO;
    }
    else
    {
        btn.tag=0;
        self.lblCarModelInfo.hidden=YES;
        self.imgCarModelInfo.hidden=YES;
    }
}

- (IBAction)onClickTaxiNoInfo:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.tag==0)
    {
        btn.tag=1;
        self.lblCarNumberInfo.hidden=NO;
        self.imgCarNumberInfo.hidden=NO;
    }
    else
    {
        btn.tag=0;
        self.lblCarNumberInfo.hidden=YES;
        self.imgCarNumberInfo.hidden=YES;
    }
}

- (IBAction)onClickResetPwd:(id)sender
{
    _viewForGreenOverlay.alpha = 0;
    _viewResetPwd.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.viewForGreenOverlay.alpha = 0.8;
        self.viewResetPwd.alpha = 1;
        self.viewForGreenOverlay.hidden = NO;
        self.viewResetPwd.hidden = NO;
        CGPoint offset;
        offset=CGPointMake(0, 0);
        [self.ScrollProfile setContentOffset:offset animated:YES];
        [self.txtPassword resignFirstResponder];
        [self.txtNewPassword resignFirstResponder];
    }];
}

- (IBAction)onClickCancelPwd:(id)sender
{
    [UIView animateWithDuration:0.3 animations:^{
        _viewForGreenOverlay.alpha = 0;
        _viewResetPwd.alpha = 0;
    } completion:^(BOOL finished){
        [_viewForGreenOverlay setHidden: finished];
        [_viewResetPwd setHidden: finished];
        CGPoint offset;
        offset=CGPointMake(0, 0);
        [self.ScrollProfile setContentOffset:offset animated:YES];
        [self.txtPassword resignFirstResponder];
        [self.txtNewPassword resignFirstResponder];
    }];
}

- (IBAction)onClickReset:(id)sender
{
    [self.txtPassword resignFirstResponder];
    [self.txtNewPassword resignFirstResponder];
    if([[AppDelegate sharedAppDelegate]connected])
    {
        if(self.txtNewPassword.text.length > 0 && self.txtPassword.text.length > 0)
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"PASSWORD_UPDATING", nil)];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setObject:self.txtPassword.text forKey:@"old_password"];
            [dictParam setObject:self.txtNewPassword.text forKey:@"new_password"];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RESET_PASSWORD withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         self.viewForGreenOverlay.hidden = YES;
                         self.viewResetPwd.hidden=YES;
                         self.txtPassword.text=@"";
                         self.txtNewPassword.text=@"";
                         [self.txtNewPassword resignFirstResponder];
                         [self.txtPassword resignFirstResponder];
                         [[AppDelegate sharedAppDelegate] showToastMessage:[response valueForKey:@"message"]];
                     }
                     else
                     {
                         self.txtPassword.text=@"";
                         self.txtNewPassword.text=@"";
                         [[AppDelegate sharedAppDelegate] showToastMessage:[response valueForKey:@"message"]];
                     }
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_ENTER_OLD_AND_NEW_PWD_FOR_RESET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"NETWORK_STAUS", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

- (IBAction)onClickPresentModal:(id)sender {
    
    [self performSegueWithIdentifier:@"segueToDocument" sender:nil];
}

- (IBAction)saveMonthlyGoal:(id)sender {
    
    if ([self.txtMontlyGoal.text integerValue] > 0) {
        _viewForGreenOverlay.alpha = 0;
        _viewForMonthlyGoal.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            _viewForGreenOverlay.alpha = 0.8;
            _viewForMonthlyGoal.alpha = 1;
            [_viewForGreenOverlay setHidden: NO];
            [_viewForMonthlyGoal setHidden: NO];
            CGPoint offset;
            offset=CGPointMake(0, 0);
            [self.ScrollProfile setContentOffset:offset animated:YES];
            [_txtMontlyGoal resignFirstResponder];
            [self submitGoal];
        }];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"INVALID_GOAL", nil) message:NSLocalizedString(@"PLEASE_ENTER_VALID_AMOUNT", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
        [alert show];
    }
}

- (IBAction)setMonthlyGoal:(id)sender {
    
    _viewForGreenOverlay.alpha = 0;
    _viewForMonthlyGoal.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewForGreenOverlay.alpha = 0.8;
        _viewForMonthlyGoal.alpha = 1;
        [_viewForGreenOverlay setHidden: NO];
        [_viewForMonthlyGoal setHidden: NO];
        CGPoint offset;
        offset=CGPointMake(0, 0);
        [self.ScrollProfile setContentOffset:offset animated:YES];
        [_txtMontlyGoal resignFirstResponder];
    }];
}

- (IBAction)cancelMonthlyGoal:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        _viewForGreenOverlay.alpha = 0;
        _viewForMonthlyGoal.alpha = 0;
    } completion:^(BOOL finished){
        [_viewForGreenOverlay setHidden: finished];
        [_viewForMonthlyGoal setHidden: finished];
    }];
}

#pragma mark -
#pragma mark - UIActionSheet delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            [self openCamera];
            break;
        case 1:
            [self chooseFromLibaray];
            break;
        case 2:
            break;
        case 3:
            break;
    }
}

-(void)openCamera
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        
        imagePickerController.delegate =self;
        imagePickerController.allowsEditing=YES;
        
        imagePickerController.view.tag = 102;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:imagePickerController animated:YES completion:^{
            [self updatePRofile];
            [self.viewForProfilePicAlert setHidden:YES];
        }];
    }
    else
    {
        UIAlertView *alt = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"CAM_NOT_AVAILABLE", nil)delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alt show];
    }
}

-(void)chooseFromLibaray
{
    // Set up the image picker controller and add it to the view
    
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    imagePickerController.delegate =self;
    
    imagePickerController.allowsEditing=YES;
    imagePickerController.view.tag = 102;
    
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickerController animated:YES completion:^{
        
    }];
}

#pragma mark -
#pragma mark - UIImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.viewForProfilePicAlert setHidden:YES];
    self.profileImage.image=[info objectForKey:UIImagePickerControllerEditedImage];
    [self updatePRofile];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mak-
#pragma mark- TextField Enable and Disable

-(void)textDisable
{
    txtName.enabled = NO;
    txtLastName.enabled = NO;
    txtEmail.enabled = NO;
    txtNumber.enabled = NO;
    txtAddress.enabled = NO;
    txtZip.enabled = NO;
    txtBio.enabled = NO;
    txtOccupation.enabled = NO;
    txtGender.enabled = NO;
    btnProPic.enabled=NO;
    txtTaxiNumber.enabled = NO;
    txtTaxiModel.enabled = NO;
    txtCarColor.enabled = NO;
    //[self.ScrollProfile setScrollEnabled:NO];
    
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.ScrollProfile setContentSize:CGSizeMake(320, 900)];
    [self.ScrollProfile setContentOffset:offset animated:YES];
}

-(void)textEnable
{
    txtName.enabled = YES;
    txtLastName.enabled = YES;
    txtEmail.enabled = NO;
    txtNumber.enabled = YES;
    txtAddress.enabled = YES;
    txtZip.enabled = YES;
    txtBio.enabled = YES;
    txtOccupation.enabled = YES;
    txtGender.enabled = NO;
    btnProPic.enabled=YES;
    txtTaxiNumber.enabled = YES;
    txtTaxiModel.enabled = YES;
    txtCarColor.enabled = YES;
    
    [self.ScrollProfile setContentSize:CGSizeMake(320, 1100)];
    //[self.ScrollProfile setScrollEnabled:YES];
}

#pragma mark - keyboard 

- (void)keyboardWillShow:(NSNotification *)notification {
    
    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
}

#pragma mark-
#pragma mark- Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField     //Hide the keypad when we pressed return
{
    CGPoint offset;
    offset=CGPointMake(0, 0);
    [self.ScrollProfile setContentOffset:offset animated:YES];
    [UIView animateWithDuration:0.5 animations:^{
        [self.viewResetPwd setFrame:CGRectMake(20, 115, self.viewResetPwd.frame.size.width, self.viewResetPwd.frame.size.height)];
    }];
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint offset;
    
    if(textField == self.txtName) {
        offset = CGPointMake(0, 130);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtLastName) {
         offset = CGPointMake(0, 130);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtNumber) {
         offset = CGPointMake(0, 150);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtOccupation) {
         offset = CGPointMake(0, 170);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtBio) {
         offset = CGPointMake(0, 190);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtTaxiNumber) {
         offset = CGPointMake(0, 290);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtTaxiModel) {
         offset = CGPointMake(0, 310);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtCarColor) {
         offset = CGPointMake(0, 330);
        [self.ScrollProfile setContentOffset:offset animated:YES];
    } else if (textField == self.txtPassword) {
        [UIView animateWithDuration:0.5 animations:^{
            [self.viewResetPwd setFrame:CGRectMake(20, 100, self.viewResetPwd.frame.size.width, self.viewResetPwd.frame.size.height)];
        }];
    } else if (textField == self.txtNewPassword) {
        [UIView animateWithDuration:0.5 animations:^{
            [self.viewResetPwd setFrame:CGRectMake(20, 80, self.viewResetPwd.frame.size.width, self.viewResetPwd.frame.size.height)];
        }];
    }
    
    
    
//    if(textField==self.txtEmail)
//    {
//        offset=CGPointMake(0, 0);
//        [self.ScrollProfile setContentOffset:offset animated:YES];
//    }
//    if(textField==self.txtNumber)
//    {
//        offset=CGPointMake(0, 40);
//        [self.ScrollProfile setContentOffset:offset animated:YES];
//    }
//    if(textField==self.txtAddress)
//    {
//        offset=CGPointMake(0, 80);
//        [self.ScrollProfile setContentOffset:offset animated:YES];
//    }
//    if(textField==self.txtBio)
//    {
//        offset=CGPointMake(0, 120);
//        [self.ScrollProfile setContentOffset:offset animated:YES];
//    }
//    if(textField==self.txtZip)
//    {
//        offset=CGPointMake(0, 160);
//        [self.ScrollProfile setContentOffset:offset animated:YES];
//    }
//    if(textField==self.txtPassword)
//    {
//        offset=CGPointMake(0, 60);
//        [self.ScrollProfile setContentOffset:offset animated:YES];
//    }
//    if(textField==self.txtNewPassword)
//    {
//        offset=CGPointMake(0, 100);
//        [self.ScrollProfile setContentOffset:offset animated:YES];
//    }
    
    
    /*
     if(textField == self.txtPassword)
     {
     UITextPosition *beginning = [self.txtPassword beginningOfDocument];
     [self.txtPassword setSelectedTextRange:[self.txtPassword textRangeFromPosition:beginning
     toPosition:beginning]];
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -35, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     if(textField == self.txtAddress)
     {
     UITextPosition *beginning = [self.txtAddress beginningOfDocument];
     [self.txtAddress setSelectedTextRange:[self.txtAddress textRangeFromPosition:beginning
     toPosition:beginning]];
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -75, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     if(textField == self.txtBio)
     {
     UITextPosition *beginning = [self.txtBio beginningOfDocument];
     [self.txtBio setSelectedTextRange:[self.txtBio textRangeFromPosition:beginning
     toPosition:beginning]];
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -115, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     else if(textField == self.txtZip)
     {
     [UIView animateWithDuration:0.3 animations:^{
     
     self.view.frame = CGRectMake(0, -128, 320, 480);
     
     } completion:^(BOOL finished) { }];
     }
     */
}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    UIDevice *thisDevice=[UIDevice currentDevice];
//    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
//    {
//        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
//        
//        if (iOSDeviceScreenSize.height == 568)
//        {
//            if(textField == self.txtPassword)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 568);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//            else if(textField == self.txtAddress)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 568);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//            else if(textField == self.txtBio)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 568);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//            else if (textField == self.txtZip)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 568);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//        }
//        else
//        {
//            if(textField == self.txtPassword)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 480);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//            else if(textField == self.txtAddress)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 480);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//            else if(textField == self.txtBio)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 480);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//            else if (textField == self.txtZip)
//            {
//                [UIView animateWithDuration:0.3 animations:^{
//                    
//                    self.view.frame = CGRectMake(0, 0, 320, 480);
//                    
//                } completion:^(BOOL finished) { }];
//            }
//            
//        }
//    }
//}

#pragma mark - Gesture Recognizer methods

- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer
{
    
    CGFloat reviewYCoordinate = 475.0;
    
    if(self.viewForReview.frame.origin.y == 370.0)
    {
        reviewYCoordinate = 475.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        self.viewForReview.frame = CGRectMake(self.viewForReview.frame.origin.x, reviewYCoordinate, self.viewForReview.frame.size.width, self.viewForReview.frame.size.height);
    }];
    
    CGFloat newYCoordinate = 520.0;
    
    if(self.viewForEarnings.frame.origin.y == 520.0)
    {
        newYCoordinate = 370.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        self.viewForEarnings.frame = CGRectMake(self.viewForEarnings.frame.origin.x, newYCoordinate, self.viewForEarnings.frame.size.width, self.viewForEarnings.frame.size.height);
    }];
}

- (void)slideUpAndDownForReviewWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer
{
    
    CGFloat newYCoordinate = 475.0;
    
    if(self.viewForReview.frame.origin.y == 475.0) {
        newYCoordinate = 370.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        self.viewForReview.frame = CGRectMake(self.viewForReview.frame.origin.x, newYCoordinate, self.viewForReview.frame.size.width, self.viewForReview.frame.size.height);
    }];
}

#pragma mark - Web Service methods
#pragma mark - Fetch data from web service

- (void)getSavings{
    
    if ([APPDELEGATE connected]) {
        
        [APPDELEGATE showLoadingWithTitle:@"Loading"];
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_GET_SAVINGS withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response) {
                if ([[response valueForKey:@"success"] boolValue]) {
                    [self.viewForEarnings setHidden:NO];
                    
                    if([response objectForKey:@"currency_sign"]) {
                        currencySign = [response objectForKey:@"currency_sign"];
                        NSLog(@"Sign : %@", currencySign);
                    } else {
                        currencySign = @"$";
                    }
                    
                    if([response objectForKey:@"current_country"]) {
                        currentCountry = [response objectForKey:@"current_country"];
                        NSLog(@"CC : %@", currentCountry);
                    } else {
                        currentCountry = @"Singapore";
                    }
                    
                    currencySignText = currencySign;
                    
                    [self.lblSavingOfMonth setText:[NSString stringWithFormat:@"%@%@",currencySign,[response valueForKey:@"monthly_saving"]]];
                    [self.lblGrossSavings setText:[NSString stringWithFormat:@"%@%@",currencySign,[response valueForKey:@"gross_saving"]]];
                    
                    NSString *goal = [NSString stringWithFormat:@"%@",[response valueForKey:@"my_goal"]];
                    
                    savingAmount = [[NSString stringWithFormat:@"%@",[response valueForKey:@"monthly_saving"]] floatValue];
                    
                    if(![goal isEqualToString:@""]) {
                        goalAmount = [[NSString stringWithFormat:@"%@",[response valueForKey:@"my_goal"]] floatValue];
                    } else {
                        goalAmount = 0.0;
                    }
                    if ([response valueForKey:@"document_status"]) {
                        
                        NSInteger status = [[response valueForKey:@"document_status"] integerValue];
                        if (status == 0) {
                            [self.documentBtn setHidden:NO];
                            [self.documentBtn setTitle:@"Account Not verified, Verify now" forState:UIControlStateNormal];
                        }
                        else if(status == 1){
                            [self.documentBtn setHidden:NO];
                            [self.documentBtn setUserInteractionEnabled:NO];
                            [self.documentBtn setTitle:@"Verification Pending" forState:UIControlStateNormal];
                        }
                        else{
                            [self.documentBtn setHidden:YES];
                        }
                    }
                    
                    [self showCurrentSavingWithPV];
                    
                }
                else{
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                    [self.viewForEarnings setHidden:YES];
                }
            }
            else{
                [self.viewForEarnings setHidden:YES];
            }
        }];
    }
}

#pragma mark - Save goal value to Web Service

- (void)submitGoal
{
    
    if ([APPDELEGATE connected]) {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"SETTING_YOUR_GOAL", nil)];
        
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:self.txtMontlyGoal.text forKey:PARAM_AMOUNT];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_SUBMIT_GOAL withParamData:dictParam withBlock:^(id response, NSError *error) {
            [APPDELEGATE hideLoadingView];
            if (response) {
                if ([[response objectForKey:@"success"]boolValue]) {
                    
                    NSString *goal = [NSString stringWithFormat:@"%@",[response valueForKey:@"my_monthly_goal"]];
                    
                    if(![goal isEqualToString:@""]) {
                        goalAmount = [[NSString stringWithFormat:@"%@",[response valueForKey:@"my_monthly_goal"]] floatValue];
                    } else {
                        goalAmount = 0.0;
                    }
                    [self showCurrentSavingWithPV];
                    
                    [self.txtMontlyGoal setText:@""];
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"GOAL_UPDATED_SUCCESFULLY", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                    
                    [self cancelMonthlyGoal:nil];
                }
                else{
                    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"OOPS", nil) message:NSLocalizedString(@"GOAL_COULDN_NOT_UPDATED", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                    
                }
            }
        }];
        
    }
}

@end
