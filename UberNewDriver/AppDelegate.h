

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import <CoreLocation/CoreLocation.h>
#define Google_Key @"AIzaSyBlJu2UTwemZ8a9uWCGLOiG_OAdN9alUhQ"
#define Google_Browser_key @"AIzaSyA0iwWpcCS13NTIDqsCR6MeHqT7z0nRQQM"

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate> {
    
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIView   *viewLoading;

//Country specific request setup
@property (weak, nonatomic) IBOutlet UILabel *lblLang;
@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblLat;
@property (weak, nonatomic) IBOutlet UILabel *lblLong;
@property (weak, nonatomic) IBOutlet UILabel *lblAddr;

- (IBAction)getCurrentLocation:(id)sender;
+ (AppDelegate *)sharedAppDelegate;
- (void)userLoggedIn;
- (NSString *)applicationCacheDirectoryString;
- (void)showHUDLoadingView:(NSString *)strTitle;
- (void)hideHUDLoadingView;
+ (void)showProgress:(CGFloat)progress;
+ (void)showProgress:(CGFloat)progress status:(NSString*)status;
- (void)showToastMessage:(NSString *)message;
- (void)showLoadingWithTitle:(NSString *)title;
- (void)hideLoadingView;
- (BOOL)connected;
- (id)setBoldFontDiscriptor:(id)objc;

@end
