//
//  DriverDocumentVC.h
//  SwiftBack Driver
//
//  Created by Rohan on 27/01/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface DriverDocumentVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    
    
    NSMutableArray *arrDocumentTypes;
    NSMutableArray *arrDocumentSubTypes;
    
    __weak IBOutlet UITableView *tableForDocument;
    
    UIActionSheet *actionProof;
    
    BOOL isAdded;
}

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIView *backgroundRect;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (IBAction)onClickNext:(id)sender;
- (IBAction)onClickSkip:(id)sender;

@end
