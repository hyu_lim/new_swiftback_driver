//
//  BankAccountVc.m
//  SwiftBack Driver
//
//  Created by Rohan on 28/01/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "BankAccountVC.h"
#import "Stripe.h"
#import <pop/POP.h>
#include <ifaddrs.h>
#include <arpa/inet.h>


@interface BankAccountVC ()
{
    
    NSMutableArray *arrForCountry;
    NSArray *_pickerData;
}
@end

@implementation BankAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    myTextField.delegate = self;
    
    txtAccountNumber.delegate = self;
    
    txtBankName.delegate = self;
    
    txtBankCode.delegate = self;
    
    // Initialize Data
    _bankNames = @[@"ANZ",@"DBS/POSB", @"FEB",
                      @"UOB", @"OCBC", @"HSBC", @"Standard Chartered", @"Maybank"];
    
    _exchangeRates = @[ @"", @"", @"",
                        @"", @"", @"", @""];
    
    _bankCodeName = @[ @"ANZ",@"DBS/POSB", @"FEB", @"UOB",
                       @"OCBC", @"HSBC", @"Standard Chartered", @"Maybank"];
    
    _bankCodeNumber = @[ @"7931", @"7171", @"7199", @"7375",
                         @"7339", @"7232", @"7144", @"7302"];
    
    
    [self layoutSetup];
    [self setLocalizedStrings];
    [self setupLocalization];
    
    txtBankName.inputView = bankPicker;
    txtDateOfBirth.inputView = _datePicker;
    
    // Connect data
    self.bankPicker.dataSource = self;
    self.bankPicker.delegate = self;
    bankPicker.showsSelectionIndicator = YES;
    
    self->txtBankName.inputView = bankPicker;
    
    [self.bankPicker setHidden:YES];
    [self.btnBankPickerDone setHidden:YES];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.25;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [self.bankCodePicker.layer addAnimation:transition forKey:nil];
    [self.bankPicker.layer addAnimation:transition forKey:nil];
    [self.btnBankPickerDone.layer addAnimation:transition forKey:nil];
    [self.datePickerBackground.layer addAnimation:transition forKey:nil];
    [self.datePicker.layer addAnimation:transition forKey:nil];
    [self.btnDatePickerDone.layer addAnimation:transition forKey:nil];
    
    //Date Picker
    //datePicker = [[UIDatePicker alloc] init];
    //datePicker.datePickerMode = UIDatePickerModeDate;
    //[datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSString *dateString = [dateFormatter stringFromDate:self.datePicker.date];
    
    //self->txtDateOfBirth.inputView = _datePicker;
    
    //NSString *resultString = [[NSString alloc] initWithFormat:
                             // @"%@",
                             // _datePicker];
    
    //txtDateOfBirth.text = dateString;
    
    //self->txtDateOfBirth.inputView = _datePicker;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
}

-(void)dismissKeyboard {
    [txtAccountHolderName resignFirstResponder];
    [txtBankName resignFirstResponder];
    [txtAccountNumber resignFirstResponder];
    [txtBankCode resignFirstResponder];
    [txtBranchCode resignFirstResponder];
    [txtIdentificationNumber resignFirstResponder];
    [txtDateOfBirth resignFirstResponder];
    [txtAddressLine1 resignFirstResponder];
    [txtAddressLine2 resignFirstResponder];
    [txtPostal resignFirstResponder];
    [txtCountry resignFirstResponder];
    [txtState resignFirstResponder];
    [txtCity resignFirstResponder];
    [txtCurrency resignFirstResponder];
    
    [myTextField resignFirstResponder];
    
}


- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    
                }
                
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
    
}


#pragma mark -
#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    //float bankName = [_exchangeRates[row] floatValue];
    //float bankName = [txtBankName.text floatValue];
    //float result = dollars * rate;
    
    NSString *resultString = [[NSString alloc] initWithFormat:
                              @"%@",
                              _bankNames[row]];
    
    txtBankName.text = resultString;
    
    NSString *bankCodeString = [[NSString alloc] initWithFormat:
                              @"%@",_bankCodeNumber[row]];
    
    txtBankCode.text = bankCodeString;
   
}





#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return _bankNames.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return _bankNames[row];
}

-(IBAction)textFieldReturn:(id)sender
{
    [sender resignFirstResponder];
}

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)bankCodePicker:(UIPickerView *)bankCodePicker
numberOfRowsInComponent:(NSInteger)component
{
    
    return _bankCodeName.count;
    return _bankCodeNumber.count;
}

- (NSString *)bankCodePicker:(UIPickerView *)bankCodePicker
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    
    return _bankCodeName[row];
    return _bankCodeName[row];
}




- (void) setupLocalization {
    NSUserDefaults *pref = [[NSUserDefaults alloc] init];
    [pref synchronize];
    NSString *country = [pref valueForKey:@"CURRENT_COUNTRY"];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"countrycodes" ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    arrForCountry = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    NSDictionary *dictCountry = [[NSDictionary alloc] init];
    
    for(int i = 0; i<arrForCountry.count; i++) {
        dictCountry = arrForCountry[i];
        NSLog(@"DictCountry : %@", [dictCountry valueForKey:@"name"]);
        if([[dictCountry valueForKey:@"name"] isEqualToString:[country capitalizedString]]) {
            //[self.btnCountryCode setTitle:[dictCountry valueForKey:@"phone-code"] forState:UIControlStateNormal];
            
            self->txtCountry.text = [dictCountry valueForKey:@"alpha-2"];
            self->txtState.text = [dictCountry valueForKey:@"alpha-2"];
            self->txtCity.text = [dictCountry valueForKey:@"alpha-2"];
            self->txtCurrency.text = [dictCountry valueForKey:@"currency"];
    
            
            break;
        }
    }
    
    self->txtAccountHolderName.text = [NSString stringWithFormat:@"%@ %@",[arrUser valueForKey:@"first_name"],[arrUser valueForKey:@"last_name"]];
}


- (void)setLocalizedStrings {
    
    [self.lblTitle setText:NSLocalizedString(@"PAYMENT_RECEIVABLE_DETAILS", nil)];
    [self.lblAccountType setText:NSLocalizedString(@"ACCOUNT_TYPE", nil)];
    [self.lblAccountNumber setText:NSLocalizedString(@"ACCOUNT_NUMBER", nil)];
    [self.lblAddress setText:NSLocalizedString(@"ADDRESS", nil)];
    [self.lblRoutingNumber setText:NSLocalizedString(@"ROUTING_NUMBER", nil)];
    
    [self.btnSkip setTitle:NSLocalizedString(@"SKIP",nil ) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"SUBMIT",nil ) forState:UIControlStateNormal];
}

- (void)layoutSetup {
    [scrollView setContentSize:CGSizeMake(0, 200)];
    
    // backgroundRect
    _backgroundRect.layer.cornerRadius = 10;
    //_backgroundRect.layer.shadowRadius = 5.0;
    //_backgroundRect.layer.shadowOpacity = 0.4;
    /*
    // btnSkip
    _btnSkip.layer.cornerRadius = 5;
    _btnSkip.layer.borderWidth = 0.7;
    _btnSkip.layer.borderColor = [UIColor whiteColor].CGColor;
    
    // btnSubmit
    _btnSubmit.layer.cornerRadius = 5;
    _btnSubmit.layer.borderWidth = 0.7;
    _btnSubmit.layer.borderColor = [UIColor whiteColor].CGColor;
     */
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //View Controller Clipping
    _btnSubmit.layer.cornerRadius = 18;
    _btnSubmit.clipsToBounds = YES;
    
    //View Controller Clipping
    _btnSkip.layer.cornerRadius = 18;
    _btnSkip.clipsToBounds = YES;
    _btnSkip.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _btnSkip.layer.borderWidth = 1;
    
    
    [self.scrollView setScrollEnabled:YES];
    [self.scrollView setContentSize:CGSizeMake(320, 1000)];
    
    if(!UIAccessibilityIsReduceTransparencyEnabled() ) {
        //self.notificationView.backgroundColor = [UIColor darkGrayColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        
        blurEffectView.frame = self.notificationView.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.notificationView addSubview:blurEffectView];
        
    } else {
        self.notificationView.backgroundColor = [UIColor blackColor];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
        CGPoint offset;
        offset=CGPointMake(0, 0);
        [self.scrollView setContentOffset:offset animated:YES];
        
        if(textField==self->txtAccountHolderName)
            [self->txtAccountHolderName resignFirstResponder];
        else if(textField==self->txtBankName)
           // [self.lblAccountNumber becomeFirstResponder];
    
    [textField resignFirstResponder];
    return YES;
}





-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    

        if (textField == txtAccountHolderName) {
            [self.btnDismiss setHidden:NO];
            [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
        
        else if (textField == txtBankName){
            [self.btnDismiss setHidden:NO];
            [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            [self.bankPicker setHidden:NO];
            [self.btnBankPickerDone setHidden:NO];
            [txtBankName resignFirstResponder];
            [self dismissKeyboard];
           
            CATransition *transition = [CATransition animation];
            transition.duration = 0.25;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionFade;
            transition.delegate = self;
            [self.bankPicker.layer addAnimation:transition forKey:nil];
        }
        else if (textField == txtAccountNumber){
            [self.btnDismiss setHidden:NO];
            [self.scrollView setContentOffset:CGPointMake(0, 40) animated:YES];
        }
        else if (textField == txtBankCode){
            [self.btnDismiss setHidden:NO];
            [self.scrollView setContentOffset:CGPointMake(0, 80) animated:YES];
            [txtAccountNumber resignFirstResponder];
            [txtBankCode resignFirstResponder];
            [self dismissKeyboard];
            
            [txtAccountNumber resignFirstResponder];
            [self.bankPicker setHidden:NO];
            [self.btnBankPickerDone setHidden:NO];
            [txtBankCode resignFirstResponder];
            
            [self dismissKeyboard];
            
            
            CATransition *transition = [CATransition animation];
            transition.duration = 0.25;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionFade;
            transition.delegate = self;
            [self.bankPicker.layer addAnimation:transition forKey:nil];
            
            // Show you custom picker here....
        }
        else if (textField == txtBranchCode){
            [self.btnDismiss setHidden:NO];
            [self.scrollView setContentOffset:CGPointMake(0, 120) animated:YES];
        }
        else if (textField == txtIdentificationNumber){
            [self.btnDismiss setHidden:NO];
            [self.scrollView setContentOffset:CGPointMake(0, 160) animated:YES];
        }
        else if (textField == txtDateOfBirth){
            [self.btnDismiss setHidden:NO];
            [self.scrollView setContentOffset:CGPointMake(0, 200) animated:YES];
            [self dismissKeyboard];
            [self.datePicker setHidden:NO];
            [self.datePickerBackground setHidden:NO];
            [self.btnDatePickerDone setHidden:NO];
            [txtDateOfBirth resignFirstResponder];
            CATransition *transition = [CATransition animation];
            transition.duration = 0.25;
            transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
            transition.type = kCATransitionFade;
            transition.delegate = self;
            [self.datePicker.layer addAnimation:transition forKey:nil];
            
        }
        else if (textField == txtAddressLine1){
            [self.scrollView setContentOffset:CGPointMake(0, 240) animated:YES];
        }
        else if (textField == txtAddressLine2){
            [self.scrollView setContentOffset:CGPointMake(0, 280) animated:YES];
        }
        else if (textField == txtPostal){
            [self.scrollView setContentOffset:CGPointMake(0, 320) animated:YES];
        }
        else if (textField == txtCountry){
            [self.scrollView setContentOffset:CGPointMake(0, 360) animated:YES];
        }
        else if (textField == txtState){
            [scrollView setContentOffset:CGPointMake(0, 400) animated:YES];
        }
        else if (textField == txtCity){
            [scrollView setContentOffset:CGPointMake(0, 440) animated:YES];
        }
        else if (textField == txtCurrency){
            [scrollView setContentOffset:CGPointMake(0, 480) animated:YES];
        }
        else{
            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    
    
    
    
    
    
}



-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == txtAccountType)
        [txtAccountNumber resignFirstResponder];
    else if (textField == txtAccountNumber)
        [txtAccountNumber resignFirstResponder];
    else if (textField == txtBankCode)
        [txtAccountNumber resignFirstResponder];
    else
        [textField resignFirstResponder];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)datePickerDone:(id)sender {
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/YYYY"];
    NSString *dateString = [dateFormatter stringFromDate:self.datePicker.date];
    
    //self->txtDateOfBirth.inputView = _datePicker;
    
    //NSString *resultString = [[NSString alloc] initWithFormat:
    // @"%@",
    // _datePicker];
    
    txtDateOfBirth.text = dateString;
    
    txtBankName.inputView = bankPicker;
    
    
    // Connect data
    //_datePicker.showsSelectionIndicator = YES;
    
    self->txtDateOfBirth.inputView = _datePicker;
    
    [self.datePickerBackground setHidden:YES];
    [self.datePicker setHidden:YES];
    [self.btnDatePickerDone setHidden:YES];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.25;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [self.datePickerBackground.layer addAnimation:transition forKey:nil];
    [self.datePicker.layer addAnimation:transition forKey:nil];
    [self.btnDatePickerDone.layer addAnimation:transition forKey:nil];
    
    
    
}
- (IBAction)dismissResponder:(id)sender {
    
    
    [self.bankCodePicker setHidden:YES];
    [self.bankPicker setHidden:YES];
    [self.btnBankPickerDone setHidden:YES];
    [self.datePickerBackground setHidden:YES];
    [self.datePicker setHidden:YES];
    [self.btnDatePickerDone setHidden:YES];
    [txtAccountHolderName resignFirstResponder];
    [txtBankName resignFirstResponder];
    [txtAccountNumber resignFirstResponder];
    [txtBankCode resignFirstResponder];
    [txtBranchCode resignFirstResponder];
    [txtIdentificationNumber resignFirstResponder];
    [txtDateOfBirth resignFirstResponder];
    [txtAddressLine1 resignFirstResponder];
    [txtAddressLine2 resignFirstResponder];
    [txtPostal resignFirstResponder];
    [txtCountry resignFirstResponder];
    [txtState resignFirstResponder];
    [txtCity resignFirstResponder];
    [txtCurrency resignFirstResponder];
    [myTextField resignFirstResponder];
    [self.btnDismiss setHidden:YES];
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.25;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [self.bankCodePicker.layer addAnimation:transition forKey:nil];
    [self.bankPicker.layer addAnimation:transition forKey:nil];
    [self.btnBankPickerDone.layer addAnimation:transition forKey:nil];
    [self.datePickerBackground.layer addAnimation:transition forKey:nil];
    [self.datePicker.layer addAnimation:transition forKey:nil];
    [self.btnDatePickerDone.layer addAnimation:transition forKey:nil];
    
    
}

- (IBAction)bankPickerDone:(id)sender {
    
    
    
    txtBankName.inputView = bankPicker;
    
    
    // Connect data
    self.bankPicker.dataSource = self;
    self.bankPicker.delegate = self;
    bankPicker.showsSelectionIndicator = YES;
    
    self->txtBankName.inputView = bankPicker;
    
    [self.bankPicker setHidden:YES];
    [self.btnBankPickerDone setHidden:YES];
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.25;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    transition.delegate = self;
    [self.bankCodePicker.layer addAnimation:transition forKey:nil];
    [self.bankPicker.layer addAnimation:transition forKey:nil];
    [self.btnBankPickerDone.layer addAnimation:transition forKey:nil];
    [self.datePickerBackground.layer addAnimation:transition forKey:nil];
    [self.datePicker.layer addAnimation:transition forKey:nil];
    [self.btnDatePickerDone.layer addAnimation:transition forKey:nil];
    
}

- (IBAction)onClickSubmit:(id)sender {
    
    if ([APPDELEGATE connected]) {
        
        /*if ([[[UtilityClass sharedObject] trimString:txtAccountType.text] length]  < 1) {
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please enter account type", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else */
        if ([[[UtilityClass sharedObject] trimString:txtBankName.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLease select bank name", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtAccountNumber.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLease enter account number", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtBankCode.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLease enter bank code", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtIdentificationNumber.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLease enter identification number", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtDateOfBirth.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLease enter your date of birth", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtCountry.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"Please enter address", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else if ([[[UtilityClass sharedObject] trimString:txtPostal.text] length] < 1)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"PLease enter your postal code", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
            
            NSString *address = @"error";
            struct ifaddrs *interfaces = NULL;
            struct ifaddrs *temp_addr = NULL;
            int success = 0;
            // retrieve the current interfaces - returns 0 on success
            success = getifaddrs(&interfaces);
            if (success == 0) {
                // Loop through linked list of interfaces
                temp_addr = interfaces;
                while(temp_addr != NULL) {
                    if(temp_addr->ifa_addr->sa_family == AF_INET) {
                        // Check if interface is en0 which is the wifi connection on the iPhone
                        if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                            // Get NSString from C String
                            address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                            
                        }
                        
                    }
                    
                    temp_addr = temp_addr->ifa_next;
                }
            }
            // Free memory
            freeifaddrs(interfaces);
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
            [dictParam setObject:[defaults objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            
            [dictParam setObject:[defaults objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            //Hidden Forms Bank Token Details
            [dictParam setObject:@"individual" forKey:PARAM_ACCOUNT_TYPE];
            [dictParam setObject:address forKey:PARAM_IP_ADDRESS];
            
            //[dictParam setObject:txtAccountType.text forKey:PARAM_ACCOUNT_TYPE];
            
            
            //[dictParam setObject:txtRoutingNumber.text forKey:PARAM_ROUTING_NUMBER];
            
            //Hidden Forms Account Details
            
            //Stripe Bank Token Details
            
            [dictParam setObject:txtAccountHolderName.text forKey:PARAM_ACCOUNT_HOLDER_NAME];
            [dictParam setObject:txtBankName.text forKey:PARAM_BANK_NAME];
            [dictParam setObject:txtAccountNumber.text forKey:PARAM_ACCOUNT_NUMBER];
            [dictParam setObject:txtCountry.text forKey:PARAM_COUNTRY];
            
            [dictParam setObject:txtCurrency.text forKey:PARAM_CURRENCY];
            
            
            
            //Stripe Managed Account Details
            
            [dictParam setObject:txtIdentificationNumber.text forKey:PARAM_IDENTIFICATION_NUMBER];
            [dictParam setObject:txtDateOfBirth.text forKey:PARAM_DATE_OF_BIRTH];
            [dictParam setObject:txtAddressLine1.text forKey:PARAM_ADDRESS_2];
            
            [dictParam setObject:txtAddressLine1.text forKey:PARAM_ADDRESS_1];
            
            
            [dictParam setObject:txtPostal.text forKey:PARAM_POSTAL_CODE];
            [dictParam setObject:txtCity.text forKey:PARAM_ACCOUNT_CITY];
            [dictParam setObject:txtState.text forKey:PARAM_ACCOUNT_STATE];
            
            //[dictParam setObject:txtToken.text forKey:PARAM_ACCOUNT_TOKEN];
            [dictParam setObject:[defaults objectForKey:PREF_USER_TOKEN] forKey:PARAM_ACCOUNT_TOKEN];
            [dictParam setObject:[defaults objectForKey:PREF_USER_ID] forKey:PARAM_ACCOUNT_ID];
            
            //[dictParam setObject:txtID.text forKey:PARAM_ACCOUNT_ID];
            
            //[dictParam setObject:txtIpAddress.text forKey:PARAM_IP_ADDRESS];
            
            [dictParam setObject:txtBankCode.text forKey:PARAM_BANK_CODE];
            
            [dictParam setObject:txtBranchCode.text forKey:PARAM_BRANCH_CODE];
            
            
            
            //Hidden Submissions
            [dictParam setObject:txtAccountType.text forKey:PARAM_TOKEN_ACCOUNT_TYPE];
            [dictParam setObject:txtAccountNumber.text forKey:PARAM_TOKEN_ACCOUNT_NUMBER];
            
            
            [APPDELEGATE showLoadingWithTitle:@"Loading..."];
            AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [helper getDataFromPath:FILE_BANKDETAILS withParamData:dictParam withBlock:^(id response, NSError *error) {
                [APPDELEGATE hideLoadingView];
                if (response) {
                    if ([[response valueForKey:@"success"]boolValue]) {
                        
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Documents Sucessfully Submitted" message:@"Pending admin approval in 24 - 48 hrs" delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        [alert show];
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setBool:YES forKey:@"isFromBank"];
                        [defaults synchronize];
                        [self dismissViewControllerAnimated:YES completion:nil];
                        
                        

                    }
                    else{
                        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:NSLocalizedString(@"An error occured, Please ensure all fields are filled and try again", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                        [alert show];
                    }
                }
            }];
        }
    }
    else{
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    /*STPBankAccountParams *bankAccount = [[STPBankAccountParams alloc] init];
    bankAccount.accountNumber = self.lblAccountType.text;
    bankAccount.routingNumber = self.lblAccountNumber.text;
    bankAccount.country = self.lblAddress.text;
    bankAccount.routingNumber = self.lblRoutingNumber.text;
    bankAccount.accountHolderName = txtAccountHolderName.text;
    bankAccount.currency = txtCurrency.text;
    
    [Stripe createTokenWithBankAccount:bankAccount completion:^(STPToken *token, NSError *error) {
      
    }];*/
}

- (IBAction)onClickSkip:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
