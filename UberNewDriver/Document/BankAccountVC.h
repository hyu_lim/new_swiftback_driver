//
//  BankAccountVc.h
//  SwiftBack Driver
//
//  Created by Rohan on 28/01/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BankAccountVC : UIViewController<UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate>
{
    UITextField *myTextField;
    UIPickerView *myPickerView;
    NSArray *pickerArray;
    
    __weak IBOutlet UIPickerView *bankPicker;
    
    __weak IBOutlet UITextField *txtAccountType;
    
    __weak IBOutlet UITextField *txtAccountNumber;
    
    __weak IBOutlet UITextField *txtCountry;
    
    __weak IBOutlet UITextField *txtRoutingNumber;
    
    __weak IBOutlet UIScrollView *scrollView;
    
    __weak IBOutlet UITextField *txtIdentificationNumber;
    
    __weak IBOutlet UITextField *txtDateOfBirth;
    
    __weak IBOutlet UITextField *txtAddressLine1;
    
    __weak IBOutlet UITextField *txtAddressLine2;
    
    __weak IBOutlet UITextField *txtPostal;
    
    __weak IBOutlet UITextField *txtCity;
    
    __weak IBOutlet UITextField *txtState;
    
    __weak IBOutlet UITextField *txtToken;
    
    __weak IBOutlet UITextField *txtID;
    
    __weak IBOutlet UITextField *txtIpAddress;
    
    __weak IBOutlet UITextField *txtBankCode;
    
    __weak IBOutlet UITextField *txtBranchCode;
    
    
    __weak IBOutlet UITextField *txtBankName;
    __weak IBOutlet UITextField *txtCurrency;
    __weak IBOutlet UITextField *txtAccountHolderName;
}
@property (weak, nonatomic) IBOutlet UIView *notificationView;
@property (weak, nonatomic) IBOutlet UIButton *dismissResponder;
@property (weak, nonatomic) IBOutlet UIButton *btnDismiss;

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) NSArray *bankCodeName;
@property (strong, nonatomic) NSArray *bankCodeNumber;
@property (strong, nonatomic) NSArray *bankNames;
@property (strong, nonatomic) NSArray *exchangeRates;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIPickerView *bankCodePicker;

@property (weak, nonatomic) IBOutlet UIView *datePickerBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnDatePickerDone;
@property (weak, nonatomic) IBOutlet UIButton *btnBankPickerDone;
@property (weak, nonatomic) IBOutlet UIPickerView *bankPicker;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAccountType;
@property (weak, nonatomic) IBOutlet UILabel *lblAccountNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblRoutingNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIView *backgroundRect;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)onClickSubmit:(id)sender;
- (IBAction)onClickSkip:(id)sender;

@end
