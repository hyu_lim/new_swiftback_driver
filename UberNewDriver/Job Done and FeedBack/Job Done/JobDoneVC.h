
#import "BaseVC.h"
#import "SWRevealViewController.h"


@interface JobDoneVC : BaseVC
@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)btnPressedJobDone:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;

@end
