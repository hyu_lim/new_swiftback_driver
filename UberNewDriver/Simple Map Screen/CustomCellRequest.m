
#import "CustomCellRequest.h"
#import "UIView+Utils.h"

@implementation CustomCellRequest

- (void)awakeFromNib {
    [self.imgUser applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
