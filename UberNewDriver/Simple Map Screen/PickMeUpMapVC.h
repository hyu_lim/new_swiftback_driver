
#import "BaseVC.h"
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
#import "sbMapAnnotation.h"
#import <CoreLocation/CoreLocation.h>
#import "LDProgressView.h"
#import "UIColor+RGBValues.h"
#import <GoogleMaps/GoogleMaps.h>
#import <AVFoundation/AVAudioPlayer.h>
#import "AudioToolbox/AudioToolbox.h"
#import "ListVC.h"
#import "ASStarRatingView.h"
#import "UserListVC.h"

@class SideBarVC;
@class ArrivedMapVC,RatingBar;

@interface PickMeUpMapVC : BaseVC <MKAnnotation,MKMapViewDelegate,GMSMapViewDelegate,dataParser,CLLocationManagerDelegate>
{
    Reachability *internetReachableFoo;
    BOOL internet;
	BOOL goToArrive;

    UIImageView* routeView;
    
	NSArray* routes;
	
	UIColor* lineColor;
    
    LDProgressView *progressView;
    GMSMapView *mapView_;
    GMSMarker *marker;
    
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}
@property (strong,nonatomic) NSString *advance_Request_ID;

@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeBack;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeText;
@property (weak, nonatomic) IBOutlet UILabel *lblNextPickUpText;
@property (weak, nonatomic) IBOutlet UILabel *lblCounter;
@property (weak, nonatomic) IBOutlet UILabel *lblBlue;
@property (weak, nonatomic) IBOutlet UILabel *lblGrey;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;
@property (weak, nonatomic) IBOutlet UILabel *lblWhite;
@property (weak, nonatomic) IBOutlet UILabel *lblNotApproved;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblPassengerName;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceTo;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceCost;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceDate;
@property (weak, nonatomic) IBOutlet UILabel *lblAdvanceTime;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *btnProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *btnOfferRide;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSTimer *time;
@property (nonatomic, strong) NSTimer *progtime;
@property (nonatomic, strong) NSTimer *counterTimer;

@property (weak, nonatomic) IBOutlet UIView *welcomeBackView;
@property (weak, nonatomic) IBOutlet UIView *blurView2;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *etaView;
@property (weak, nonatomic) IBOutlet UIView *datePicker;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIView *ProfileView;
@property (weak, nonatomic) IBOutlet UIView *viewForPickup;
@property (weak, nonatomic) IBOutlet UIView *viewForMap;
@property (weak, nonatomic) IBOutlet UIView *viewForNotApproved;
@property (weak, nonatomic) IBOutlet UIView *viewForAdvancePickup;
@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (weak, nonatomic) IBOutlet UIImageView *imgStar;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (weak, nonatomic) IBOutlet UIImageView *gradientRect;
@property (weak, nonatomic) IBOutlet UIImageView *advanceProfileImgView;
@property (weak, nonatomic) IBOutlet UIButton *closeNotice;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView_;
@property (weak, nonatomic) IBOutlet UIProgressView *progressTimer;
@property (weak, nonatomic) IBOutlet UIImageView *imgTimeBg;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImgView;
@property (weak, nonatomic) IBOutlet UIButton *updateAccount;

@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (weak, nonatomic) IBOutlet RatingBar *advanceRatingView;

@property (strong, nonatomic) AVAudioPlayer *sound1Player;
@property (nonatomic, strong) ArrivedMapVC *arrivedMap;
@property (weak, nonatomic) IBOutlet UIView *updateBankingView;
@property (weak, nonatomic) IBOutlet UIView *verifyAccountView;

- (IBAction)pickMeBtnPressed:(id)sender;
- (IBAction)onClickConfirmPickup:(id)sender;
- (IBAction)onClickClose:(id)sender;
- (IBAction)onClickSetEta:(id)sender;
- (IBAction)onClickReject:(id)sender;
- (IBAction)onClickAccept:(id)sender;
- (IBAction)onClickNoKey:(id)sender;
- (IBAction)onClickAdvanceStart:(id)sender;

- (void)onClickAcceptRide:(UIButton*)sender;
- (void)onClickRejectRide:(UIButton*)sender;
- (void)goToSetting:(NSString *)str;
- (void)goToRide;


@end
