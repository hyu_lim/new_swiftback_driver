
#import "ListVC.h"
#import "SWRevealViewController.h"
#import "CustomCellRequest.h"
#import "UIView+Utils.h"
#import "UIImageView+Download.h"

@interface ListVC ()
{
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableDictionary *dictOwner;
    NSMutableString *strRequsetId;
    NSMutableDictionary *dict;
    NSMutableArray *arrAllRequests;

}
@end

@implementation ListVC
@synthesize tblRequests;

- (void)viewDidLoad
{
    [super viewDidLoad];
    internet=[APPDELEGATE connected];
    [super viewDidLoad];
    
    [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle: ) forControlEvents:UIControlEventTouchUpInside];
    
    [self getAllRequests];
    NSRunLoop *runloop = [NSRunLoop currentRunLoop];
    self.timerGetRequests = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(getAllRequests) userInfo:nil repeats:YES];
    [runloop addTimer:self.timerGetRequests forMode:NSRunLoopCommonModes];
    [runloop addTimer:self.timerGetRequests forMode:UITrackingRunLoopMode];
    
    self.imgNoItems.hidden=YES;
    [self setNeedsStatusBarAppearanceUpdate];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //Swipe to reveal menu
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        
        
    }
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    internet=[APPDELEGATE connected];
}

#pragma mark-
#pragma mark- API Methods

-(void)getAllRequests
{
    
    if(internet)
    {
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strUserId = [pref valueForKey:PARAM_ID];
        strUserToken = [pref valueForKey:PARAM_TOKEN];
        [pref synchronize];
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:strUserId forKey:PARAM_ID];
        [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             
             NSLog(@"Get All Request= %@",response);
             
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     
                     NSMutableArray *arrRespone=[response valueForKey:@"incoming_requests"];
                     if(arrRespone.count!=0 )
                     {
                         NSString *strCheckID = [NSString stringWithFormat:@"%@",[[arrRespone objectAtIndex:0] objectForKey:@"request_id"] ];
                         if (![strCheckID isEqualToString:@"-1"])
                         {
                             arrAllRequests=arrRespone;
                             
                             
                             //                         NSMutableDictionary *dictRequestData=[arrRespone valueForKey:@"request_data"];
                             //
                             //                         NSMutableArray *arrOwner=[dictRequestData valueForKey:@"owner"];
                             //                         dictOwner=[arrOwner objectAtIndex:0];
                             //
                             //                         NSMutableArray *arrRequest_Id=[arrRespone valueForKey:@"request_id"];
                             //                         strRequsetId=[NSMutableString stringWithFormat:@"%@",[arrRequest_Id objectAtIndex:0]];
                             //
                             //                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                             //                         [pref setObject:strRequsetId forKey:PREF_REQUEST_ID];
                             //                         [pref synchronize];
                             
                             //                         lblName.text=[dictOwner valueForKey:@"name"];
                             //                         lblRate.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"rating"]];
                             //                         lblDetails.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"phone"]];
                             //                         [self.imgUserProfile downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
                             
                             strowner_lati=[dictOwner valueForKey:@"latitude"];
                             strowner_longi=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"longitude"]];
                             RBRatings rate=([[dictOwner valueForKey:@"rating"]floatValue]*2);
                             [ self.ratingView setRatings:rate];
                             
                             payment=[[dictOwner valueForKey:@"payment_type"] intValue];
                             [self.tblRequests reloadData];
                             self.tblRequests.hidden=NO;

                             //[self show];
                             //[self centerMapFirst:current two:currentOwner third:current];
                             self.imgNoItems.hidden=YES;

                         }
                         else
                         {
                             self.tblRequests.hidden=YES;
                             self.imgNoItems.hidden=NO;
                         }
                     }
                     else
                     {
                         self.tblRequests.hidden=YES;
                         self.imgNoItems.hidden=NO;
                     }
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
//
#pragma mark-
#pragma mark - TableView delegate methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrAllRequests.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 180;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cellRequest";
    
    CustomCellRequest *cell = [tblRequests dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[CustomCellRequest alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSMutableDictionary *dictReq=[arrAllRequests objectAtIndex:indexPath.row];
    
    NSMutableDictionary *dictRequestData=[dictReq valueForKey:@"request_data"];
    
    NSMutableArray *arrOwner=[dictRequestData valueForKey:@"owner"];
    //dictOwner=[arrOwner objectAtIndex:indexPath.row];
    
    //NSMutableArray *arrRequest_Id=[arrRespone valueForKey:@"request_id"];
    //strRequsetId=[NSMutableString stringWithFormat:@"%@",[arrRequest_Id objectAtIndex:0]];
    
    //NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    //[pref setObject:strRequsetId forKey:PREF_REQUEST_ID];
    //[pref synchronize];
    
    //                         lblName.text=[dictOwner valueForKey:@"name"];
    //                         lblRate.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"rating"]];
    //                         lblDetails.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"phone"]];
    //                         [self.imgUserProfile downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
//    strowner_lati=[arrOwner valueForKey:@"latitude"];
//    strowner_longi=[NSString stringWithFormat:@"%@",[arrOwner valueForKey:@"longitude"]];
    
    
    payment=[[dictOwner valueForKey:@"payment_type"] intValue];
    
    
    [cell.ratingView initRateBar];
    [cell.ratingView setUserInteractionEnabled:NO];
    RBRatings rate=([[arrOwner valueForKey:@"rating"]floatValue]*2);
    [cell.ratingView setRatings:rate];
    
    [cell.imgUser downloadFromURL:[arrOwner valueForKey:@"picture"] withPlaceholder:nil];
    
    cell.lblName.text= [arrOwner valueForKey:@"name"];
    cell.lblFrom.text= [NSString stringWithFormat:@"From: %@",[arrOwner valueForKey:@"s_address"]];
    cell.lblTo.text= [NSString stringWithFormat:@"To: %@",[arrOwner valueForKey:@"d_address"]];
    
    cell.lblDist.text=[NSString stringWithFormat:@"%.2f km",[[arrOwner valueForKey:@"distance"] floatValue]];
    
    cell.lblPrice.text=[NSString stringWithFormat:@"$ %@",[arrOwner valueForKey:@"total"]];
    [cell.btnShowRoute setTag:indexPath.row];
    [cell.btnShowRoute addTarget:self action:@selector(openNavigation:) forControlEvents:UIControlEventTouchUpInside];
        
    /*
    float distance=[[arrOwner valueForKey:@"distance"] floatValue];
    if(distance< 32)
    {
        if(distance<16)
        {
            if(distance<8)
            {
                cell.lblPrice.text=@"$5";
            }
            else
            {
                cell.lblPrice.text=@"$10";
            }
        }
        else
        {
            cell.lblPrice.text=@"$15";
        }
    }
    else
    {
        cell.lblPrice.text=@"$15";
    }
    */
    
    [cell.btnAcceptRide addTarget:self action:@selector(onClickAcceptRide:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.btnRejectRide addTarget:self action:@selector(onClickRejectRide:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnAcceptRide.tag=indexPath.row;
    cell.btnRejectRide.tag=indexPath.row;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
- (IBAction)openNavigation:(UIButton*)sender {
    
    NSMutableDictionary *dictReq=[arrAllRequests objectAtIndex:sender.tag];
    
    NSMutableDictionary *dictRequestData=[dictReq valueForKey:@"request_data"];
    
    NSMutableArray *arrOwner=[dictRequestData valueForKey:@"owner"];
    
     NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%@,%@&daddr=%@,%@", [arrOwner valueForKey:@"latitude"], [arrOwner valueForKey:@"longitude"], [arrOwner valueForKey:@"d_latitude"], [arrOwner valueForKey:@"d_longitude"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:googleMapsURLString]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];

    }
}

-(void)onClickAcceptRide:(UIButton*)sender
{
    
   // if (is_accepted!=1)
    //{
        int reqNo=(int)sender.tag;
        NSMutableDictionary *dictReq=[arrAllRequests objectAtIndex:reqNo];
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        [pref setObject:dictReq forKey:@"requestInfo"];
        [pref synchronize];
        
        if ([self.delegate respondsToSelector:@selector(listLoaded:)])
        {
            [self.delegate listLoaded:dictReq];
            [self.navigationController popViewControllerAnimated:YES];
        }

   // }
    
}

-(void)onClickRejectRide:(UIButton*)sender
{
    int reqNo=(int)sender.tag;
    NSMutableDictionary *dictReq=[arrAllRequests objectAtIndex:reqNo];
    if(internet)
    {
        
        if ([dictReq valueForKey:@"request_id"]!=nil)
        {
            NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
            
            [dictparam setObject:[dictReq valueForKey:@"request_id"] forKey:PARAM_REQUEST_ID];
            [dictparam setObject:strUserId forKey:PARAM_ID];
            [dictparam setObject:strUserToken forKey:PARAM_TOKEN];
            [dictparam setObject:@"0" forKey:PARAM_ACCEPTED];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_RESPOND_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 
                 NSLog(@"Respond to Request= %@",response);
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         [self.tblRequests reloadData];
                     }
                 }
                 
             }];
        }
        else
        {
            
        }
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backButtonPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
