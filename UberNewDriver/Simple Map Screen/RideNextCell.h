
#import <UIKit/UIKit.h>
#import "RatingBar.h"
#import "SWTableViewCell.h"


@interface RideNextCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblFromStation;
@property (weak, nonatomic) IBOutlet UILabel *lblToStation;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureDate;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfRate;
@property (weak, nonatomic) IBOutlet UIButton *overlayButton;
@property (weak, nonatomic) IBOutlet UIImageView *paymentImgView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundCell;
@property (weak, nonatomic) IBOutlet UIImageView *overlayDriver;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@end
