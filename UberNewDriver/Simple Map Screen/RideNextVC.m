
#import "RideNextVC.h"
#import "RideNextCell.h"
#import "RideNowCell.h"
#import "SWRevealViewController.h"
#import "Constant.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import <MapKit/MapKit.h>
#import "AdvancePickupVC.h"
#import "RatingBar.h"


@interface RideNextVC (){
    
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableString *strRequsetId;
    UITableViewController *tableViewController;
    NSUInteger page,totalPagesNearest,totalPagesEarliest,NearestPage,EarliestPage;
    NSMutableArray *arrTempNearest,*arrTempEarliest;
    BOOL is_first;
    NSTimer *timerForPush;
    NSString *currencySign;
    NSString *currentCountry;
}

@end

@implementation RideNextVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customSetup];
    [self layoutSetup];
    [self setLocalizedStrings];
    
    NearestPage=1;
    EarliestPage=1;
    totalPagesEarliest=1;
    totalPagesNearest=1;
    is_first=YES;
    arrTempEarliest=[[NSMutableArray alloc] init];
    arrTempNearest=[[NSMutableArray alloc] init];
    
    //Pull to refresh table view configuration
    tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = tblRequestList;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor darkGrayColor];
    [self.refreshControl addTarget:self action:@selector(callRequestMethod) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;
    
    // for reload more data
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:40.0f tableView:tblRequestList withClient:self];
    
}

- (void)callRequestMethod
{
    if(self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        arrTempNearest=[[NSMutableArray alloc] init];
        NearestPage=1;
        [self getRequests:@"nearest" page:1];
    }
    else if(self.customSegmentedControl.selectedSegmentIndex == 1)
    {
        arrTempEarliest=[[NSMutableArray alloc] init];
        EarliestPage=1;
        [self getRequests:@"earliest" page:1];
    }
}

- (void)layoutSetup {
    
    [noItemsImgView setHidden:NO];
    
    //Segmented button properties
    CGRect frame= segmentToggle.frame;
    [segmentToggle setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 25)];
    
    _confirmRidesBtn.layer.cornerRadius = 7;
    _confirmRidesBtn.layer.borderWidth = 1;
    _confirmRidesBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //  counterTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(counter) userInfo:nil repeats:YES];
    
    //NYSegmentedControl
    
    self.customSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"NEAREST", nil), NSLocalizedString(@"EARLIEST", nil)]];
    self.customSegmentedControl.frame = CGRectMake(10, 60, 300, 35);
    [self.customSegmentedControl addTarget:self action:@selector(segmentSelected) forControlEvents:UIControlEventValueChanged];
    
    self.customSegmentedControl.titleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    self.customSegmentedControl.selectedTitleFont = [UIFont systemFontOfSize:12.0f];
    
    self.customSegmentedControl.backgroundColor = [UIColor colorWithRed:37.f/255.f green:131.f/255.f blue:111.f/255.f alpha:1.0f];

    self.customSegmentedControl.borderWidth = 0.0f;
    
    self.customSegmentedControl.segmentIndicatorBackgroundColor = [UIColor colorWithRed:59.f/255.f green:177.f/255.f blue:156.f/255.f alpha:1.0f];
    self.customSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    self.customSegmentedControl.segmentIndicatorInset = 2.0f;
    self.customSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    
    self.customSegmentedControl.cornerRadius = CGRectGetHeight(self.customSegmentedControl.frame) / 2.0f;
    
    [self.view addSubview:self.customSegmentedControl];
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
}

- (void)setLocalizedStrings {
    [self.lblNavTitle setText:NSLocalizedString(@"ADVANCE_RIDES", nil)];
}

- (void)viewWillAppear:(BOOL)animated
{
    if(self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        arrTempNearest=[[NSMutableArray alloc] init];
        NearestPage=1;
        [self getRequests:@"nearest" page:1];
    }
    else if(self.customSegmentedControl.selectedSegmentIndex == 1)
    {
        arrTempEarliest=[[NSMutableArray alloc] init];
        EarliestPage=1;
        [self getRequests:@"earliest" page:1];
    }
    timerForPush=[NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(checkpush) userInfo:nil repeats:YES];
}
-(void)checkpush
{
    if(pushId==5)
    {
        pushId=0;
        [self performSegueWithIdentifier:SEGUE_TO_ADVANCE_PICKUP sender:pushDict];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

//-(void)counter
//{
//    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
//    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
//    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
//    
//    AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
//    [helper getDataFromPath:FILE_REQUEST_COUNTER withParamData:dictParam withBlock:^(id response, NSError *error) {
//        if (response)
//        {
//            if ([[response objectForKey:@"success"] boolValue])
//            {
//                if ([[response objectForKey:@"count"] integerValue ]== 0)
//                {
//                    //[self.confirmRidesBtn setHidden:YES];
//                }
//                else
//                {
//                    [self.confirmRidesBtn setHidden:NO];
//                    [self.confirmRidesBtn setTitle:[NSString stringWithFormat:@"%ld",(long)[[response objectForKey:@"count"] integerValue]] forState:UIControlStateNormal];
//                    [self.confirmRidesBtn setBackgroundColor:[UIColor redColor]];
//                    [self.confirmRidesBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//                }
//            }
//        }
//    }];
//    
//    
//}

#pragma mark
#pragma mark Reveal Setup

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [menuBtn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [_menuOn addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [viewForNavigation addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        [_mainView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
}

#pragma mark
#pragma mark - Get Future reqeusts

/*-(void)getRequests:(NSString*)type
{
    NSLog(@"TYPE : %@", type);
    
    if ([APPDELEGATE connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:@"1" forKey:type];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_ALL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error) {
            
            [APPDELEGATE hideLoadingView];
            
            if (response)
            {
                if ([[response objectForKey:@"success"] boolValue])
                {
                    if ([type isEqualToString:@"nearest"]) {
                        nearestRequest = [[response valueForKey:@"request"] mutableCopy];
                        
                    }
                    else{
                        earliestRequest = [[response valueForKey:@"request"] mutableCopy];
                    }
                    
                    if ([type isEqualToString:@"nearest"] && [nearestRequest count] > 0)
                    {
                        
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                    }
                    else if ([type isEqualToString:@"earliest"] && [earliestRequest count] >0){
                        
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                        
                    }
                    else
                    {
                        [tblRequestList setHidden:YES];
                        [noItemsImgView setHidden:NO];
                    }
                    
                }
                else{
                    
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                    [tblRequestList setHidden:YES];
                    [noItemsImgView setHidden:NO];
                }
                
               
            }
            else
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
            }
             [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }];
    }
} */
- (void)reloadData {
    //Reload table data
    [tblRequestList reloadData];
    
    //End the refreshing
    if(self.refreshControl) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update : %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrDictionary = [NSDictionary dictionaryWithObject:[UIColor darkGrayColor] forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        [self.refreshControl endRefreshing];
    }
}

#pragma mark
#pragma mark - TableView Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        return [nearestRequest count];
        
    }
    else if (self.customSegmentedControl.selectedSegmentIndex == 1){
        
        return [earliestRequest count];
    }
    else
    {
        return [confirmRequests count];
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictRequest;
    
    RideNowCell *cellZero = [tableView dequeueReusableCellWithIdentifier:@"CellZero"];
    RideNextCell *cellOne = [tableView dequeueReusableCellWithIdentifier:@"CellOne"];

    
//    if (segmentToggle.selectedSegmentIndex == 0) {
//         dictRequest = [nearestRequest objectAtIndex:indexPath.row];
//    }
//    else if (segmentToggle.selectedSegmentIndex == 1){
//         dictRequest = [earliestRequest objectAtIndex:indexPath.row];
//    }
//    else{
//        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
//    }
    
    if (self.customSegmentedControl.selectedSegmentIndex == 0) {
        dictRequest = [nearestRequest objectAtIndex:indexPath.row];
    }
    else if (self.customSegmentedControl.selectedSegmentIndex == 1){
        dictRequest = [earliestRequest objectAtIndex:indexPath.row];
    }
    else{
        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
    }

    
    NSDictionary *dictOwner = [dictRequest  valueForKey:@"owner"];

    
    if ([[dictRequest objectForKey:@"later"]integerValue] == 0) {
     
        //Ride Now Properties
//        cellZero.layer.cornerRadius = 10;
//        cellZero.layer.shadowRadius = 5.0;
//        cellZero.layer.shadowOpacity = 0.4;
//        cellZero.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        cellZero.layer.borderWidth = 0.5;
//        cellZero.layer.opacity = 0.6;
//        cellZero.layer.backgroundColor = (__bridge CGColorRef)([UIColor colorWithRed:1 green:1 blue:1 alpha:1]);
//        cellZero.clipsToBounds = YES;
        
        UIView *viewForCellZero = [cellZero viewWithTag:903];
        viewForCellZero.layer.cornerRadius = 10;
        viewForCellZero.clipsToBounds = YES;

        //Adjust horizontal and vertical lines size
        UILabel *horizontalLine = [cellZero viewWithTag:906];
        UILabel *verticalLine = [cellZero viewWithTag:907];
        
        horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
        
        verticalLine.frame = CGRectMake(verticalLine.frame.origin.x, verticalLine.frame.origin.y, 0.5, verticalLine.frame.size.height);

        if ([[dictRequest objectForKey:@"payment_mode"]boolValue]) {
            // Cash Image
            [cellZero.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
        }
        else{
            [cellZero.paymentImgView setImage:[UIImage imageNamed:@"icon_card-01"]];
        }

        [cellZero.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
        [cellZero.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];
        
        
        [cellZero.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:@"total"]]];
        [cellZero.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
        
        [cellZero.lblPassengerName setText:[dictOwner valueForKey:@"name"]];
        [cellZero.lblNumberOfRate setText:[NSString stringWithFormat:@"(%@)",[dictOwner valueForKey:@"num_rating"]]];
        
        [cellZero.profileImgView  applyRoundedCornersFullWithColor:[UIColor clearColor]];
        
        [cellZero.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.25;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        transition.delegate = self;
        [cellZero.layer addAnimation:transition forKey:nil];
        
        cellZero.hidden=NO;
        
        /*
        CATransition *transition = [CATransition animation];
        
        transition.duration = 0.2;
        
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        
        transition.type = kCATransitionFade;
        
        transition.delegate = self;
        
        [tableForPlaces.layer addAnimation:transition forKey:nil];
        
        
        
        tableForPlaces.hidden=NO;*/
        
        
        cellZero.profileImgView.alpha = 0;
        [UIView beginAnimations:@"fadeIn" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3];
        
        cellZero.profileImgView.alpha = 1;
        [UIView commitAnimations];
        
        
        
        

        
        [cellZero.ratingView setUserInteractionEnabled:NO];
        
        RBRatings rate=([[dictOwner valueForKey:@"rating"]floatValue]*2);
        [cellZero.ratingView setRatings:rate];
        
        return cellZero;
    }
    else
    {
        cellZero.profileImgView.alpha = 0;
        [UIView beginAnimations:@"fadeIn" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3];
        
        cellZero.profileImgView.alpha = 1;
        [UIView commitAnimations];
        
        //Ride Next Properties
//        cellOne.layer.cornerRadius = 10;
//        cellOne.layer.shadowRadius = 5.0;
//        cellOne.layer.shadowOpacity = 0.4;
//        cellOne.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        cellOne.layer.borderWidth = 0.5;
//        cellOne.layer.opacity = 0.6;
//        cellOne.layer.backgroundColor = (__bridge CGColorRef)([UIColor colorWithRed:1 green:1 blue:1 alpha:1]);
//        cellOne.clipsToBounds = YES;
        
        UIView *viewForCellOne = [cellOne viewWithTag:904];
        viewForCellOne.layer.cornerRadius = 10;
        viewForCellOne.clipsToBounds = YES;
        
        //Adjust horizontal and vertical lines size
        UILabel *horizontalLine = [cellOne viewWithTag:908];
        UILabel *verticalLine1 = [cellOne viewWithTag:909];
        UILabel *verticalLine2 = [cellOne viewWithTag:910];
        
        horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
        
        verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
        
        verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);

        if ([[dictRequest objectForKey:@"payment_mode"]boolValue]) {
            // Cash Image
            [cellOne.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
        }
        else{
            [cellOne.paymentImgView setImage:[UIImage imageNamed:@"icon_card-01"]];
        }
        
        [cellOne.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
        [cellOne.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];
        
        [cellOne.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:@"total"]]];
        [cellOne.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
        
        NSDate *date = [[UtilityClass sharedObject] stringToDate:[dictRequest valueForKey:@"future_request_date"] withFormate:@"yyyy-MM-dd"];


        [cellOne.lblDepartureDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
        
        [cellOne.lblDepartureTime setText:[dictRequest valueForKey:@"future_request_time"]];
        
        [cellOne.lblName setText:[dictOwner valueForKey:@"name"]];
        [cellOne.lblNumberOfRate setText:[NSString stringWithFormat:@"(%@)",[dictOwner valueForKey:@"num_rating"]]];
        
        [cellOne.profileImgView  applyRoundedCornersFullWithColor:[UIColor clearColor]];
        
        
        [cellOne.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
        
        [cellOne.profileImgView  applyRoundedCornersFullWithColor:[UIColor clearColor]];
        
        [cellOne.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.25;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionFade;
        transition.delegate = self;
        [cellOne.layer addAnimation:transition forKey:nil];
        
        cellOne.hidden=NO;
        
        /*
         CATransition *transition = [CATransition animation];
         
         transition.duration = 0.2;
         
         transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
         
         transition.type = kCATransitionFade;
         
         transition.delegate = self;
         
         [tableForPlaces.layer addAnimation:transition forKey:nil];
         
         
         
         tableForPlaces.hidden=NO;*/
        
        
        cellOne.profileImgView.alpha = 0;
        [UIView beginAnimations:@"fadeIn" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationDuration:0.3];
        
        cellOne.profileImgView.alpha = 1;
        [UIView commitAnimations];

        
        [cellOne.ratingView setUserInteractionEnabled:NO];
        
        RBRatings ratings = (float)([[dictOwner objectForKey:@"rating"] floatValue]*2);
        [cellOne.ratingView setRatings:ratings];
        
        return cellOne;

    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 160.0f;
    return 105.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [UIView new];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

#pragma mark
#pragma mark - Tableview Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dictRequest;
//    if ([segmentToggle selectedSegmentIndex] == 0) {
//        
//        dictRequest = [nearestRequest objectAtIndex:indexPath.row];
//    }
//    else if ([segmentToggle selectedSegmentIndex] == 1){
//        
//        dictRequest = [earliestRequest objectAtIndex:indexPath.row];
//    }
//    else{
//        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
//    }
    
    if ([self.customSegmentedControl selectedSegmentIndex] == 0) {
        
        dictRequest = [nearestRequest objectAtIndex:indexPath.row];
    }
    else if ([self.customSegmentedControl selectedSegmentIndex] == 1){
        
        dictRequest = [earliestRequest objectAtIndex:indexPath.row];
    }
    else{
        dictRequest = [confirmRequests objectAtIndex:indexPath.row];
    }
	
	[self performSegueWithIdentifier:SEGUE_TO_ADVANCE_PICKUP sender:dictRequest];
}

#pragma mark
#pragma mark Segue Method
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:SEGUE_TO_ADVANCE_PICKUP])
    {
        AdvancePickupVC *advance = [segue destinationViewController];
        advance.dictRequest = sender;
    }
}

- (void)segmentSelected
{
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        nearestRequest = [arrTempNearest mutableCopy];
        if ([nearestRequest count] > 0)
        {
            [tblRequestList reloadData];
            [tblRequestList setHidden:NO];
            [noItemsImgView setHidden:YES];
            if(NearestPage>=totalPagesNearest)
            {
                [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
            }
            else
            {
                [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
            }
        }
        else
        {
            [tblRequestList setHidden:YES];
            [noItemsImgView setHidden:NO];
        }
    }
    else
    {
        if(arrTempEarliest.count==0)
        {
            if(EarliestPage==1)
            {
                is_first=YES;
                [self getRequests:@"earliest" page:EarliestPage];
            }
        }
        else
        {
            earliestRequest = [arrTempEarliest mutableCopy];
        }
        if ([earliestRequest count] > 0)
        {
            [tblRequestList reloadData];
            [tblRequestList setHidden:NO];
            [noItemsImgView setHidden:YES];
            if(EarliestPage>=totalPagesNearest)
            {
                [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
            }
            else
            {
                [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
            }
        }
        else
        {
            [tblRequestList setHidden:YES];
            [noItemsImgView setHidden:NO];
        }
    }
    [pullToRefreshManager_ relocatePullToRefreshView];
}

- (IBAction)onClickConfirmRides:(id)sender {
    
    [self performSegueWithIdentifier:SEGUE_TO_CONFIRMED_RIDES sender:nil];
}

#pragma mark -
#pragma mark - ReloadTable to bottom
- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager
{
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
}

- (void)loadTable
{
    if (self.customSegmentedControl.selectedSegmentIndex == 0)
    {
        NearestPage++;
        page=NearestPage;
        if(page<=totalPagesNearest)
        {
            [self getRequests:@"nearest" page:page];
        }
    }
    else
    {
        EarliestPage++;
        page=EarliestPage;
        if(page<=totalPagesEarliest)
        {
            [self getRequests:@"earliest" page:page];
        }
    }
    [pullToRefreshManager_ tableViewReloadFinished];
    
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [pullToRefreshManager_ relocatePullToRefreshView];
}

-(void)getRequests:(NSString*)type page:(NSInteger)numberOfPage
{
    NSLog(@"TYPE : %@", type);
    
    if ([APPDELEGATE connected])
    {
        if(is_first==YES)
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
            is_first=NO;
        }
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:@"1" forKey:type];
        [dictParam setObject:[NSString stringWithFormat:@"%ld",(long)numberOfPage] forKey:PARAM_PAGE];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_ALL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error) {
            
            [APPDELEGATE hideLoadingView];
            
            if (response)
            {
                if ([[response objectForKey:@"success"] boolValue])
                {
                    if([response objectForKey:@"currency_sign"]) {
                        currencySign = [response objectForKey:@"currency_sign"];
                        NSLog(@"Sign : %@", currencySign);
                    } else {
                        currencySign = @"$";
                    }
                    
                    if([response objectForKey:@"current_country"]) {
                        currentCountry = [response objectForKey:@"current_country"];
                        NSLog(@"CC : %@", currentCountry);
                    } else {
                        currentCountry = @"Singapore";
                    }
                    
                    NSUserDefaults *pref = [[NSUserDefaults alloc]init];
                    [pref setObject:currencySign forKey:@"RN_CURRENCY_SIGN"];
                    [pref setObject:currentCountry forKey:@"RN_CURRENT_COUNTRY"];
                    [pref synchronize];
                    
                    if ([type isEqualToString:@"nearest"])
                    {
                        [arrTempNearest addObjectsFromArray:[response valueForKey:@"request"]];
                        nearestRequest=[arrTempNearest mutableCopy];
                        totalPagesNearest=[[response valueForKey:@"page_counts"]integerValue];
                        if(numberOfPage==totalPagesNearest)
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
                        }
                        else
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }
                    }
                    else
                    {
                        [arrTempEarliest addObjectsFromArray:[response valueForKey:@"request"]];
                        earliestRequest = [arrTempEarliest mutableCopy];
                        totalPagesEarliest=[[response valueForKey:@"page_counts"]integerValue];
                        if(numberOfPage==totalPagesEarliest)
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:NO];
                        }
                        else
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }
                    }
                    
                    if ([type isEqualToString:@"nearest"] && [nearestRequest count] > 0)
                    {
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                    }
                    else if ([type isEqualToString:@"earliest"] && [earliestRequest count] >0){
                        
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                        
                    }
                    else
                    {
                        [tblRequestList setHidden:YES];
                        [noItemsImgView setHidden:NO];
                    }
                    [pullToRefreshManager_ relocatePullToRefreshView];
                    
                    
                    
                    
                }
                else{
                    
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                    [tblRequestList setHidden:YES];
                    [noItemsImgView setHidden:NO];
                }
                
                
            }
            else
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
            }
            [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [pullToRefreshManager_ tableViewScrolled];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}
- (IBAction)menuBtn:(id)sender {
    
    //[_menuOn setHidden:NO];
}
- (IBAction)menuOn:(id)sender {
    
    //[_menuOn setHidden:YES];
}

@end
