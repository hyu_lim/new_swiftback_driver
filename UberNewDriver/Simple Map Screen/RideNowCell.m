//
//  RideNowCell.m
//  SwiftBack Driver
//
//  Created by Elluminati on 13/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "RideNowCell.h"

@implementation RideNowCell

- (void)awakeFromNib {
    
    [self.ratingView initRateBar];
    [self setLocalizedStrings];
    // Initialization code
}

- (void)setLocalizedStrings {
    [self.lblDepartureTimeTitle setText:NSLocalizedString(@"DEPARTURE_TIME", nil)];
    [self.lblDistanceTitle setText:NSLocalizedString(@"DISTANCE", nil)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
