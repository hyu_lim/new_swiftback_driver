

#import <UIKit/UIKit.h>
#import "RatingBar.h"

@interface CustomCellRequest : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDist;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;

@property (weak, nonatomic) IBOutlet UIButton *btnAcceptRide;
@property (weak, nonatomic) IBOutlet UIButton *btnRejectRide;
@property (weak, nonatomic) IBOutlet UIButton *btnShowRoute;

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@end
