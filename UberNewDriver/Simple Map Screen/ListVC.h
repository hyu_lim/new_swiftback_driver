
#import "BaseVC.h"
#import "RatingBar.h"

@protocol dataParser <NSObject>

-(void)listLoaded:(NSDictionary *)dictData;

@end

@interface ListVC : BaseVC<UITableViewDelegate,UITableViewDataSource>
{
    BOOL internet;
}
@property (strong,nonatomic) id<dataParser> delegate;
@property (strong, nonatomic) NSTimer *timerGetRequests;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (weak, nonatomic) IBOutlet UITableView *tblRequests;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoItems;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (void)onClickAcceptRide:(UIButton*)sender;
- (void)onClickRejectRide:(UIButton*)sender;
- (IBAction)backButtonPressed:(id)sender;

@end
