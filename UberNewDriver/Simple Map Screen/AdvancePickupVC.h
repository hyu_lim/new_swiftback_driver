

#import "BaseVC.h"
#import  <GoogleMaps/GoogleMaps.h>
#import "SWRevealViewController.h"
#import "Constant.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import  <MapKit/MapKit.h>
#import "ASStarRatingView.h"
#import "RatingBar.h"

@interface AdvancePickupVC : BaseVC<GMSMapViewDelegate,UIAlertViewDelegate>
{
    GMSMapView *mapView_;
    NSArray *routes;
	NSDictionary *dictOwner;
    NSMutableArray *arrReviewList;
    NSDictionary *dictReview;
}

@property (strong,nonatomic) NSDictionary *dictRequest;

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNote_Label;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfPassengers;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentBy;
@property (weak, nonatomic) IBOutlet UILabel *lblBioTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblGenderTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMutualFriendsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfRate;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblPassengerCount;
@property (weak, nonatomic) IBOutlet UILabel *verticalLine3;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;
@property (weak, nonatomic) IBOutlet UILabel *lblGender;
@property (weak, nonatomic) IBOutlet UILabel *lblBio;
@property (weak, nonatomic) IBOutlet UILabel *lblOccupation;
@property (weak, nonatomic) IBOutlet UILabel *lblBtnsBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (strong, nonatomic) IBOutlet UILabel *lblClientName;
@property (strong, nonatomic) IBOutlet UILabel *lblClientComment;

@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *acceptBtn;
@property (weak, nonatomic) IBOutlet UIButton *rejectBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnOpenInMaps;
@property (weak, nonatomic) IBOutlet UIButton *menuBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnPlaceIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnDocIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnPersonIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnReviewIcon;
@property (strong, nonatomic) IBOutlet UIButton *btnLeft;
@property (strong, nonatomic) IBOutlet UIButton *btnRight;

@property (weak, nonatomic) IBOutlet UIView *viewForGoogleMap;
@property (weak, nonatomic) IBOutlet UIView *frontView;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *roundedRectView;
@property (weak, nonatomic) IBOutlet UIView *viewForPlace;
@property (weak, nonatomic) IBOutlet UIView *viewForDoc;
@property (weak, nonatomic) IBOutlet UIView *viewForPerson;
@property (weak, nonatomic) IBOutlet UIView *viewForReview;
@property (strong, nonatomic) IBOutlet UIView *baseView;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundRect;
@property (weak, nonatomic) IBOutlet UIImageView *UserImgView;
@property (weak, nonatomic) IBOutlet UIImageView *paymentImgView;
@property (strong, nonatomic) IBOutlet UIImageView *clientImgView;
@property (strong, nonatomic) IBOutlet UIImageView *placeholderImgView;
@property (weak, nonatomic) IBOutlet UIImageView *triangleImgView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIScrollView *horizontalScrollView;

@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (strong, nonatomic) IBOutlet RatingBar *clientRatingView;

- (IBAction)onClickBack:(id)sender;
- (IBAction)onClickAccept:(id)sender;
- (IBAction)onClickReject:(id)sender;
- (IBAction)toMaps:(id)sender;
- (IBAction)onClickChat:(id)sender;
- (IBAction)onClickPlaceIcon:(id)sender;
- (IBAction)onClickDocIcon:(id)sender;
- (IBAction)onClickPersonIcon:(id)sender;

- (void)slideUpWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;
- (void)slideDownWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer;
- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer;

@end
