
#import "BaseVC.h"
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
#import "CrumbPathView.h"
#import "CrumbPath.h"
#import "WAGLocation.h"
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>

@class PickMeUpMapVC,RatingBar;

@interface ArrivedMapVC : BaseVC <MKMapViewDelegate,MKAnnotation,CLLocationManagerDelegate,GMSMapViewDelegate>
{
    UIImageView* routeView;
	
	NSArray* routes;
	
	UIColor* lineColor;
    
    GMSMapView *mapView_;
	
    
    CLLocationCoordinate2D dest;
    CLLocationCoordinate2D CurrentCoordinate;
    CLLocationCoordinate2D PickupCoordinate;
	
	NSDictionary *dictOwner;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTo;
@property (weak, nonatomic) IBOutlet UILabel *lblFrom;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblDestAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblTripCompletedTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblUserRate;
@property (weak, nonatomic) IBOutlet UILabel *lblCallUser;

@property (weak, nonatomic) IBOutlet UITextField *txtFeedBack;

@property (weak, nonatomic) IBOutlet UIButton *btnMenu;
@property (weak, nonatomic) IBOutlet UIButton *OpenMap;
@property (weak, nonatomic) IBOutlet UIButton *btnWalker;
@property (weak, nonatomic) IBOutlet UIButton *btnWalk;
@property (weak, nonatomic) IBOutlet UIButton *btnJob;
@property (weak, nonatomic) IBOutlet UIButton *btnArrived;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (weak, nonatomic) IBOutlet UIButton *btnNav;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;
@property (weak, nonatomic) IBOutlet UIButton *btnDistance;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;

@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@property (weak, nonatomic) IBOutlet RatingBar *ratingViewFeedback;

@property (weak, nonatomic) IBOutlet UIView *feedbackView;
@property (weak, nonatomic) IBOutlet UIView *viewForNavigation;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@property (nonatomic, strong) NSTimer *timeForUpdateWalkLoc;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSTimer *timerForDistance;
@property (nonatomic, strong) NSTimer *timerForCancelRequest;

@property (nonatomic,strong) NSNumber *latitude;
@property (nonatomic,strong) NSNumber *longitude;

@property (nonatomic,strong) CrumbPath *crumbs;
@property (nonatomic,strong) CrumbPathView *crumbView;

@property (nonatomic,strong) PickMeUpMapVC *pickMeUp;

@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet GMSMapView *mapView_;
@property (nonatomic,strong) MKPolyline *polyline;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *blurView2;
@property (weak, nonatomic) IBOutlet UIView *blurView3;
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentMode;
@property (weak, nonatomic) IBOutlet UIView *whiteProfile;
@property (weak, nonatomic) IBOutlet UIView *whiteButton;
@property (weak, nonatomic) IBOutlet UILabel *lblLedger;
@property (weak, nonatomic) IBOutlet UILabel *lblProfileTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblBase;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIView *alertViewBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnCancellation1;
@property (weak, nonatomic) IBOutlet UIButton *btnCancellation2;
@property (weak, nonatomic) IBOutlet UIButton *btnCancellation3;
@property (weak, nonatomic) IBOutlet UIImageView *alertImg;
@property (weak, nonatomic) IBOutlet UIView *alertCancellationView;
@property (weak, nonatomic) IBOutlet UIView *alertRatingView;
@property (weak, nonatomic) IBOutlet RatingBar *alertRatingBar;
@property (weak, nonatomic) IBOutlet UITextField *alertReview;
@property (weak, nonatomic) IBOutlet UIView *greyOverlay;
@property (weak, nonatomic) IBOutlet UIView *alertCancellation2View;
@property (weak, nonatomic) IBOutlet UIView *alertCancellation3View;
@property (weak, nonatomic) IBOutlet UITextField *txtComment;
@property (weak, nonatomic) IBOutlet UIButton *toggleOn;
@property (weak, nonatomic) IBOutlet UIView *alertPopup;
@property (weak, nonatomic) IBOutlet UIView *feedbackView1;
@property (weak, nonatomic) IBOutlet UIView *feedbackView2;
@property (weak, nonatomic) IBOutlet UIView *feedbackView3;
@property (weak, nonatomic) IBOutlet UIView *feedbackViewContainer;

//Progress Bar
@property (weak, nonatomic) IBOutlet UILabel *lblSavingOfMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblGrossSavings;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthlyGoal;
@property (weak, nonatomic) IBOutlet UILabel *lblProgBarStart;
@property (weak, nonatomic) IBOutlet UILabel *lblMonthlyGoalTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSavingText;
@property (weak, nonatomic) IBOutlet UILabel *lblGrossSavingsText;
@property (weak, nonatomic) IBOutlet UILabel *lblSetMonthlyGoalTitle;
@property (weak, nonatomic) IBOutlet UITextField *txtMontlyGoal;
@property (weak, nonatomic) IBOutlet UIView *viewForEarnings;
@property (weak, nonatomic) IBOutlet UIView *viewForMonthlyGoal;
@property (weak, nonatomic) IBOutlet UIView *viewForGreenOverlay;
@property (weak, nonatomic) IBOutlet UIProgressView *pvSaving;

@property (weak, nonatomic) IBOutlet UIButton *btnSetMonthlyGoal;

- (IBAction)saveMonthlyGoal:(id)sender;
- (IBAction)setMonthlyGoal:(id)sender;
- (IBAction)cancelMonthlyGoal:(id)sender;

- (void)showCurrentSavingWithPV;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitGoal;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelGoal;


@property (weak, nonatomic) IBOutlet UIButton *btnShowProgress;
@property (weak, nonatomic) IBOutlet UIButton *btnShowInvoice;
@property (weak, nonatomic) IBOutlet UIView *invoiceContainerView;

@property (weak, nonatomic) IBOutlet UIView *profileView;
- (IBAction)onClickChat:(id)sender;
- (IBAction)onClickOpenInMaps:(id)sender;
- (IBAction)onClickArrived:(id)sender;
- (IBAction)onClickJobDone:(id)sender;
- (IBAction)onClickWalkStart:(id)sender;
- (IBAction)onClickWalkerStart:(id)sender;
- (IBAction)btnCall:(id)sender;
- (IBAction)onClickCancel:(id)sender;
- (IBAction)onClickSubmit:(id)sender;
- (IBAction)onClickFeedback:(id)sender;
- (IBAction)onClickNav:(id)sender;
- (IBAction)onClickCall:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *buttonBackground;
@property (weak, nonatomic) IBOutlet UIView *alertBackground;

@end
