//
//  RideNowCell.h
//  SwiftBack Driver
//
//  Created by Elluminati on 13/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingBar.h"

@interface RideNowCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblBackground;
@property (weak, nonatomic) IBOutlet UILabel *lblPassengerName;
@property (weak, nonatomic) IBOutlet UILabel *lblFromStation;
@property (weak, nonatomic) IBOutlet UILabel *lblToStation;
@property (weak, nonatomic) IBOutlet UILabel *lblDepartureTimeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistanceTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblCost;
@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfRate;
@property (weak, nonatomic) IBOutlet UIImageView *paymentImgView;
@property (weak, nonatomic) IBOutlet UIImageView *profileImgView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundCell;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;

@end
