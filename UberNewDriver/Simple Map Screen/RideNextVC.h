

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "NYSegmentedControl.h"
#import "MNMBottomPullToRefreshManager.h"

@interface RideNextVC : BaseVC <UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,MNMBottomPullToRefreshManagerClient>
{
    __weak IBOutlet UITableView *tblRequestList;
    __weak IBOutlet UIButton *menuBtn;
    __weak IBOutlet UISegmentedControl *segmentToggle;
    __weak IBOutlet UIImageView *noItemsImgView;
    __weak IBOutlet UIView *viewForNavigation;
    
    NSMutableArray *nearestRequest;
    NSMutableArray *earliestRequest;
    NSMutableArray *arrIndex;
    NSMutableArray *confirmRequests;
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
    NSInteger selectedCell;
    NSString *strType;
    NSDictionary *selectedRequest;
    NSTimer *counterTimer;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (weak, nonatomic) IBOutlet UIButton *confirmRidesBtn;
@property (strong, nonatomic) NYSegmentedControl *customSegmentedControl;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIView *mainView;

- (IBAction)onClickSegment:(id)sender;
- (IBAction)onClickConfirmRides:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *menuOn;

@end
