


#import "AdvancePickupVC.h"
#import "ChatVC.h"
#import "PickMeUpMapVC.h"

@interface AdvancePickupVC ()

@end

@implementation AdvancePickupVC {
    
    NSInteger numberOfViews;
    NSString *commentString;
    NSString *currencySign;
    NSString *currentCountry;
}

@synthesize dictRequest;



- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self customSetup];
    [self setData];
    [self layoutSetup];
    [self setLocalizedStrings];
    
    
    arrReviewList = [[NSMutableArray alloc] init];
    dictReview = [[NSDictionary alloc] init];
    
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
    
    //SwipeGestureRecognizer setup
    UISwipeGestureRecognizer *swipeUpBackgroundRect = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpWithGestureRecognizer:)];
    swipeUpBackgroundRect.direction = UISwipeGestureRecognizerDirectionUp;
    
    UISwipeGestureRecognizer *swipeDownBackgroundRect = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideDownWithGestureRecognizer:)];
    swipeDownBackgroundRect.direction = UISwipeGestureRecognizerDirectionDown;
    
    [self.frontView addGestureRecognizer:swipeUpBackgroundRect];
    [self.frontView addGestureRecognizer:swipeDownBackgroundRect];
    
    //UITapGestureRecognizer setup
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(slideUpAndDownWithTapGestureRecognizer:)];
    
    [self.frontView addGestureRecognizer:singleTapGestureRecognizer];
}



- (void)setLocalizedStrings {
    
    [self.lblNavTitle setText:NSLocalizedString(@"ADVANCE_RIDE", nil)];
    [self.lblDateTitle setText:NSLocalizedString(@"DATE", nil)];
    [self.lblTimeTitle setText:NSLocalizedString(@"TIME", nil)];
    [self.lblDistanceTitle setText:NSLocalizedString(@"DISTANCE", nil)];
    [self.lblNote_Label setText:NSLocalizedString(@"NOTES", nil)];
    [self.lblNoOfPassengers setText:NSLocalizedString(@"NO_OF_PASSENGERS", nil)];
    [self.lblPaymentBy setText:NSLocalizedString(@"PAYMENT_BY", nil)];
    [self.lblBioTitle setText:NSLocalizedString(@"BIO", nil)];
    [self.lblGenderTitle setText:NSLocalizedString(@"GENDER", nil)];
    [self.lblOccupationTitle setText:NSLocalizedString(@"OCCUPATION", nil)];
    [self.lblMutualFriendsTitle setText:NSLocalizedString(@"MUTUAL_FRIENDS", nil)];
    [self.chatBtn setTitle:NSLocalizedString(@"MESSAGE_USER", nil) forState:UIControlStateNormal];
    [self.acceptBtn setTitle:NSLocalizedString(@"ACCEPT_PICKUP", nil) forState:UIControlStateNormal];
    [self.rejectBtn setTitle:NSLocalizedString(@"CANCEL_PICKUP", nil) forState:UIControlStateNormal];
    [self.btnOpenInMaps setTitle:NSLocalizedString(@"OPEN_IN_MAP", nil) forState:UIControlStateNormal];
}

- (void)layoutSetup {
    
    [_UserImgView.layer setCornerRadius:_UserImgView.frame.size.width / 2];
    [_UserImgView applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    
    //frontView
    _frontView.layer.cornerRadius = 10;
    
    //roundedRectView
    _roundedRectView.layer.cornerRadius = 10;
    _roundedRectView.layer.shadowRadius = 5.0;
    _roundedRectView.layer.shadowOpacity = 0.4;
    
    //lblBtnsBackground
    _lblBtnsBackground.layer.cornerRadius = 10;
    _lblBtnsBackground.clipsToBounds = YES;
    
    //ChatBtm
    _chatBtn.layer.cornerRadius = 5;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

-(void)setData
{
    NSLog(@"DICTREQUEST : %@", dictRequest);
    
    if ([[self.dictRequest objectForKey:@"later"]integerValue] == 0) {
        
        [self.acceptBtn setHidden:NO];
        [self.rejectBtn setHidden:YES];
        //            [self.chatBtn setHidden:YES];
    }
    else{
        if ([[self.dictRequest objectForKey:@"status"] integerValue] == 0)
        {
            [self.acceptBtn setHidden:NO];
            [self.rejectBtn setHidden:YES];
            //            [self.chatBtn setHidden:YES];
        }
        else
        {
            [self.acceptBtn setHidden:YES];
            [self.rejectBtn setHidden:NO];
        }
        
    }
    
    NSUserDefaults *pref = [[NSUserDefaults alloc] init];
    [pref synchronize];
    currencySign = [pref valueForKey:@"RN_CURRENCY_SIGN"];
    currentCountry = [pref valueForKey:@"RN_CURRENT_COUNTRY"];
	
    [self.UserImgView applyRoundedCornersFull];
    [self.scrollView setContentSize:CGSizeMake(0, 350)];
    
    dictOwner = [self.dictRequest valueForKey:@"owner"];
    
    arrReviewList = [[NSMutableArray alloc] init];
    [arrReviewList addObject: [dictOwner valueForKey:@"rating_list"]];
    numberOfViews = [arrReviewList[0] count];
    [self prepareHorizontalScrollView];
    
    if([[dictOwner objectForKey:PARAM_BIO] isKindOfClass:[NSNull class]] || [[dictOwner objectForKey:PARAM_BIO] isEqualToString:@""] || ![dictOwner objectForKey:PARAM_BIO]) {
        [self.lblBio setText:@"-"];
    } else {
        [self.lblBio setText:[dictOwner objectForKey:PARAM_BIO]];
    }
    
    if([[dictOwner objectForKey:PARAM_OCCUPATION] isKindOfClass:[NSNull class]] || [[dictOwner objectForKey:PARAM_BIO] isEqualToString:@""] || ![dictOwner objectForKey:PARAM_OCCUPATION]) {
        [self.lblOccupation setText:@"-"];
    } else {
        [self.lblOccupation setText:[dictOwner objectForKey:PARAM_OCCUPATION]];
    }
    
    if([[dictOwner objectForKey:PARAM_GENDER] isKindOfClass:[NSNull class]] || [[dictOwner objectForKey:PARAM_GENDER] isEqualToString:@""] || ![dictOwner objectForKey:PARAM_GENDER]) {
        [self.lblGender setText:@"-"];
    } else {
        NSString *gender = [dictOwner objectForKey:PARAM_GENDER];
        [self.lblGender setText:[NSString stringWithFormat:@"%@", [gender capitalizedString]]];
    }
    
    if ([[self.dictRequest objectForKey:@"no_of_passenger"] isKindOfClass:[NSNull class]] || ![self.dictRequest objectForKey:@"no_of_passenger"]) {
        
        [self.lblPassengerCount setText:@"N/A"];
    }
    else{
        
        [self.lblPassengerCount setText:[NSString stringWithFormat:@"%@",[self.dictRequest objectForKey:@"no_of_passenger"]]];
    }
    
    if([[self.dictRequest objectForKey:@"note"] isKindOfClass:[NSNull class]] || [[self.dictRequest objectForKey:@"note"] isEqualToString:@""] || ![self.dictRequest objectForKey:@"note"]){
        
        [self.lblNote setText:@"-"];
        
        [self.verticalLine3 setHidden:YES];
//        [self.lblNote_Label setHidden:YES];
//        [self.lblNote sonClicetHidden:YES];
        
    } else {
        
        [self.lblNote setText:[self.dictRequest objectForKey:@"note"]];
    }
    
    if ([[self.dictRequest objectForKey:@"payment_mode"]boolValue]) {
        
        // Cash Image
        [self.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
        [self.lblPayment setText:@"Cash :"];
        
    }
    else{
        
        [self.paymentImgView setImage:[UIImage imageNamed:@"icon_card-01"]];
        [self.lblPayment setText:@"Card :"];
    }
    
    [self.ratingView initRateBar];
    [self.ratingView setUserInteractionEnabled:NO];
    RBRatings rating = (float)([[dictOwner valueForKey:@"rating"] floatValue] *2);
    [self.ratingView setRatings:rating];
    
    
    //    NSString *payment_mode = [NSString stringWithFormat:@"%@",[self.dictRequest objectForKey:@"payment_mode"]];
    
    [self.UserImgView downloadFromURL:[dictOwner objectForKey:PARAM_PICTURE] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
    
    [self.lblName setText:[dictOwner objectForKey:@"name"]];
    [self.lblNumberOfRate setText:[NSString stringWithFormat:@"(%@)",[dictOwner valueForKey:@"num_rating"]]];
    [self.lblFrom setText:[self.dictRequest objectForKey:@"s_address"]];
    [self.lblTo setText:[self.dictRequest objectForKey:@"d_address"]];

    
    [self.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[self.dictRequest valueForKey:@"total"]]];
    
    [self.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[self.dictRequest valueForKey:@"distance"] doubleValue]]];
    
    if ([[self.dictRequest objectForKey:@"future_request_date"] isKindOfClass:[NSNull class]] || ![self.dictRequest objectForKey:@"future_request_date"]) {
        
        [self.lblDate setText:@"NOW"];
    }
    else{
        
        NSDate *date = [[UtilityClass sharedObject] stringToDate:[self.dictRequest valueForKey:@"future_request_date"] withFormate:@"yyyy-MM-dd"];
        
        [self.lblDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
    }
    
    if ([[self.dictRequest objectForKey:@"future_request_time"] isKindOfClass:[NSNull class]] || ![self.dictRequest objectForKey:@"future_request_time"]) {
        
        [self.lblTime setText:@"NOW"];
    }
    else{
        
        [self.lblTime setText:[self.dictRequest valueForKey:@"future_request_time"]];
    }
    
    CLLocationCoordinate2D walker;
    walker.latitude=[[self.dictRequest objectForKey:@"walker_latitude"] doubleValue];
    walker.longitude=[[self.dictRequest objectForKey:@"walker_longitude"] doubleValue];
    
    CLLocationCoordinate2D source;
    source.latitude=[[self.dictRequest objectForKey:@"latitude"] doubleValue];
    source.longitude=[[self.dictRequest objectForKey:@"longitude"] doubleValue];
    
    CLLocationCoordinate2D destination;
    destination.latitude=[[self.dictRequest objectForKey:@"d_latitude"] doubleValue];
    destination.longitude=[[self.dictRequest objectForKey:@"d_longitude"] doubleValue];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:walker.latitude
                                                            longitude:walker.longitude
                                                                 zoom:15];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0,0,self.viewForGoogleMap.frame.size.width,self.viewForGoogleMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = YES;
    [self.viewForGoogleMap addSubview:mapView_];
    
    
    GMSMarker *driver = [[GMSMarker alloc]init];
    driver.position = walker;
    driver.icon = [UIImage imageNamed:@"pin_driver"];
    //    driver.icon = [self image:driver.icon scaledToSize:CGSizeMake(45.0f,45.0f)];
    driver.map = mapView_;
    
    GMSMarker *pickup = [[GMSMarker alloc]init];
    pickup.position = source;
    pickup.icon = [UIImage imageNamed:@"pin_client_org"];
    //    pickup.icon = [self image:pickup.icon scaledToSize:CGSizeMake(40.0f,40.0f)];
    pickup.map = mapView_;
    
    GMSMarker *to = [[GMSMarker alloc]init];
    to.position = destination;
    to.icon = [UIImage imageNamed:@"pin_client_destination"];
    //    to.icon = [self image:to.icon scaledToSize:CGSizeMake(45.0f,45.0f)];
    to.map = mapView_;
    
    UIColor *themeColor = [UIColor colorWithRed:51.0/255.0 green:204.0/255.0 blue:153.0/255.0 alpha:1.0];
    UIColor *blueColor = [UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:255.0/255.0 alpha:1.0];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSArray *decodeLine1 =  [self calculateRoutesFrom:walker t:source :themeColor];
        NSArray *decodeLine2 =  [self calculateRoutesFrom:source t:destination :blueColor];
        
        NSMutableArray *decodeLines = [[NSMutableArray alloc]init];
        [decodeLines addObjectsFromArray:decodeLine1];
        [decodeLines addObjectsFromArray:decodeLine2];
        
        [self centerMap:decodeLines];
        [self.viewForGoogleMap setUserInteractionEnabled:YES];
        [APPDELEGATE hideLoadingView];

    });
    
    //    For Polyline:
    //    (Driver current) - (Passenger "FROM") :  33cc99
    //    (Passenger "FROM") - (Passenger "TO") : 00CCFF
    
    //    [UIColor colorWithRed:51.0/255.0 green:204.0/255.0 blue:153.0/255.0 alpha:1.0];
    //        [UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:255.0/255.0 alpha:1.0];
    
}

- (void) prepareHorizontalScrollView {
    
    CGRect scrollViewFrame = CGRectMake(0, 25, self.viewForReview.frame.size.width, 100);
    self.horizontalScrollView.frame = scrollViewFrame;
    self.horizontalScrollView.pagingEnabled = YES;
    self.horizontalScrollView.bounces = NO;
    
    
    //Create left button and add to the subview
    CGRect leftBtnFrame = CGRectMake(10, 70, 20, 20);
    self.btnLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnLeft addTarget:self action:@selector(onClickLefBtn) forControlEvents:UIControlEventTouchUpInside];
    self.btnLeft.frame = leftBtnFrame;
    [self.btnLeft setImage:[UIImage imageNamed:@"icon_left_arrow_black"] forState:UIControlStateNormal];
    [self.viewForReview addSubview:self.btnLeft];
    
    
    
    //Create right button and add to the subview
    CGRect rightBtnFrame = CGRectMake((self.viewForReview.frame.size.width - 40), 70, 20, 20);
    self.btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.btnRight addTarget:self action:@selector(onClickRightBtn) forControlEvents:UIControlEventTouchUpInside];
    self.btnRight.frame = rightBtnFrame;
    [self.btnRight setImage:[UIImage imageNamed:@"icon_right_arrow_black"] forState:UIControlStateNormal];
    [self.viewForReview addSubview:self.btnRight];
    
    
    //Init RatingBar here (P.S. cannnot initialize it inside of loop, if we do, stars won't be filled)
    [self.clientRatingView initRateBar];
    
    if(numberOfViews == 0) {
        CGRect placeholderFrame = CGRectMake(25, 10, self.viewForReview.frame.size.width - 40, self.viewForReview.frame.size.height - 30);
        self.placeholderImgView = [[UIImageView alloc] initWithFrame:placeholderFrame];
        [self.placeholderImgView setImage:[UIImage imageNamed:@"icon_no_review.png"]];
        [self.viewForReview addSubview:self.placeholderImgView];
        [self.viewForReview bringSubviewToFront:self.viewForReview];
    } else {
        for(int i=0; i<numberOfViews; i++) {
            
            dictReview = arrReviewList[0][i];
//            NSLog(@"FirstName : %@",[dictReview objectForKey:@"first_name"]);
//            NSLog(@"LastName : %@",[dictReview objectForKey:@"last_name"]);
//            NSLog(@"ID : %@",[dictReview objectForKey:@"id"]);
//            NSLog(@"Rating : %@",[dictReview objectForKey:@"rating"]);
//            NSLog(@"Picture : %@",[dictReview objectForKey:@"picture"]);
//            NSLog(@"Comment : %@",[dictReview objectForKey:@"comment"]);
    
            //Set the origin of the sub view
            CGFloat myOrigin = i * self.viewForReview.frame.size.width;
            
            //Create the sub view and allocate memory
            self.baseView = [[UIView alloc] initWithFrame:CGRectMake(myOrigin, 0, self.horizontalScrollView.frame.size.width, self.horizontalScrollView.frame.size.height)];
            //Set the background to clear color
            self.baseView.backgroundColor = [UIColor clearColor];
            
            
            //Create a imageView and add to the subview
            CGRect imgViewFrame = CGRectMake(50.f, 20.f, 70.f, 70.f);
            self.clientImgView = [[UIImageView alloc] initWithFrame:imgViewFrame];
            [self.clientImgView downloadFromURL:[dictReview objectForKey:@"picture"]withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
            [self.clientImgView applyRoundedCornersFull];
            [self.baseView addSubview:self.clientImgView];
            
            
            //Create a label and add to the subview
            CGRect nameFrame = CGRectMake(130.0f, 15.0f, 200.0f, 21.0f);
            self.lblClientName = [[UILabel alloc]initWithFrame:nameFrame];
            self.lblClientName.text = [NSString stringWithFormat:@"%@", [[dictReview objectForKey:@"first_name"] capitalizedString]];
            [self.lblClientName setFont:[UIFont fontWithName:@"OpenSans-Light" size:13.f]];
            self.lblClientName.textColor = [UIColor darkGrayColor];
            [self.baseView addSubview:self.lblClientName];
            
            
            //Create RatingBar
            self.clientRatingView = [[RatingBar alloc] initWithSize:CGSizeMake(70.f, 13.f) AndPosition:CGPointMake(130.f, 37.f)];
            self.clientRatingView.backgroundColor = [UIColor clearColor];
            //        [self.clientRatingView initRateBar];
            [self.clientRatingView setUserInteractionEnabled:NO];
            RBRatings rating = (float)([[dictReview objectForKey:@"rating"] floatValue] *2);
            //        NSLog(@"Rating ==> %f", (float)([[dictReview objectForKey:@"rating"] floatValue] *2));
            [self.clientRatingView setRatings:rating];
            [self.baseView addSubview:self.clientRatingView];
            
            
            //Create a label and add to the subview
            CGRect commentFrame = CGRectMake(130.0f, 53.0f, 150.0f, 40.0f);
            self.lblClientComment = [[UILabel alloc]initWithFrame:commentFrame];
            
            commentString = [dictReview objectForKey:@"comment"];
            if([commentString isEqualToString:@""] || [commentString isKindOfClass:[NSNull class]] || !commentString){
                commentString = NSLocalizedString(@"NO_REVIEW", nil);
            }
            self.lblClientComment.text = [NSString stringWithFormat:@"%@", commentString];
            [self.lblClientComment setFont:[UIFont fontWithName:@"OpenSans-Light" size:11.f]];
            [self.lblClientComment setNumberOfLines:0];
            [self.lblClientComment sizeToFit];
            [self.lblClientComment setLineBreakMode:NSLineBreakByWordWrapping];
            self.lblClientComment.textColor = [UIColor darkGrayColor];
            [self.baseView addSubview:self.lblClientComment];
            
            
            //Set the scroll view
            self.horizontalScrollView.delegate = self;
            [self.horizontalScrollView addSubview:self.baseView];
            self.horizontalScrollView.contentSize = CGSizeMake(self.viewForReview.frame.size.width * numberOfViews, self.horizontalScrollView.frame.size.height);
            [self.viewForReview addSubview:self.horizontalScrollView];
        }
    }
    
    if((numberOfViews == 0) || (numberOfViews == 1)) {
        [self.btnLeft setHidden:YES];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:YES];
    }
    
    [self.viewForReview bringSubviewToFront:self.btnLeft];
    [self.viewForReview bringSubviewToFront:self.btnRight];
}

- (void)onClickLefBtn {
    CGFloat pageWidth = self.horizontalScrollView.frame.size.width;
    CGFloat offset = self.horizontalScrollView.contentOffset.x - pageWidth;
    [self.horizontalScrollView setContentOffset:CGPointMake(offset, 0)];
    
    if(offset == 0.0) {
        [self.btnLeft setHidden:YES];
        [self.btnRight setHidden:NO];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.horizontalScrollView.layer addAnimation:transition forKey:nil];
}

- (void)onClickRightBtn {
    CGFloat pageWidth = self.horizontalScrollView.frame.size.width;
    CGFloat offset = self.horizontalScrollView.contentOffset.x + pageWidth;
    CGFloat mainViewFrameSize = self.view.frame.size.width * (numberOfViews-1);
    [self.horizontalScrollView setContentOffset:CGPointMake(offset, 0)];
    
    if(offset == mainViewFrameSize) {
        [self.btnRight setHidden:YES];
        [self.btnLeft setHidden:NO];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.horizontalScrollView.layer addAnimation:transition forKey:nil];
}

// scrolling ends
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth/2)/pageWidth) + 1;
    
    if(page == 0) {
        [self.btnRight setHidden:NO];
        [self.btnLeft setHidden:YES];
    } else if(page == numberOfViews-1) {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
}

// dragging ends
- (void) scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGFloat pageWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth/2)/pageWidth) + 1;
    
    if(page == 0) {
        [self.btnRight setHidden:NO];
        [self.btnLeft setHidden:YES];
    } else if(page == numberOfViews-1) {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:YES];
    } else {
        [self.btnLeft setHidden:NO];
        [self.btnRight setHidden:NO];
    }
}

//- (UIImage *)image:(UIImage*)originalImage scaledToSize:(CGSize)size
//{
//    //avoid redundant drawing
//    if (CGSizeEqualToSize(originalImage.size, size))
//    {
//        return originalImage;
//    }
//    
//    //create drawing context
//    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0f);
//    
//    //draw
//    [originalImage drawInRect:CGRectMake(0.0f, 0.0f, size.width, size.height)];
//    
//    //capture resultant image
//    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    //return image
//    return image;
//}

#pragma mark -
#pragma mark - Draw Route Methods

- (NSMutableArray *)decodePolyLine: (NSMutableString *)encoded
{
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\" options:NSLiteralSearch range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len)
    {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do
        {
            if(index<len)
            {
                b = [encoded characterAtIndex:index++] - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            }
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        //printf("[%f,", [latitude doubleValue]);
        //printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
    }
    return array;
}

-(NSArray*) calculateRoutesFrom:(CLLocationCoordinate2D) f   t:(CLLocationCoordinate2D) t :(UIColor*)color
{
    //    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    //    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    //
    //    NSString* apiUrlStr = [NSString stringWithFormat:@"http://maps.google.com/maps?output=dragdir&saddr=%@&daddr=%@", saddr, daddr];
    //    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    //    //NSLog(@"api url: %@", apiUrl);
    //    NSError* error = nil;
    //    NSString *apiResponse = [NSString stringWithContentsOfURL:apiUrl encoding:NSASCIIStringEncoding error:&error];
    //    NSString *encodedPoints = [apiResponse stringByMatching:@"points:\\\"([^\\\"]*)\\\"" capture:1L];
    //    return [self decodePolyLine:[encodedPoints mutableCopy]];
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    // NSString* via = [NSString stringWithFormat:@"%f,%f",wayPoint.latitude,wayPoint.longitude];

    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,Google_Browser_key];
    //chk
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] ||  [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
    }
    else
    {
        GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
        GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 5.0f;
        singleLine.strokeColor = color;
        singleLine.map = mapView_;
        
        routes = json[@"routes"];
        
        
    }
    
    NSString *points=[[[routes objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
    return [self decodePolyLine:[points mutableCopy]];
}

-(void)centerMap:(NSArray*)locations
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (CLLocation *loc in locations)
    {
        location.latitude = loc.coordinate.latitude;
        location.longitude = loc.coordinate.longitude;
        // Creates a marker in the center of the map.
        bounds = [bounds includingCoordinate:location];
    }
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:10.0f]];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    ChatVC *chatVC = [segue destinationViewController];
	chatVC.dictOwner = dictOwner;
	
}

- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClickAccept:(id)sender {
    
    if([APPDELEGATE  connected])
    {
        [APPDELEGATE showLoadingWithTitle:@"Loading..."];
        
        
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        
        [dictparam setObject:[self.dictRequest objectForKey:@"request_id"] forKey:PARAM_REQUEST_ID];
        [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictparam setObject:@"1" forKey:PARAM_ACCEPTED];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_RESPOND_REQUEST withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             
             NSLog(@"Respond to Request= %@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
					 if ([[self.dictRequest objectForKey:@"later"]integerValue] == 0) {
						 
						 PickMeUpMapVC *vcFeed = nil;
						 for (int i=0; i<self.navigationController.viewControllers.count; i++)
						 {
							 UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
							 if ([vc isKindOfClass:[PickMeUpMapVC class]])
							 {
								 vcFeed = (PickMeUpMapVC *)vc;
								 
								 break;
							 }
							 
						 }
						 [self.navigationController popToViewController:vcFeed animated:NO];
					 }
					 else{
						 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"RIDE_REQUEST_CONFIRMED", nil) message:NSLocalizedString(@"YOU_WILL_BE_REMINDED_OF_YOUR_PICKUP", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
						 [alert show];
					 }
					 
					 
                 }
                 else{
                     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                     [alert show];
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)onClickReject:(id)sender {
    
    UIAlertController *cancelAlert = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"CANCEL_REQUEST", nil) message:NSLocalizedString(@"ARE_U_SURE_TO_CANCEL_REQUEST", nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
        if([APPDELEGATE  connected])
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
            NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
            
            [dictparam setObject:[self.dictRequest objectForKey:@"request_id"] forKey:PARAM_REQUEST_ID];
            [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_FUTURE_WALK withParamData:dictparam withBlock:^(id response, NSError *error)
             {
                 
                 NSLog(@"Cancel Request= %@",response);
                 [APPDELEGATE hideLoadingView];
                 if (response)
                 {
                     if([[response valueForKey:@"success"] intValue]==1)
                     {
                         
                         [self.navigationController popViewControllerAnimated:YES];
                     }
                     else{
                         UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ERROR", nil) message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                         [alert show];
                     }
                 }
                 
             }];
        }
        else
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
            [alert show];
        }
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"CANCEL", nil) style:UIAlertActionStyleCancel handler:nil];
    
    [cancelAlert  addAction:okAction];
    [cancelAlert addAction:cancelAction];
    
    [self presentViewController:cancelAlert animated:YES completion:nil];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:NSLocalizedString(@"OK", nil)])
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)toMaps:(id)sender {
    NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=%@,%@&daddr=%@,%@", [dictRequest valueForKey:@"latitude"], [dictRequest valueForKey:@"longitude"], [dictRequest valueForKey:@"d_latitude"], [dictRequest valueForKey:@"d_longitude"]];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:googleMapsURLString]];
}

#pragma mark - SwipeGestureRecognizer methods

- (void)slideUpWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer {
    
    [UIView animateWithDuration:0.7 animations:^{
        self.frontView.frame = CGRectMake(0, 175, 320, self.frontView.frame.size.height);
        
        self.blurView.frame = CGRectMake(0, 175, 320, self.blurView.frame.size.height);
    }];
}

- (void)slideDownWithGestureRecognizer:(UISwipeGestureRecognizer *)gestureRecognizer {
    
    [UIView animateWithDuration:0.7 animations:^{
        self.frontView.frame =CGRectMake(0, 480, 320, self.frontView.frame.size.height);
        
        self.blurView.frame =CGRectMake(0, 480, 320, self.blurView.frame.size.height);
    }];
}

- (void)slideUpAndDownWithTapGestureRecognizer:(UITapGestureRecognizer *)tapGestureRecognizer {
    
    CGFloat newYCoordinate = 175.0;
    
    if(self.frontView.frame.origin.y == 175.0) {
        newYCoordinate = 480.0;
    }
    
    [UIView animateWithDuration:0.7 animations:^{
        self.frontView.frame = CGRectMake(self.frontView.frame.origin.x, newYCoordinate, self.frontView.frame.size.width, self.frontView.frame.size.height);
        
        self.blurView.frame = CGRectMake(self.blurView.frame.origin.x, newYCoordinate, self.blurView.frame.size.width, self.blurView.frame.size.height);
    }];
}

- (IBAction)onClickChat:(id)sender
{
    [self performSegueWithIdentifier:SEGUE_TO_DIRECT_CHAT sender:self];
}

- (IBAction)onClickPlaceIcon:(id)sender {
    [self.viewForPlace setHidden:NO];
    [self.viewForDoc setHidden:YES];
    [self.viewForPerson setHidden:YES];
    [self.viewForReview setHidden:YES];
    
    [self.btnPlaceIcon setImage:[UIImage imageNamed:@"icon_pin_green.png"] forState:UIControlStateNormal];
    [self.btnDocIcon setImage:[UIImage imageNamed:@"icon_doc_gray.png"] forState:UIControlStateNormal];
    [self.btnPersonIcon setImage:[UIImage imageNamed:@"icon_person_gray.png"] forState:UIControlStateNormal];
    [self.btnReviewIcon setImage:[UIImage imageNamed:@"icon_review_gray.png"] forState:UIControlStateNormal];
}

- (IBAction)onClickDocIcon:(id)sender {
    [self.viewForPlace setHidden:YES];
    [self.viewForDoc setHidden:NO];
    [self.viewForPerson setHidden:YES];
    [self.viewForReview setHidden:YES];
    
    [self.btnPlaceIcon setImage:[UIImage imageNamed:@"icon_pin_gray.png"] forState:UIControlStateNormal];
    [self.btnDocIcon setImage:[UIImage imageNamed:@"icon_doc_green.png"] forState:UIControlStateNormal];
    [self.btnPersonIcon setImage:[UIImage imageNamed:@"icon_person_gray.png"] forState:UIControlStateNormal];
    [self.btnReviewIcon setImage:[UIImage imageNamed:@"icon_review_gray.png"] forState:UIControlStateNormal];
}

- (IBAction)onClickPersonIcon:(id)sender {
    [self.viewForPlace setHidden:YES];
    [self.viewForDoc setHidden:YES];
    [self.viewForPerson setHidden:NO];
    [self.viewForReview setHidden:YES];
    
    [self.btnPlaceIcon setImage:[UIImage imageNamed:@"icon_pin_gray.png"] forState:UIControlStateNormal];
    [self.btnDocIcon setImage:[UIImage imageNamed:@"icon_doc_gray.png"] forState:UIControlStateNormal];
    [self.btnPersonIcon setImage:[UIImage imageNamed:@"icon_person_green.png"] forState:UIControlStateNormal];
    [self.btnReviewIcon setImage:[UIImage imageNamed:@"icon_review_gray.png"] forState:UIControlStateNormal];
}

- (IBAction)onClickReviewIcon:(id)sender {
    [self.viewForPlace setHidden:YES];
    [self.viewForDoc setHidden:YES];
    [self.viewForPerson setHidden:YES];
    [self.viewForReview setHidden:NO];
    
    [self.btnPlaceIcon setImage:[UIImage imageNamed:@"icon_pin_gray.png"] forState:UIControlStateNormal];
    [self.btnDocIcon setImage:[UIImage imageNamed:@"icon_doc_gray.png"] forState:UIControlStateNormal];

    [self.btnPersonIcon setImage:[UIImage imageNamed:@"icon_person_gray.png"] forState:UIControlStateNormal];
    [self.btnReviewIcon setImage:[UIImage imageNamed:@"icon_review_green.png"] forState:UIControlStateNormal];
}

@end
