//
//  ConfirmRidesVC.m
//  SwiftBack Driver
//
//  Created by Elluminati on 15/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "ConfirmRidesVC.h"
#import "RideNextCell.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "AdvancePickupVC.h"

@interface ConfirmRidesVC () {
    NSString *currencySign;
    NSString *currentCountry;
}

@end

@implementation ConfirmRidesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    confirmRides = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
	[self getConfirmedRequests];

}

#pragma mark
#pragma mark Get Confrimed Requests


-(void)getConfirmedRequests
{
    if([APPDELEGATE  connected])
    {
        
        NSMutableDictionary *dictparam=[[NSMutableDictionary alloc]init];
        [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictparam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        //[dictparam setObject:strLatitude forKey:PARAM_LATITUDE];
        // [dictparam setObject:strLongitude forKey:PARAM_LONGITUDE];
        
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING...", nil)];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_GET_CONFIRMED_REQUESTS withParamData:dictparam withBlock:^(id response, NSError *error)
         {
             
             NSLog(@"Confirm Requests%@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     
                     if([response objectForKey:@"currency_sign"]) {
                         currencySign = [response objectForKey:@"currency_sign"];
                         NSLog(@"Sign : %@", currencySign);
                     } else {
                         currencySign = @"$";
                     }
                     
                     if([response objectForKey:@"current_country"]) {
                         currentCountry = [response objectForKey:@"current_country"];
                         NSLog(@"CC : %@", currentCountry);
                     } else {
                         currentCountry = @"Singapore";
                     }
                     
                     NSMutableArray *arrRides = [[response valueForKey:@"future_request"] mutableCopy];
                     
                     if ([arrRides count]>0)
                     {
                         [self makeSection:arrRides];
                         [tableForConfirmRides reloadData];
                         [tableForConfirmRides setHidden:NO];
                         [noItemsImgView setHidden:YES];
                         
                     }
                     else
                     {
                         [tableForConfirmRides setHidden:YES];
                         [noItemsImgView setHidden:NO];
                         
                     }
                 }
                 else{
                     if (![[response valueForKey:@"error"] isEqualToString:@""])
                     {
                         [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                         [tableForConfirmRides setHidden:YES];
                         [noItemsImgView setHidden:NO];
                     }
                     
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"NO_INTERNET_TITLE", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
}



#pragma mark
#pragma mark - TableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return [confirmRides count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[confirmRides objectAtIndex:section] count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    RideNextCell *cellOne = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSDictionary *dictRequest = [[confirmRides objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    

    NSDictionary *dictOwner = [dictRequest  valueForKey:@"owner"];
    
        UIView *viewForCellOne = [cellOne viewWithTag:904];
        viewForCellOne.layer.cornerRadius = 10;
        viewForCellOne.layer.borderColor = [UIColor lightGrayColor].CGColor;;
        viewForCellOne.layer.borderWidth = 0.5;
        viewForCellOne.clipsToBounds = YES;
        
        //Adjust horizontal and vertical lines size
        UILabel *horizontalLine = [cellOne viewWithTag:908];
        UILabel *verticalLine1 = [cellOne viewWithTag:909];
        UILabel *verticalLine2 = [cellOne viewWithTag:910];
        
        horizontalLine.frame = CGRectMake(horizontalLine.frame.origin.x, horizontalLine.frame.origin.y, horizontalLine.frame.size.width, 0.5);
        
        verticalLine1.frame = CGRectMake(verticalLine1.frame.origin.x, verticalLine1.frame.origin.y, 0.5, verticalLine1.frame.size.height);
        
        verticalLine2.frame = CGRectMake(verticalLine2.frame.origin.x, verticalLine2.frame.origin.y, 0.5, verticalLine2.frame.size.height);
        
        if ([[dictRequest objectForKey:@"payment_mode"]boolValue]) {
            // Cash Image
            [cellOne.paymentImgView setImage:[UIImage imageNamed:@"icon_cash-01"]];
        }
        else{
            [cellOne.paymentImgView setImage:[UIImage imageNamed:@"icon_card-01"]];
        }
        
        [cellOne.lblFromStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"s_address"]]];
        [cellOne.lblToStation setText:[NSString stringWithFormat:@"%@",[dictRequest valueForKey:@"d_address"]]];

    
        [cellOne.lblCost setText:[NSString stringWithFormat:@"%@%@",currencySign,[dictRequest valueForKey:@"total"]]];
        [cellOne.lblDistance setText:[NSString stringWithFormat:@"%.2fKm",[[dictRequest valueForKey:@"distance"] doubleValue]]];
        
        NSDate *date = [[UtilityClass sharedObject] stringToDate:[dictRequest valueForKey:@"future_request_date"] withFormate:@"yyyy-MM-dd"];
        
        
        [cellOne.lblDepartureDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMM"]];
        
        [cellOne.lblDepartureTime setText:[dictRequest valueForKey:@"future_request_time"]];
        
        [cellOne.lblName setText:[dictOwner valueForKey:@"name"]];
        
        [cellOne.profileImgView  applyRoundedCornersFullWithColor:[UIColor clearColor]];
        
        
        [cellOne.profileImgView downloadFromURL:[dictOwner objectForKey:@"picture"] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
        
        [cellOne.ratingView setUserInteractionEnabled:NO];
        
        RBRatings ratings = (float)([[dictOwner objectForKey:@"rating"] floatValue]*2);
        [cellOne.ratingView setRatings:ratings];
        
        return cellOne;
        
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *dictRequest = [[confirmRides objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:SEGUE_TO_ADVANCE_PICKUP sender:dictRequest];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105.0f;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(20, 0, 320, 40)];
    
    UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(10, 10, 300, 20)];
    NSString *strDate = [arrForDate objectAtIndex:section];
	
	NSDate *date = [[UtilityClass sharedObject] stringToDate:strDate withFormate:@"yyyy-MM-dd"];
	
	
	[lblDate setText:[[UtilityClass sharedObject] DateToString:date withFormate:@"dd MMMM yyyy"]];
    lblDate.font = [UberStyleGuide fontRegular];
    [lblDate setTextColor:[UIColor darkGrayColor]];
//    [lblDate setTextColor:[UIColor colorWithRed:59.0/255.0 green:177.0/255.0 blue:156.0/255.0 alpha:1.0]];
    
    [v addSubview:lblDate];
    [v setBackgroundColor:[UIColor clearColor]];
    return v;
}

#pragma mark-
#pragma mark- Table View Delegate

-(void)makeSection:(NSMutableArray*)arrRides
{
    arrForDate=[[NSMutableArray alloc]init];
    confirmRides=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrRides];
//    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"future_request_date" ascending:YES
//                                                                              selector:@selector(localizedStandardCompare:)];
//    
//    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
	
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[arrtemp objectAtIndex:i];
        
        NSString *temp=[dictDate valueForKey:@"future_request_date"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
        
    }
    
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [confirmRides addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"future_request_date"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[confirmRides objectAtIndex:j] addObject:dictSection];
                
            }
        }
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
	AdvancePickupVC *pickup = [segue destinationViewController];
	pickup.dictRequest = sender;
	
	
}


- (IBAction)onClickBack:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
