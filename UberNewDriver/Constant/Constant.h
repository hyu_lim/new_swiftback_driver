
//// color



#define GOOGLE_KEY @"AIzaSyA0iwWpcCS13NTIDqsCR6MeHqT7z0nRQQM"
#define Address_URL @"https://maps.googleapis.com/maps/api/geocode/json?"
#define AutoComplete_URL @"https://maps.googleapis.com/maps/api/place/autocomplete/json?"
#define Place_URL @"https://maps.googleapis.com/maps/api/place/details/json?"


#define _RGB(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define kCLR_RANDOM_COLOR _RGB(arc4random()%255, arc4random()%255, arc4random()%255, 1)

#define ARR_COLOR [NSArray arrayWithObjects:_RGB(255, 115, 38, 1),_RGB(98, 217, 255, 1),_RGB(38, 255, 150, 1), _RGB(38, 79, 255, 1), _RGB(212, 38, 255, 1), nil]

//iPhone5 helper
#define ASSET_BY_SCREEN_HEIGHT(regular) (([[UIScreen mainScreen] bounds].size.height <= 480.0) ? regular : [regular stringByAppendingString:@"-568h"])

//iPhone Or iPad
#define isiPhone ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)

//iOS7 Or less
#define ISIOS7 (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//UserDefault
#define USERDEFAULT [NSUserDefaults standardUserDefaults]

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
#   define DLog(...)
#endif

#define IsIPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IsIPhone5 ([[UIScreen mainScreen] bounds].size.height == 568)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

//Api Url

#define mapQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0)
#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0)
//#define API_URL @"http://192.168.0.104/swift_back/public/"

#define API_URL @"http://52.24.68.97/"
#define SERVICE_URL @"http://52.24.68.97/"
#define TERM_OF_USE_URL @""
#define PRIVACY_URL @"http://swiftback.com/terms/"
#define VERIFY_URL @"http://swiftback.com/verify/"

#pragma Mark - Sinch

extern int pushId;
extern NSDictionary *pushDict;
extern NSString *const  kSinchSandBoxEnvironment;
extern NSString *const  kSinchLiveEnvironment;
extern NSString *const  kSinchApplicationSecret;
extern NSString *const  kSinchApplicationkey;


//////////// WS Methods
#pragma mark-
#pragma mark- Methods
extern NSString *const FILE_COUNTRY_LIST;
extern NSString *const FILE_REGISTER;//register
extern NSString *const FILE_LOGIN;//login
extern NSString *const FILE_REQUEST;//getrequests
extern NSString *const FILE_RESPOND_REQUEST;//respondrequest
extern NSString *const FILE_GET_REQUEST;//getrequest
extern NSString *const FILE_WLKER_STARTED;//requestwalkerstarted
extern NSString *const FILE_WLKER_ARRIVED;//requestwalkerarrived
extern NSString *const FILE_WALK_STARTED;//requestwalkstarted
extern NSString *const FILE_ADD_LOCATION;//addlocation
extern NSString *const FILE_WALK_COMPLETED;//requestwalkcompleted
extern NSString *const FILE_RATING;//rating
extern NSString *const FILE_PROGRESS;//requestinprogress
extern NSString *const FILE_USERLOCATION;//addlocation
extern NSString *const FILE_PAGES;//application/pages
extern NSString *const FILE_UPDATE_PROFILE;//@"provider/update"
extern NSString *const FILE_WALKER_TYPE;//application/types
extern NSString *const FILE_HISTORY;//provider/history
extern NSString *const FILE_FORGOT_PASSWORD;//application/forgot-password
extern NSString *const FILE_CHECKSTATUS; //provider/checkstate
extern NSString *const FILE_TOGGLE;//provider/togglestate
extern NSString *const FILE_WALK_LOCATION;//request/location
extern NSString *const FILE_REQUEST_PATH;//request/path
extern NSString *const FILE_LOGOUT;//provider/logout
extern NSString *const FILE_CANCELREQUEST;//walker/cancelwalk
extern NSString *const FILE_RESET_PASSWORD;
extern NSString *const FILE_REQUEST_COUNTER;
extern NSString *const FILE_ALL_REQUEST;
extern NSString *const FILE_GET_CONFIRMED_REQUESTS;
extern NSString *const FILE_VERIFY_USER;
extern NSString *const FILE_RESEND_OTP;
extern NSString *const FILE_CANCEL_FUTURE_WALK;
extern NSString *const FILE_GET_SAVINGS;
extern NSString *const FILE_SUBMIT_GOAL;

extern NSString *const FILE_BILL;//requestwalkerstarted
extern NSString *const FILE_BANK_ACCOUNT;//requestwalkerstarted


extern NSString *const FILE_DOCUMENT_LIST;
extern NSString *const FILE_DRIVER_DOCUMENT;
extern NSString *const FILE_BANKDETAILS;
extern NSString *const FILE_CONFIGURATION;
extern NSString *const FILE_GET_PASSENGERS;
extern NSString *const FILE_SEND_PUSH_TO_PASSENGER;
extern NSString *const FILE_CHAT_HISTORY;
extern NSString *const FILE_ADVANCE_RIDE_START;
extern NSString *const FILE_LEADER_POINTS;

extern NSString *const FILE_GET_USER_INFO;
extern NSString *const FILE_OFFER_RIDE;
extern NSString *const FILE_OFFERS;
extern NSString *const FILE_CANCEL_RIDE;
extern NSString *const FILE_COMPETE_OFFER;
extern NSString *const FILE_TOGGLE_RIDE_NOTIFICATION;
extern NSString *const FILE_SEND_PUSH_TO_WALKER;


#pragma mark-
#pragma mark- General Parameter

extern NSString *const PREF_IS_LOGIN;
extern NSString *const PREF_LOGIN_BY;
extern NSString *const PREF_EMAIL;
extern NSString *const PREF_PASSWORD;
extern NSString *const PREF_SOCIAL_ID;
extern NSString *const PREF_USER_TOKEN;
extern NSString *const PREF_USER_ID;
extern NSString *const PREF_REQUEST_ID;//request_id
extern NSString *const PREF_DEVICE_TOKEN;
extern NSString *const PREF_IS_APPROVED;


extern NSString *const PREF_USER_NAME;
extern NSString *const PREF_USER_PHONE;
extern NSString *const PREF_USER_RATING;
extern NSString *const PREF_USER_PICTURE;

extern NSString *const PREF_WALK_TIME;
extern NSString *const PREF_WALK_DISTANCE;
extern NSString *const PREF_START_TIME;
extern NSString *const PREF_FIRST_NAME;
extern NSString *const PREF_LAST_NAME;
extern NSString *const PREF_CAR_COLOR;
extern NSString *const PREF_CAR_NUMBER;
extern NSString *const PREF_LOGIN_OBJECT;

extern NSString *const PREF_NAV;
extern NSString *const PREF_OTP;
extern NSString *const PREF_CHAT_COUNTER;
extern NSString *const PREF_DEVICE_LANGUAGE;
extern NSString *const PREF_COUNTRY;


extern NSString *const PREF_PROMO_DISCOUNT;
extern NSString *const PREF_BASE_PAYMENT;;

extern NSString *device_token;
extern NSArray *arrUser;
extern NSArray *arrPage;

extern NSString *struser_lati;
extern NSString *struser_longi;
extern NSString *strowner_lati;
extern NSString *strowner_longi;

#pragma mark-
#pragma mark- register Parameter

extern NSString *const PARAM_EMAIL;//email
extern NSString *const PARAM_PASSWORD;//password
extern NSString *const PARAM_OLDPASSWORD;//old_password
extern NSString *const PARAM_NEWPASSWORD;//new_password
extern NSString *const PARAM_FIRST_NAME;//first_name
extern NSString *const PARAM_LAST_NAME;//last_name
extern NSString *const PARAM_PHONE;//phone
extern NSString *const PARAM_DEVICE_TOKEN;//device_token
extern NSString *const PARAM_DEVICE_TYPE;//device_type
extern NSString *const PARAM_BIO;//bio
extern NSString *const PARAM_OCCUPATION;
extern NSString *const PARAM_ADDRESS;//address
extern NSString *const PARAM_STATE;//state
extern NSString *const PARAM_COUNTRY;//country
extern NSString *const PARAM_ZIPCODE;//zipcode
extern NSString *const PARAM_LOGIN_BY;//login_by
extern NSString *const PARAM_SOCIAL_ID;//social_unique_id
extern NSString *const PARAM_PICTURE;//picture
extern NSString *const PARAM_WALKER_TYPE;//type
extern NSString *const PARAM_TAXI_MODEL;//car_model
extern NSString *const PARAM_CAR_NUMBER;//car_number
extern NSString *const PARAM_GENDER;
extern NSString *const PARAM_CAR_COLOR;//car_color
extern NSString *const PARAM_VERSION;
extern NSString *const PARAM_AMOUNT;
extern NSString *const PARAM_IMAGE;
extern NSString *const PARAM_NAME;


//Stripe Details
extern NSString *const PARAM_ACCOUNT_TYPE;
extern NSString *const PARAM_ACCOUNT_NUMBER;
extern NSString *const PARAM_ROUTING_NUMBER;
extern NSString *const PARAM_DATE_OF_BIRTH;
extern NSString *const PARAM_BANK_NAME;
extern NSString *const PARAM_POSTAL;
extern NSString *const PARAM_ACCOUNT_HOLDER_NAME;
extern NSString *const PARAM_CURRENCY;

extern NSString *const PARAM_IDENTIFICATION_NUMBER;
extern NSString *const PARAM_ADDRESS_1;
extern NSString *const PARAM_ADDRESS_2;
extern NSString *const PARAM_POSTAL_CODE;
extern NSString *const PARAM_ACCOUNT_CITY;
extern NSString *const PARAM_ACCOUNT_STATE;
extern NSString *const PARAM_ACCOUNT_TOKEN;
extern NSString *const PARAM_ACCOUNT_ID;
extern NSString *const PARAM_IP_ADDRESS;

extern NSString *const PARAM_TOKEN_ACCOUNT_TYPE;
extern NSString *const PARAM_TOKEN_ACCOUNT_NUMBER;

extern NSString *const PARAM_BANK_CODE;
extern NSString *const PARAM_BRANCH_CODE;


extern NSString *const PARAM_SOURCE_LATITUDE;
extern NSString *const PARAM_SOURCE_LONGITUDE;

extern NSString *const PARAM_DESTIANTION_LATITUDE;
extern NSString *const PARAM_DESTIANTION_LONGITUDE;

extern NSString *const PARAM_SOURCE_ADDRESS;
extern NSString *const PARAM_DESTIANTION_ADDRESS;

extern NSString *const PARAM_OFFER_ID;

extern NSString *const DATE_TIME;

#pragma mark-
#pragma mark- Check Request

extern NSString *PARAM_ID;//id
extern NSString *PARAM_TOKEN;//token
extern NSString *const PARAM_PAGE;
#pragma mark-
#pragma mark- Respond Request

extern NSString *PARAM_REQUEST_ID;//request_id
extern NSString *PARAM_ACCEPTED;//accepted

#pragma mark-
#pragma mark- Walker Started Request

extern NSString *PARAM_LATITUDE;//latitude
extern NSString *PARAM_LONGITUDE;//longitude
extern NSString *PARAM_CURRENT_COUNTRY; //current country //New Param for contry specific request


#pragma mark-
#pragma mark- Walk COMPLETED Request

extern NSString *PARAM_DISTANCE;//distance
extern NSString *PARAM_TIME;//time
extern NSString *PARAM_TOTAL;
extern NSString *PARAM_PROMO_DISCOUNT;
extern NSString *PARAM_BASE_PAYMENT;

extern NSString *const PARAM_NO_OF_PASSENGER;
extern NSString *const PARAM_NOTE;
extern NSString *const PARAM_WEEKLY;


#pragma mark-
#pragma mark- RATING

extern NSString *PARAM_RATING;//rating
extern NSString *PARAM_COMMENT;//comment

extern NSString *const PARAM_OTP_CODE;
extern NSString *const PARAM_OTP_VERIFY;

//Driver Reasons for cancellation
extern NSString *PARAM_DRIVER_REASON;


extern NSDictionary *dictBillInfo;
extern int is_completed;
extern int is_dog_rated;
extern int is_walker_started;
extern int is_walker_arrived;
extern int is_started;
extern int is_accepted;
extern int payment;
extern int driver_reason;
extern int promo_discount;
extern int base_payment;


#pragma mark -
#pragma mark - Segue Identifier

extern NSString *const SEGUE_TO_VERIFY_DRIVER;
extern NSString *const SEGUE_TO_OTP;
extern NSString *const SEGUE_TO_ADVANCE_PICKUP;
extern NSString *const SEGUE_TO_CONFIRMED_RIDES;
extern NSString *const SEGUE_TO_CREATE_OFFER;
extern NSString *const SEGUE_TO_CHAT;
extern NSString *const SEGUE_TO_DIRECT_CHAT;
extern NSString *const SEGUE_TO_PUSH_CHAT;
extern NSArray *arrPassengers;





