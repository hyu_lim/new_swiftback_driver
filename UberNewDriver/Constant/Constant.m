
#import "Constant.h"


#pragma Mark - Sinch
int pushId;
NSDictionary *pushDict;
NSString   *const kSinchSandBoxEnvironment  = @"sandbox.sinch.com";
NSString   *const kSinchLiveEnvironment     = @"clientapi.sinch.com";
NSString   *const kSinchApplicationSecret  = @"8Yviu8+u50aAtvSrus1ZxQ==";
NSString   *const kSinchApplicationkey      = @"388a7042-08dd-4484-bcf4-02ca7003ed02";

#pragma mark-
#pragma mark- Methods
NSString *const FILE_COUNTRY_LIST=@"provider/country.php";
NSString *const FILE_REGISTER=@"provider/register";//register
NSString *const FILE_LOGIN=@"provider/login";//login
NSString *const FILE_REQUEST=@"provider/getrequests";//getrequests
NSString *const FILE_RESPOND_REQUEST=@"provider/respondrequest";//respondrequest
NSString *const FILE_GET_REQUEST=@"provider/getrequest";//getrequest
NSString *const FILE_WLKER_STARTED=@"provider/requestwalkerstarted";//requestwalkerstarted
NSString *const FILE_WLKER_ARRIVED=@"provider/requestwalkerarrived";//requestwalkerarrived
NSString *const FILE_WALK_STARTED=@"provider/requestwalkstarted";//requestwalkstarted
NSString *const FILE_ADD_LOCATION=@"provider/addlocation";//addlocation
NSString *const FILE_WALK_COMPLETED=@"provider/requestwalkcompleted";//requestwalkcompleted
NSString *const FILE_RATING=@"provider/rating";//rating
NSString *const FILE_PROGRESS=@"provider/requestinprogress";//requestinprogress
NSString *const FILE_USERLOCATION=@"provider/location";//location
NSString *const FILE_PAGES=@"application/pages";//application/pages
NSString *const FILE_UPDATE_PROFILE=@"provider/update";//provider/update
NSString *const FILE_WALKER_TYPE=@"application/types";//application/types
NSString *const FILE_HISTORY=@"provider/history";//user/history
NSString *const FILE_FORGOT_PASSWORD=@"application/forgot-password";//application/forgot-password
NSString *const FILE_CHECKSTATUS=@"provider/checkstate"; //provider/checkstate
NSString *const FILE_TOGGLE=@"provider/togglestate";//provider/togglestate
NSString *const FILE_WALK_LOCATION=@"request/location";//request/location
NSString *const FILE_REQUEST_PATH=@"provider/requestpath";//request/path
NSString *const FILE_LOGOUT=@"provider/logout";//provider/logout
NSString *const FILE_CANCELREQUEST=@"walker/cancelwalk";
NSString *const FILE_RESET_PASSWORD=@"provider/reset-password";//provider/reset-password
NSString *const FILE_REQUEST_COUNTER=@"provider/incoming_request_count";


NSString *const FILE_BANK_ACCOUNT=@"https://api.stripe.com/v1/tokens";


NSString *const FILE_BILL=@"provider/bill";//BILL

NSString *const FILE_ALL_REQUEST=@"provider/get_all_requests";
NSString *const FILE_GET_CONFIRMED_REQUESTS=@"provider/get_Confirm_requests";
NSString *const FILE_VERIFY_USER=@"provider/otp_verification";
NSString *const FILE_RESEND_OTP=@"provider/resend_otp";
NSString *const FILE_CANCEL_FUTURE_WALK=@"provider/cancel_future_walk";
NSString *const FILE_GET_SAVINGS=@"/provider/walker_earning";
NSString *const FILE_SUBMIT_GOAL=@"provider/my_goal";
NSString *const FILE_DOCUMENT_LIST=@"provider/document_list";
NSString *const FILE_DRIVER_DOCUMENT=@"provider/set_driver_document";
NSString *const FILE_BANKDETAILS=@"provider/provider_bankdetails";
NSString *const FILE_CONFIGURATION=@"provider/configuration";
NSString *const FILE_GET_PASSENGERS=@"provider/get_associated_client";
NSString *const FILE_SEND_PUSH_TO_PASSENGER=@"provider/send_push_to_passenger";
NSString *const FILE_CHAT_HISTORY=@"provider/chat_history";

NSString *const FILE_ADVANCE_RIDE_START=@"provider/advance_ride_started";
NSString *const FILE_LEADER_POINTS=@"provider/leader_points";

NSString *const FILE_GET_USER_INFO=@"provider/get-info";
NSString *const FILE_OFFER_RIDE=@"provider/offer_ride";
NSString *const FILE_OFFERS=@"provider/offer_ride_list";
NSString *const FILE_CANCEL_RIDE=@"provider/cancel_offer_ride";
NSString *const FILE_COMPETE_OFFER=@"provider/complete_offer_ride";
NSString *const FILE_TOGGLE_RIDE_NOTIFICATION=@"provider/toggle_ride_notification";


#pragma mark-
#pragma mark- General Parameter

NSString *const PREF_IS_LOGIN=@"is_login";
NSString *const PREF_LOGIN_BY=@"type";
NSString *const PREF_EMAIL=@"email";
NSString *const PREF_PASSWORD=@"password";
NSString *const PREF_SOCIAL_ID=@"social_id";
NSString *const PREF_USER_TOKEN=@"token";
NSString *const PREF_USER_ID=@"id";
NSString *const PREF_REQUEST_ID=@"request_id";
NSString *const PREF_DEVICE_TOKEN=@"device_token";
NSString *const PREF_USER_NAME=@"name";
NSString *const PREF_USER_PHONE=@"phone";
NSString *const PREF_USER_RATING=@"rate";
NSString *const PREF_USER_PICTURE=@"picture";
NSString *const PREF_WALK_TIME=@"time";
NSString *const PREF_WALK_DISTANCE=@"distance";
NSString *const PREF_START_TIME=@"startTime";
NSString *const PREF_FIRST_NAME=@"first_name";
NSString *const PREF_LAST_NAME=@"last_name";
NSString *const PREF_IS_APPROVED=@"is_approved";
NSString *const PREF_CAR_COLOR=@"car_color";
NSString *const PREF_CAR_NUMBER=@"car_number";
NSString *const PREF_LOGIN_OBJECT=@"loginobject";

NSString *const PREF_NAV=@"PREF_NAV";
NSString *const PREF_OTP=@"otp";
NSString *const PREF_CHAT_COUNTER=@"chatCounter";
NSString *const PREF_DEVICE_LANGUAGE=@"device_language";
NSString *const PREF_COUNTRY=@"current_country";

NSString *const PREF_BASE_PAYMENT=@"base_payment";
NSString *const PREF_PROMO_DISCOUNT=@"promo_discount";

NSString *strowner_lati;
NSString *strowner_longi;
NSString *struser_lati;
NSString *struser_longi;
NSString *device_token;
NSArray *arrUser;
NSArray *arrPage;

NSDictionary *dictBillInfo;
int is_completed;
int is_dog_rated;
int is_walker_started;
int is_walker_arrived;
int is_started;
int is_accepted;
int payment;
int driver_reason;
int base_payment;
int promo_discount;

#pragma mark-
#pragma mark- Register Parameter

 NSString *const PARAM_EMAIL=@"email";//email
 NSString *const PARAM_PASSWORD=@"password";//password
 NSString *const PARAM_OLDPASSWORD=@"old_password";//old_password
 NSString *const PARAM_NEWPASSWORD=@"new_password";//new_password
 NSString *const PARAM_FIRST_NAME=@"first_name";//first_name
 NSString *const PARAM_LAST_NAME=@"last_name";//last_name
 NSString *const PARAM_PHONE=@"phone";//phone
 NSString *const PARAM_DEVICE_TOKEN=@"device_token";//device_token
 NSString *const PARAM_DEVICE_TYPE=@"device_type";//device_type
 NSString *const PARAM_BIO=@"bio";//bio
 NSString *const PARAM_OCCUPATION=@"occupation";
 NSString *const PARAM_ADDRESS=@"address";//address
 NSString *const PARAM_STATE=@"state";//state
 NSString *const PARAM_COUNTRY=@"country";//country
 NSString *const PARAM_ZIPCODE=@"zipcode";//zipcode
 NSString *const PARAM_LOGIN_BY=@"login_by";//login_by
 NSString *const PARAM_SOCIAL_ID=@"social_unique_id";//social_unique_id
 NSString *const PARAM_PICTURE=@"picture";//picture
 NSString *const PARAM_WALKER_TYPE=@"type";//type
 NSString *const PARAM_TAXI_MODEL=@"car_type";//car_model
 NSString *const PARAM_CAR_NUMBER=@"car_number";//car_number
 NSString *const PARAM_GENDER=@"gender";//car_number
 NSString *const PARAM_CAR_COLOR=@"car_color";//car_color
 NSString *const PARAM_VERSION=@"version";
 NSString *const PARAM_AMOUNT=@"amount";
 NSString *const PARAM_IMAGE=@"image";
 NSString *const PARAM_NAME=@"name";

NSString *const TOTAL=@"total";
NSString *const BASE_PAYMENT=@"base_payment";
NSString *const PROMO_DISCOUNT=@"promo_discount";

//Details for Stripe Bank Token
 NSString *const PARAM_ACCOUNT_TYPE=@"acc_type";
 NSString *const PARAM_ACCOUNT_NUMBER=@"acc_number";
 NSString *const PARAM_ROUTING_NUMBER=@"routing_number";
 NSString *const PARAM_DATE_OF_BIRTH=@"dob";
 NSString *const PARAM_BANK_NAME=@"bank_name";
 NSString *const PARAM_POSTAL=@"postal_code";
 NSString *const PARAM_ACCOUNT_HOLDER_NAME=@"account_holder_name";
 NSString *const PARAM_CURRENCY=@"currency";
 NSString *const PARAM_IDENTIFICATION_NUMBER=@"personal_id_number";
 NSString *const PARAM_ADDRESS_1=@"address";
 NSString *const PARAM_ADDRESS_2=@"address_line";
 NSString *const PARAM_POSTAL_CODE=@"postal_code";
 NSString *const PARAM_ACCOUNT_CITY=@"city";
 NSString *const PARAM_ACCOUNT_STATE=@"state";
 NSString *const PARAM_ACCOUNT_TOKEN=@"token";
 NSString *const PARAM_ACCOUNT_ID=@"id";
 NSString *const PARAM_IP_ADDRESS=@"ip";

NSString *const PARAM_TOKEN_ACCOUNT_TYPE=@"account_number";
NSString *const PARAM_TOKEN_ACCOUNT_NUMBER=@"account_holder_type";

NSString *const PARAM_BANK_CODE=@"bank_code";
NSString *const PARAM_BRANCH_CODE=@"branch_code";


//De



NSString *const PARAM_SOURCE_LATITUDE=@"s_latitude";
NSString *const PARAM_SOURCE_LONGITUDE=@"s_longitude";

NSString *const PARAM_DESTIANTION_LATITUDE=@"d_latitude";
NSString *const PARAM_DESTIANTION_LONGITUDE=@"d_longitude";



#pragma mark-
#pragma mark- Check Request

NSString *PARAM_ID=@"id";//id
NSString *PARAM_TOKEN=@"token";//token
NSString *const PARAM_PAGE=@"page";

//driver cancellation reason
NSString *PARAM_DRIVER_REASON=@"driver_reason";//id

#pragma mark-
#pragma mark- Respond Request

NSString *PARAM_REQUEST_ID=@"request_id";//request_id
NSString *PARAM_ACCEPTED=@"accepted";//accepted

#pragma mark-
#pragma mark- Walker Started Request

NSString *PARAM_LATITUDE=@"latitude";//latitude
NSString *PARAM_LONGITUDE=@"longitude";//longitude
NSString *PARAM_CURRENT_COUNTRY=@"current_country";//current country //New Param for contry specific request

NSString *const PARAM_SOURCE_ADDRESS=@"s_address";
NSString *const PARAM_DESTIANTION_ADDRESS=@"d_address";

NSString *const PARAM_OFFER_ID=@"offer_id";

NSString *const DATE_TIME=@"date_time";

NSString *const PARAM_NO_OF_PASSENGER=@"no_of_passenger";
NSString *const PARAM_NOTE=@"note";

NSString *const PARAM_WEEKLY = @"weekly";


#pragma mark-
#pragma mark- Walk COMPLETED Request

NSString *PARAM_DISTANCE=@"distance";//distance
NSString *PARAM_TIME=@"time";//time
NSString *PARAM_TOTAL=@"total";
NSString *PARAM_BASE_PAYMENT=@"base_payment";
NSString *PARAM_PROMO_DISCOUNT=@"promo_discount";

#pragma mark-
#pragma mark- RATING

NSString *PARAM_RATING=@"rating";//rating
NSString *PARAM_COMMENT=@"comment";//comment
NSString *const PARAM_OTP_CODE=@"otp_code";
NSString *const PARAM_OTP_VERIFY=@"otp_verify";


#pragma mark -
#pragma mark - Segue Identifier

NSString *const SEGUE_TO_VERIFY_DRIVER=@"segueToVerifyDriver";
NSString *const SEGUE_TO_OTP=@"segueToOTP";
NSString *const SEGUE_TO_ADVANCE_PICKUP=@"segueToAdvancePIckup";
NSString *const SEGUE_TO_CONFIRMED_RIDES=@"segueToConfirmedRides";
NSString *const SEGUE_TO_CREATE_OFFER=@"segueToCreateOffer";
NSString *const SEGUE_TO_CHAT=@"segueToChatVC";
NSString *const SEGUE_TO_DIRECT_CHAT=@"seugeToDirectChat";
NSString *const SEGUE_TO_PUSH_CHAT=@"segueToPushChat";

NSArray *arrPassengers;









