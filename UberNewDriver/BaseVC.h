

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController
{
    BOOL animPop;
}

- (NSMutableDictionary *)cleanNullInJsonDic:(NSMutableDictionary *)dict;
- (NSMutableArray *)cleanNullInJsonArray:(NSMutableArray *)array;

- (void)setBackBarItem;
- (void)setBackBarItem:(BOOL)animated;
- (void)setNavBarTitle:(NSString *)title;

@end
