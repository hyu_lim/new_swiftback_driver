
#import "BaseVC.h"
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "introPageContentViewController.h"

@interface ViewController : BaseVC <CLLocationManagerDelegate,UIPageViewControllerDataSource>

@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;
@property (strong, nonatomic) NSArray *pageTitles2;
@property (strong, nonatomic) NSArray *pageImages2;
@property (strong, nonatomic) NSArray *pageTitles3;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@property (weak, nonatomic) IBOutlet UIButton *btnPassenger;

@property (weak, nonatomic) IBOutlet UIView *movieView;
@property (weak, nonatomic) IBOutlet UIView *gradintView;
@property (strong, nonatomic) IBOutlet UIView *testFrame;
@property (strong, nonatomic) UIPageViewController *pageViewController;
///- (IBAction)startWalkthrough:(id)sender;
//- (IBAction)onClickSignIn:(id)sender;
//- (IBAction)onClickRegister:(id)sender;

@end
