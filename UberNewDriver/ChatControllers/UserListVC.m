//
//  UserListVC.m
//  SwiftBack
//
//  Created by Elluminati on 25/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import "UserListVC.h"
#import "UserCell.h"
#import "UIImageView+Download.h"
#import "ChatVC.h"
#import "SWRevealViewController.h"

@interface UserListVC (){
    
    NSNumber *snumber;
    UITableViewController *tableViewController;
    NSMutableArray *arrTempPassenger;
    NSUInteger PassengerPage,page,totalPagesPassenger;
    BOOL is_first;
    //int value;
}

@end

@implementation UserListVC

- (void)viewDidLoad {
    
    
    
    /*
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    int intValue = 1;
    
    [dict setObject:[NSNumber numberWithInt:intValue] forKey:@"current_page"];
    
    int number = [[dict objectForKey:@"integer"] intValue];
     */
    
	[super viewDidLoad];
	[self customSetup];
    [self setLocalizedStrings];
    
    int value = [snumber intValue];
    snumber = [NSNumber numberWithInt:value + 1];
    
    
    //PassengerPage=1;
    //totalPagesPassenger=2;
    
	if ([arrPassengers count] == 0)
    {
		//[self getAssociatedPassengers];
        
        //View Controller Clipping
        _mainView.layer.cornerRadius = 5;
        _mainView.clipsToBounds = YES;
	}
    
    //View Controller Clipping
    _mainView.layer.cornerRadius = 5;
    _mainView.clipsToBounds = YES;
    
    //Pull to refresh table view configuration
    tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = tableForUsers;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor darkGrayColor];
    [self.refreshControl addTarget:self action:@selector(callRequestMethod) forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;
    
    // for reload more data
    pullToRefreshManager_ = [[MNMBottomPullToRefreshManager alloc] initWithPullToRefreshViewHeight:40.0f tableView:tableForUsers withClient:self];
    
    //
    
    PassengerPage=1;
    totalPagesPassenger=1;
    is_first=YES;
    arrTempPassenger=[[NSMutableArray alloc] init];
    
    
}

- (void)callRequestMethod
{
   
        arrTempPassenger=[[NSMutableArray alloc] init];
        PassengerPage=1;
        [self getClients:@"clients" page:1];
}

-(void)getClients:(NSString*)type page:(NSInteger)numberOfPage
{
    NSLog(@"TYPE : %@", type);
    
    if ([APPDELEGATE connected])
    {
        if(is_first==YES)
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
            is_first=NO;
        }
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        //[dictParam setObject:@"1" forKey:type];
        [dictParam setObject:@"1" forKey:PARAM_PAGE];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [helper getDataFromPath:FILE_GET_PASSENGERS withParamData:dictParam withBlock:^(id response, NSError *error) {
            
            [APPDELEGATE hideLoadingView];
            
            if (response)
            {
                if ([[response objectForKey:@"success"] boolValue])
                {
                    
                    
                    NSUserDefaults *pref = [[NSUserDefaults alloc]init];
                    [pref synchronize];
                    
                    
                    
                    
                    [arrTempPassenger addObjectsFromArray:[response valueForKey:@"clients"]];
                    
                    arrPassengers=[arrTempPassenger mutableCopy];
                    
                    
                    //arrPassengers = [response valueForKey:@"clients"];
                        //[arrTempPassenger addObjectsFromArray:[response valueForKey:@"clients"]];
                        //arrPassengers=[arrTempPassenger mutableCopy];
                        //totalPagesPassenger=[[response valueForKey:@"page_counts"]integerValue];
                        /*if(numberOfPage==totalPagesPassenger)
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }
                        else
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }*/
                    
                    
                    /*
                    if ([type isEqualToString:@"clients"] && [arrPassengers count] > 0)
                    {*/
                        [tableForUsers reloadData];
                        [tableForUsers setHidden:NO];
                        [noItemsImgView setHidden:YES];
                    /*}
                    else if ([type isEqualToString:@"earliest"] && [earliestRequest count] >0){
                        
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                        
                    }
                    else
                    {
                        [tableForUsers setHidden:YES];
                        [noItemsImgView setHidden:NO];
                    }*/
                    [pullToRefreshManager_ relocatePullToRefreshView];
                    
                    
                    
                    
                }
                else{
                    
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                    [tableForUsers setHidden:YES];
                    [noItemsImgView setHidden:NO];
                }
                
                
            }
            else
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
            }
            [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }];
    }
    
    
    PassengerPage=0;
    totalPagesPassenger=0;
    is_first=YES;
    snumber=0;
    
    int value = [snumber intValue];
    
    
    snumber = [NSNumber numberWithInt:value + 1];
    
    //[snumber intValue] == 1;
    arrTempPassenger=[[NSMutableArray alloc] init];
    
    //NSNumber *snumber = [NSNumber numberWithInt:0];
}

- (void)reloadData {
    //Reload table data
    [tableForUsers reloadData];
    
    //End the refreshing
    if(self.refreshControl) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update : %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrDictionary = [NSDictionary dictionaryWithObject:[UIColor darkGrayColor] forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        [self.refreshControl endRefreshing];
    }
}

#pragma mark -
#pragma mark - ReloadTable to bottom
- (void)bottomPullToRefreshTriggered:(MNMBottomPullToRefreshManager *)manager
{
    [self performSelector:@selector(loadTable) withObject:nil afterDelay:1.0f];
    
    
}

- (void)loadTable
{
    
        /*PassengerPage++;
        page=PassengerPage;
    if(page<=totalPagesPassenger)
    {
         
         
            [self getRequests:@"clients" page:2];
    
}*/
    
    if ([APPDELEGATE connected])
    if(is_first==YES)
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
        is_first=NO;
    }
    
    
    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
    //[dictParam setObject:@"1" forKey:type];
    /*
    NSInteger currentPage = 2;
    NSNumber *number = [NSNumber numberWithInteger: currentPage];
    NSInteger value = [number integerValue];
    */
    /*
    [myDict setObject:[NSNumber numberWithInt:intValue] forKey:[NSNumber numberWithInt:intKey]];
    NSLog(@"key: %i, value: %i", intKey, [[myDict objectForKey:[NSNumber numberWithInt:intKey] intValue]);*/
    /*
    int first = 15;
    int second = 14;
    
    NSNumber *sum = @([@"1" integerValue] + [@"2" integerValue]);
     */
    
    
    
    int value = [snumber intValue];
    
    
    snumber = [NSNumber numberWithInt:value + 1];
    
    [dictParam setObject:snumber forKey:PARAM_PAGE];
    
    
    
    
    
    
    
    //NSInteger temp = 1;
    //[dictParam setObject:[NSNumber numberWithInteger:temp] forKey:PARAM_PAGE];
    

    AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [helper getDataFromPath:FILE_GET_PASSENGERS withParamData:dictParam withBlock:^(id response, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        if (response)
        {
            if ([[response objectForKey:@"success"] boolValue])
            {
                
                
                NSUserDefaults *pref = [[NSUserDefaults alloc]init];
                [pref synchronize];
                
                //arrPassengers = [response valueForKey:@"clients"];
                
                
                [arrTempPassenger addObjectsFromArray:[response valueForKey:@"clients"]];
                
                arrPassengers=[arrTempPassenger mutableCopy];
                
                
                
                
                
                //passengerRequest=[arrTempPassenger mutableCopy];
                //totalPagesNearest=[[response valueForKey:@"page_counts"]integerValue];
                
                //[arrTempPassenger addObjectsFromArray:[dictParam setObject:@"2" forKey:PARAM_PAGE]];
                
                
                //[arrTempPassenger addObjectsFromArray:[response valueForKey:@"clients"]];
                //arrPassengers=[arrTempPassenger mutableCopy];
                //totalPagesPassenger=[[response valueForKey:@"page_counts"]integerValue];
                /*if(numberOfPage==totalPagesPassenger)
                 {
                 [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                 }
                 else
                 {
                 [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                 }*/
                
                
                /*
                 if ([type isEqualToString:@"clients"] && [arrPassengers count] > 0)
                 {*/
                [tableForUsers reloadData];
                [tableForUsers setHidden:NO];
                [noItemsImgView setHidden:YES];
                /*}
                 else if ([type isEqualToString:@"earliest"] && [earliestRequest count] >0){
                 
                 [tblRequestList reloadData];
                 [tblRequestList setHidden:NO];
                 [noItemsImgView setHidden:YES];
                 
                 }
                 else
                 {
                 [tableForUsers setHidden:YES];
                 [noItemsImgView setHidden:NO];
                 }*/
                
                if([[response valueForKey:@"error_code"] intValue]==407)
                    
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"USERS_LOADED", nil) message:NSLocalizedString(@"ALL_USERS_LOADED", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                    
                }
                
                
                [pullToRefreshManager_ relocatePullToRefreshView];
                
                
                
                
            }
            else{
                
                [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                [tableForUsers setHidden:YES];
                [noItemsImgView setHidden:NO];
            }
            
            
        }
        else
        {
            [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
        }
        [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    }];



       [pullToRefreshManager_ tableViewReloadFinished];


}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [pullToRefreshManager_ relocatePullToRefreshView];
}

-(void)getRequests:(NSString*) page:(NSInteger)numberOfPage
{
    //NSLog(@"TYPE : %@", type);
    
    if ([APPDELEGATE connected])
    {
        if(is_first==YES)
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
            is_first=NO;
        }
        NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        //[dictParam setObject:@"1" forKey:type];
        //[dictParam setObject:[NSString stringWithFormat:@"%ld",(long)numberOfPage] forKey:PARAM_PAGE];
        
        
        [dictParam setObject:@"1" forKey:PARAM_PAGE];
        
        AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [helper getDataFromPath:FILE_GET_PASSENGERS withParamData:dictParam withBlock:^(id response, NSError *error) {
            
            [APPDELEGATE hideLoadingView];
            
            if (response)
            {
                if ([[response objectForKey:@"success"] boolValue])
                {
                    
                    
                    NSUserDefaults *pref = [[NSUserDefaults alloc]init];
                  
                    [pref synchronize];
                    
                    
                    
                    [arrTempPassenger addObjectsFromArray:[response valueForKey:@"clients"]];
                    
                    arrPassengers=[arrTempPassenger mutableCopy];
                    
                    
                        //[arrTempPassenger addObjectsFromArray:[response valueForKey:@"request"]];
                        //PassengerRequest=[arrTempPassenger mutableCopy];
                       /* totalPagesPassenger=[[response valueForKey:@"page_counts"]integerValue];
                        if(numberOfPage==totalPagesPassenger)
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }
                        else
                        {
                            [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                        }*/
                    
                   
                    
                    
                        [tblRequestList reloadData];
                        [tblRequestList setHidden:NO];
                        [noItemsImgView setHidden:YES];
                    
                    [pullToRefreshManager_ relocatePullToRefreshView];
                    
                    
                    
                    
                }
                else{
                    
                    [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                    [tblRequestList setHidden:YES];
                    [noItemsImgView setHidden:NO];
                }
                
                
            }
            else
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
            }
            [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        }];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [pullToRefreshManager_ tableViewScrolled];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [pullToRefreshManager_ tableViewReleased];
}

-(void)viewWillAppear:(BOOL)animated
{
    arrPassengers=[[NSMutableArray alloc] init];
    PassengerPage=1;
    [self getAssociatedPassengers:@"clients" page:1];
}

-(void)customSetup{
	SWRevealViewController *revealViewController = self.revealViewController;
	if ( revealViewController )
	{
		[menuBtn addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
		//Swipe to reveal menu
		[self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
	}
}

- (void)setLocalizedStrings {
    [self.lblNavTitle setText:NSLocalizedString(@"SELECT_PASSENGER", nil)];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark  - Get Connected Passenger List

-(void)getAssociatedPassengers:(NSString*)type page:(NSInteger)numberOfPage{
    
    /*PassengerPage++;
     page=PassengerPage;
     if(page<=totalPagesPassenger)
     {
     
     
     [self getRequests:@"clients" page:2];
     
     }*/
    
    if ([APPDELEGATE connected])
        if(is_first==YES)
        {
            [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING", nil)];
            is_first=NO;
        }
    
    
    NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_ID] forKey:PARAM_ID];
    [dictParam setObject:[[NSUserDefaults standardUserDefaults] objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
    //[dictParam setObject:@"1" forKey:type];
    
    [dictParam setObject:@"1" forKey:PARAM_PAGE];
    
    
    AFNHelper *helper = [[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [helper getDataFromPath:FILE_GET_PASSENGERS withParamData:dictParam withBlock:^(id response, NSError *error) {
        
        [APPDELEGATE hideLoadingView];
        
        if (response)
        {
            if ([[response objectForKey:@"success"] boolValue])
            {
                
                
                NSUserDefaults *pref = [[NSUserDefaults alloc]init];
                [pref synchronize];
                
                //arrPassengers = [response valueForKey:@"clients"];
                
                
                [arrTempPassenger addObjectsFromArray:[response valueForKey:@"clients"]];
                
                arrPassengers=[arrTempPassenger mutableCopy];
                
                
                
                
                
                //passengerRequest=[arrTempPassenger mutableCopy];
                //totalPagesNearest=[[response valueForKey:@"page_counts"]integerValue];
                
                //[arrTempPassenger addObjectsFromArray:[dictParam setObject:@"2" forKey:PARAM_PAGE]];
                
                
                //[arrTempPassenger addObjectsFromArray:[response valueForKey:@"clients"]];
                //arrPassengers=[arrTempPassenger mutableCopy];
                //totalPagesPassenger=[[response valueForKey:@"page_counts"]integerValue];
                /*if(numberOfPage==totalPagesPassenger)
                 {
                 [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                 }
                 else
                 {
                 [pullToRefreshManager_ setPullToRefreshViewVisible:YES];
                 }*/
                
                
                /*
                 if ([type isEqualToString:@"clients"] && [arrPassengers count] > 0)
                 {*/
                [tableForUsers reloadData];
                [tableForUsers setHidden:NO];
                [noItemsImgView setHidden:YES];
                /*}
                 else if ([type isEqualToString:@"earliest"] && [earliestRequest count] >0){
                 
                 [tblRequestList reloadData];
                 [tblRequestList setHidden:NO];
                 [noItemsImgView setHidden:YES];
                 
                 }
                 else
                 {
                 [tableForUsers setHidden:YES];
                 [noItemsImgView setHidden:NO];
                 }*/
                [pullToRefreshManager_ relocatePullToRefreshView];
                
                
                
                
            }
            else{
                
                [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                [tableForUsers setHidden:YES];
                [noItemsImgView setHidden:NO];
            }
            
            
        }
        else
        {
            [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_TIME_OUT", nil)];
        }
        [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    }];
    
    
    
    [pullToRefreshManager_ tableViewReloadFinished];
    
    
}

#pragma mark - Navigation
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	// Get the new view controller using [segue destinationViewController].
	// Pass the selected object to the new view controller.
	
	if ([[segue identifier] isEqualToString:SEGUE_TO_CHAT]) {
		ChatVC *vc = [segue destinationViewController];
		vc.dictOwner = sender;
        
	}
}

#pragma mark
#pragma mark - Tableview Data source methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return  [arrPassengers count];	
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	UserCell *cell = [tableForUsers dequeueReusableCellWithIdentifier:@"Cell"];
	
	NSDictionary *dictWalker = [arrPassengers objectAtIndex:indexPath.row];
    if([[dictWalker valueForKey:@"unread"]integerValue]==0)
    {
        cell.lblMessageCounter.hidden=YES;
    }
    else
    {
        cell.lblMessageCounter.hidden=NO;
        cell.lblMessageCounter.layer.masksToBounds = YES;
        cell.lblMessageCounter.layer.cornerRadius = (cell.lblMessageCounter.frame.size.width / 2);
        [cell.lblMessageCounter setText:[NSString stringWithFormat:@"%@",[dictWalker valueForKey:@"unread"]]];
        [cell.lblMessageCounter setBackgroundColor:[UIColor colorWithRed:255.0/255.0f green:82.0/255.0f blue:82.0/255.0f alpha:1]];
    }

	[cell.lblDriverName setText:[dictWalker objectForKey:PARAM_NAME]];
	[cell.driverProfileView downloadFromURL:[dictWalker objectForKey:PARAM_PICTURE] withPlaceholder:[UIImage imageNamed:@"PROFPIC4"]];
    
	
    
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	
	NSDictionary *dictWalker = [arrPassengers objectAtIndex:indexPath.row];
	[self performSegueWithIdentifier:SEGUE_TO_CHAT sender:dictWalker];
    
    //NSMutableDictionary *dictParam = [[NSMutableDictionary alloc]init];
    
    
    
    PassengerPage=0;
    totalPagesPassenger=0;
    is_first=YES;
    snumber=0;
    
    int value = [snumber intValue];
    
    
    snumber = [NSNumber numberWithInt:value + 1];
    
    //[snumber intValue] == 1;
    arrTempPassenger=[[NSMutableArray alloc] init];
    
    //NSNumber *snumber = [NSNumber numberWithInt:0];
    
    
    
    [pullToRefreshManager_ tableViewReloadFinished];
    

    
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 70.0f;
}

#pragma mark
#pragma mark - Button events

- (IBAction)onClickBack:(id)sender {
	
	[self.navigationController popViewControllerAnimated:YES];
}
@end
