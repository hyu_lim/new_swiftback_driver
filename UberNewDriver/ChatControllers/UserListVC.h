//
//  UserListVC.h
//  SwiftBack
//
//  Created by Elluminati on 25/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "MNMBottomPullToRefreshManager.h"

@interface UserListVC : BaseVC<UITableViewDataSource,UITableViewDelegate,MNMBottomPullToRefreshManagerClient>
{
	__weak IBOutlet UIButton *menuBtn;
    
    __weak IBOutlet UITableView *tblRequestList;
	
    
    __weak IBOutlet UIImageView *noItemsImgView;
	__weak IBOutlet UITableView *tableForUsers;
    
    
    
    MNMBottomPullToRefreshManager *pullToRefreshManager_;
}

@property (weak, nonatomic) IBOutlet UILabel *lblNavTitle;
@property (strong, nonatomic) IBOutlet UIView *mainView;



@property (strong, nonatomic) UIRefreshControl *refreshControl;

- (IBAction)onClickBack:(id)sender;

@end
