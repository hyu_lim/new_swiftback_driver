
#import "BaseVC.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ChatVC : BaseVC<UITableViewDataSource, UITableViewDelegate , UITextFieldDelegate>{
	
	float tableHeight,viewPosition;

}

@property (strong,nonatomic) NSDictionary *dictOwner;
@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lblPassenger;
@property (weak, nonatomic) IBOutlet UITextField *message;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *enlargeImage;
@property (weak, nonatomic) IBOutlet UITableView *tableForChat;
@property (weak, nonatomic) IBOutlet UIScrollView *scrView;
@property (weak, nonatomic) IBOutlet UIImageView *driverImgView;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *viewForMessage;

- (IBAction)sendButtonPressed:(id)sender;
- (IBAction)onClickBack:(id)sender;

@end
