//
//  UserCell.h
//  SwiftBack
//
//  Created by Elluminati on 25/02/16.
//  Copyright © 2016 Swiftback. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *lblDriverName;
@property (weak, nonatomic) IBOutlet UILabel *lblMessageCounter;
@property (weak, nonatomic) IBOutlet UIImageView *driverProfileView;

@end
